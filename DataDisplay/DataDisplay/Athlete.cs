﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    [Serializable]
    public class Athlete
    {
        public string name { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string fullName { get; set; }
        public string nationalId { get; set; }
        public int yearOfBirth { get; set; }
        public int followingBreak { get; set; }
        public string clubName { get; set; }
        public string birthday { get; set; }
        public string sex { get; set; }
        public string feiId { get; set; }
        public string iocCode { get; set; }

        public void SetAthleteData(int x)
        {
            name = Constants.reportInfo[x + 5][0] + " " + Constants.reportInfo[x + 5][1];
            firstName = Constants.reportInfo[x + 5][1];
            lastName = Constants.reportInfo[x + 5][0];
            fullName = Constants.reportInfo[x + 5][1] + Constants.reportInfo[x + 5][0];
            nationalId = Constants.reportInfo[x + 8][0];
            yearOfBirth = Convert.ToInt16(Constants.reportInfo[x + 9][0]);
            followingBreak = Convert.ToInt16(Constants.reportInfo[x + 3][3]);
            clubName = Constants.reportInfo[x + 12][0];
            birthday = Constants.reportInfo[x + 16][0];
            sex = Constants.reportInfo[x + 19][0];
            feiId = Constants.reportInfo[x + 22][0];
            iocCode = Constants.reportInfo[x + 24][0];
        }
    }
}
