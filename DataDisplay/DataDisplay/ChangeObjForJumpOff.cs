﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDisplay
{
    public class ChangeObjForJumpOff
    {
        Rank_Calculate rank_Calculate = new Rank_Calculate();
        public List<TeamCompetition> tmp_result;

        public Serialization JumpOff_ranking(Serialization obj)
        {
            rank_Calculate.Rank_for_JumpOff(obj);
            int position = 1;

            for (int i = 0; i < obj.Teams.Count;)
            {
                int it = i;
                tmp_result = new List<TeamCompetition>();
                tmp_result.Add(obj.Teams[it]);
                    
                while (it + 1 < rank_Calculate.result.Count() && rank_Calculate.result[it].rank == rank_Calculate.result[it + 1].rank)
                {
                    it = it + 1;
                    tmp_result.Add(obj.Teams[it]);
                }

                if (it != i)
                {
                    tmp_result = partial_ranking(tmp_result);
                    i = it + 1;
                    position = i + 1;
                }

                if (it == i)
                {
                    obj.Teams[i].Rank = position;
                    i++;
                    position++;
                }
            }

            int rank = 1;
            for (int i = 0; i < obj.Teams.Count(); i++)
            {
                int it = 0;
                
                while (it < obj.Teams.Count && obj.Teams[it].Rank != rank)
                {
                    it++;
                }

                if (it < obj.Teams.Count && obj.Teams[it].Rank == rank)
                {
                    obj.Rank[i].teamName = obj.Teams[it].Name;
                    obj.Rank[i].rank = obj.Teams[it].Rank;
                }

                else if (it >= obj.Teams.Count && obj.Teams[it - 1].Rank == rank)
                {
                    obj.Rank[i].teamName = obj.Teams[it - 1].Name;
                    obj.Rank[i].rank = obj.Teams[it - 1].Rank;
                }

                rank++;
            }

            return obj;
        }

        public List<TeamCompetition> partial_ranking(List<TeamCompetition> teamCompetition)
        {
            for (int i = 0; i < teamCompetition.Count; i++)
            {
                int min_score = teamCompetition[i].Total_JumpOff_Penalty;
                int team_index = i;
                for (int j = i; j < teamCompetition.Count(); j++)
                {
                    if (teamCompetition[j].Total_JumpOff_Penalty < min_score)
                    {
                        min_score = teamCompetition[j].Total_JumpOff_Penalty;
                        team_index = j;
                    }
                }

                TeamCompetition tmp = new TeamCompetition();
                tmp = teamCompetition[team_index];
                teamCompetition[team_index] = teamCompetition[i];
                teamCompetition[i] = tmp;
            }

            // Assigning ranks to teams
            int position = teamCompetition[0].Rank;
            for (int i = 0; i < teamCompetition.Count();)
            {
                int it = i; // Temporary iterator without changing current index value
                float min_time = teamCompetition[it].Total_JumpOff_Time;
                int team_index = it;

                // If the team have same penalties then retrieve the team with lowest time
                while (it + 1 < teamCompetition.Count() && teamCompetition[it].Total_JumpOff_Penalty == teamCompetition[it + 1].Total_JumpOff_Penalty)
                {
                    if (teamCompetition[it + 1].Total_JumpOff_Time < min_time)
                    {
                        min_time = teamCompetition[it + 1].Total_JumpOff_Time;
                        team_index = it + 1;
                    }
                    it = it + 1;
                }

                // Move the team up-to current index 
                if (team_index != i)
                {
                    TeamCompetition tmp = new TeamCompetition();
                    tmp = teamCompetition[team_index];
                    teamCompetition[team_index] = teamCompetition[i];
                    teamCompetition[i] = tmp;
                }

                // Assign rank to current indexed team
                teamCompetition[i].Rank = position;
                i = i + 1;
                position++;
            }

            return teamCompetition;
        }

        // List of teams that participate in JumpOff
        public List<TeamCompetition> jumpoff_Teams(Serialization obj)
        {
            rank_Calculate.Rank_for_JumpOff(obj);
            tmp_result = new List<TeamCompetition>();
            for (int i = 0; i < obj.Teams.Count;)
            {
                int it = i; 
                bool flag = false;

                while (it + 1 < rank_Calculate.result.Count() && rank_Calculate.result[it].rank == rank_Calculate.result[it + 1].rank)
                {
                    if(flag == false)
                    {
                        tmp_result.Add(obj.Teams[it]);
                        flag = true;
                    }

                    it = it + 1;
                    tmp_result.Add(obj.Teams[it]);
                }

                if (it != i)
                {
                    i = it + 1;
                }

                else if (i == it)
                {
                    i++;
                }
            }

            return tmp_result;
        }
    }
}
