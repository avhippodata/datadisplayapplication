﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    public class ChangeRankingOrder
    {
        public Serialization DescendingRanking(Serialization obj)
        {
            for (int i = 0; i < obj.Teams.Count; i++)
            {
                int it = i;
                while (obj.Rank[obj.Teams.Count - 1 - i].teamName != obj.Teams[it].Name)
                {
                    it++;
                }
                if (it != i)
                {
                    TeamCompetition tmp_team = new TeamCompetition();
                    tmp_team = obj.Teams[i];
                    obj.Teams[i] = obj.Teams[it];
                    //tmp_obj.Teams[i].No = Constants.obj.Rank[i].rank;
                    obj.Teams[it] = tmp_team;
                }
            }

            return obj;
        }

        public Serialization AscendingRanking(Serialization obj)
        {
            try
            {
                for (int i = 0; i < obj.Rank.Count; i++)
                {
                    int it = 0;
                    while (it < obj.Teams.Count() && obj.Rank[i].teamName != obj.Teams[it].Name)
                    {
                        it++;
                    }
                    if (it != i)
                    {
                        TeamCompetition tmp_team = new TeamCompetition();
                        tmp_team = obj.Teams[i];
                        obj.Teams[i] = obj.Teams[it];
                        //tmp_obj.Teams[i].No = Constants.obj.Rank[i].rank;
                        obj.Teams[it] = tmp_team;
                    }
                }
                priority_order(obj);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Problem with ascending order of teams\n" + ex.Message);
            }

            return obj;
        }

        public void priority_order(Serialization obj)
        {
            try
            {
                for (int i = 0; i < obj.Event.noOfteamsForNextRound; i++)
                {
                    int it = i + 1;
                    int tmp_index = i;
                    int max = obj.Teams[i].No;
                    while (it < obj.Event.noOfteamsForNextRound && obj.Teams[i].Total_all_Penalty == obj.Teams[it].Total_all_Penalty)
                    {
                        if (obj.Teams[it].No > max)
                        {
                            max = obj.Teams[it].No;
                            tmp_index = it;
                        }

                        it += 1;
                    }

                    if (i != tmp_index)
                    {
                        TeamCompetition tmp_team = new TeamCompetition();
                        tmp_team = obj.Teams[i];
                        obj.Teams[i] = obj.Teams[tmp_index];
                        obj.Teams[tmp_index] = tmp_team;
                    }
                }
            }

            catch(Exception ex)
            {
                MessageBox.Show("Problem with prioritizing teams for next round\n" + ex.Message);
            }
            //return obj;
        }
    }
}