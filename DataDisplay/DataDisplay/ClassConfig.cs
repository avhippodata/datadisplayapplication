﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    public class ClassConfig
    {
        public bool TimingPortActive = false;

        public int TimingPort = 29001;

        public bool SharePortActive = false;

        public int SharePort = 22001;

        //private string _PathReiten = "C:\\REITEN";

        //private string _PathReports = "C:\\REITEN\\REPORTS";

        //public string DefaultStartReport = "";

        //public string DefaultResultReport = "";

        public bool DeveloperMode = false;

        public string DefaultPrintPort = "LPT1:";

        public string DefaultPrintFormat = "OHNERAND";

        public int PrintPageLines = 64;

        public int PrintTopLines = 0;

        public bool NotAus = false;

        public bool AsiaNames = true;

        public bool MoneyFloat = true;

        public string Currency = "EUR";

        public bool MoneyBeforeR1 = true;

        public int TimeDigits = 2;

        public int FaultDigits = -1;

        private string _PathTiming = "C:\\";

        private string _PathRiderDB = "C:\\REITEN\\DATA\\";

        private string _PathInternet = "C:\\BAZAR\\INTERNET\\";

        private string _PathShow = "C:\\BAZAR\\SHOW\\";

        private string _PathTV = "C:\\BAZAR\\TV\\";

        private string _PathSTA = "C:\\BAZAR\\STA\\";

        private string _PathHorseDB = "C:\\REITEN\\PFERDE\\";

        public string PathColorFile = "C:\\REITEN\\FARBEN.DAT";

        public string PathJudgeDB = "C:\\REITEN\\RIRA2006.LST";

        private string _PathReadFN = "C:\\BAZAR\\STA\\";

        public string PathQualiFile = "C:\\REITEN\\QUALP.MLS";

        private string _PathTemp = "C:\\TEMP\\";

        public string FooterFile = "C:\\REITEN\\HIPPOE.TXT";

        public string PrinterStartlist = "C:\\REITEN\\STARTP.MLS";

        public string PrinterResultlist = "C:\\REITEN\\PLATZP.MLS";

        public string PrinterRanking = "C:\\REITEN\\RANGP.MLS";

        private string _PathPDF = "C:\\PDF\\";

        public string _PathToris = "C:\\FN20\\ROBOSAVE\\";
 
        public string PathFlags = "";

        public bool AutoOpenPDF = true;

        private bool _UseUTF8 = false;

        public void Load()
        {
            this.TimingPort = Convert.ToInt32(this.LoadSetting("TimingPort"));
            this.SharePort = Convert.ToInt32(this.LoadSetting("SharePort"));
            if ((this.LoadSetting("TimingPortActive") == "True"))
            {
                this.TimingPortActive = true;
            }
            else
            {
                this.TimingPortActive = false;
            }

            if ((this.LoadSetting("SharePortActive") == "True"))
            {
                this.SharePortActive = true;
            }
            else
            {
                this.SharePortActive = false;
            }

            //this.PathReiten = this.LoadSetting("PathReiten");
            //this.PathReports = this.LoadSetting("PathReports");
            this.PathFlags = this.LoadSetting("PathFlags");
            //this.DefaultStartReport = this.LoadSetting("DefaultStartReport");
            //this.DefaultResultReport = this.LoadSetting("DefaultResultReport");
            this.DefaultPrintPort = this.LoadSetting("DefaultPrintPort");
            this.DefaultPrintFormat = this.LoadSetting("DefaultPrintFormat");
            this.PrintPageLines = Convert.ToInt32(this.LoadSetting("PrintPageLines"));
            this.PrintTopLines = Convert.ToInt32(this.LoadSetting("PrintTopLines"));
            this.TimeDigits = Convert.ToInt32(this.LoadSetting("TimeDigits"));
            this.FaultDigits = Convert.ToInt32(this.LoadSetting("FaultDigits"));
            if (((this.TimeDigits == 0)
                        && (this.FaultDigits == 0)))
            {
                this.TimeDigits = 2;
                this.FaultDigits = 2;
            }

            //this.JSPDiff = 0;
            //// Val(LoadSetting("JSPDiff"))
            //this.JSPDiffTotal = 5;
            //// Val(LoadSetting("JSPDiffTotal"))
            //this.LanguageSystem = this.LoadSetting("LanguageSystem");
            //if ((this.LanguageSystem == ""))
            //{
            //    this.LanguageSystem = "deutsch";
            //}

            if ((this.LoadSetting("AutoOpenPDF") == "True"))
            {
                this.AutoOpenPDF = true;
            }
            else
            {
                this.AutoOpenPDF = false;
            }

            if ((this.LoadSetting("DeveloperMode") == "True"))
            {
                this.DeveloperMode = true;
            }
            else
            {
                this.DeveloperMode = false;
            }

            //if ((this.LoadSetting("UseHippoDrive") == "True"))
            //{
            //    this.UseHippoDrive = true;
            //}
            //else
            //{
            //    this.UseHippoDrive = false;
            //}

            if ((this.LoadSetting("UseUTF8") == "True"))
            {
                this.UseUTF8 = true;
            }
            else
            {
                this.UseUTF8 = false;
            }

            if ((this.TimingPort == 0))
            {
                this.TimingPort = 29001;
            }

            //if ((this.PathReiten == ""))
            //{
            //    this.PathReiten = "C:\\REITEN";
            //}

            //if ((this.PathReports == ""))
            //{
            //    this.PathReports = "C:\\REITEN\\REPORTS\\";
            //}

          
        }

        public void Save()
        {
            this.SaveSetting("TimingPort", TimingPort.ToString());
            this.SaveSetting("TimingPortActive", TimingPortActive.ToString());
            this.SaveSetting("SharePort", SharePort.ToString());
            this.SaveSetting("SharePortActive", SharePortActive.ToString());    
            this.SaveSetting("UseUTF8", UseUTF8.ToString());
            SaveSetting("DeveloperMode", DeveloperMode.ToString());
        }

        private void SaveSetting(string name, string value)
        {
            try
            {
                Application.UserAppDataRegistry.SetValue(name, value);
            }
            catch (Exception ex)
            {
                Console.WriteLine(("Fehler: " + ex.Message));
            }

        }

        private string LoadSetting(string name)
        {
            try
            {
                if (!(Application.UserAppDataRegistry.GetValue(name) == null))
                {
                    return ((string)(Application.UserAppDataRegistry.GetValue(name)));
                }
                else
                {
                    return String.Empty;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(("Fehler: " + ex.Message));
                return String.Empty;
            }

        }

        //string PathReiten
        //{
        //    get
        //    {
        //        return _PathReiten;
        //    }
        //    set
        //    {
        //        _PathReiten = this.CheckPath(value);
        //    }
        //}

        //int JSPDiff
        //{
        //    get
        //    {
        //        return _JSPDiff;
        //    }
        //    set
        //    {
        //        _JSPDiff = Math.Abs(value);
        //    }
        //}

        //int JSPDiffTotal
        //{
        //    get
        //    {
        //        return _JSPDiffTotal;
        //    }
        //    set
        //    {
        //        _JSPDiffTotal = Math.Abs(value);
        //    }
        //}

        //string PathSuccessDB
        //{
        //    get
        //    {
        //        return _PathSuccessDB;
        //    }
        //    set
        //    {
        //        _PathSuccessDB = this.CheckPath(value);
        //    }
        //}

        //string PathDressageTest
        //{
        //    get
        //    {
        //        return _PathDressageTest;
        //    }
        //    set
        //    {
        //        _PathDressageTest = this.CheckPath(value);
        //    }
        //}

        //string PathPrintCenter
        //{
        //    get
        //    {
        //        return _PathPrintCenter;
        //    }
        //    set
        //    {
        //        _PathPrintCenter = this.CheckPath(value);
        //    }
        //}

        string PathTiming
        {
            get
            {
                return _PathTiming;
            }
            set
            {
                _PathTiming = this.CheckPath(value);
            }
        }

        string PathRiderDB
        {
            get
            {
                return _PathRiderDB;
            }
            set
            {
                _PathRiderDB = this.CheckPath(value);
            }
        }

        string PathInternet
        {
            get
            {
                return _PathInternet;
            }
            set
            {
                _PathInternet = this.CheckPath(value);
            }
        }

        string PathShow
        {
            get
            {
                return _PathShow;
            }
            set
            {
                _PathShow = this.CheckPath(value);
            }
        }

        string PathTV
        {
            get
            {
                return _PathTV;
            }
            set
            {
                _PathTV = this.CheckPath(value);
            }
        }

        string PathSTA
        {
            get
            {
                return _PathSTA;
            }
            set
            {
                _PathSTA = this.CheckPath(value);
            }
        }

        string PathHorseDB
        {
            get
            {
                return _PathHorseDB;
            }
            set
            {
                _PathHorseDB = this.CheckPath(value);
            }
        }

        string PathReadFN
        {
            get
            {
                return _PathReadFN;
            }
            set
            {
                _PathReadFN = this.CheckPath(value);
            }
        }

        string PathTemp
        {
            get
            {
                return _PathTemp;
            }
            set
            {
                _PathTemp = this.CheckPath(value);
            }
        }

        string PathPDF
        {
            get
            {
                string _s;
                if (((_PathPDF.Substring(0, 1) == "/")
                            || (_PathPDF.Substring(0, 1) == "\\")))
                {
                    _s = "C:";
                }
                else
                {
                    _s = "";
                }

                return (_s + _PathPDF);
            }
            set
            {
                _PathPDF = this.CheckPath(value);
            }
        }

        string PathToris
        {
            get
            {
                return _PathToris;
            }
            set
            {
                _PathToris = this.CheckPath(value);
            }
        }

        //string PathDressageExport
        //{
        //    get
        //    {
        //        return _PathDressageExport;
        //    }
        //    set
        //    {
        //        _PathDressageExport = this.CheckPath(value);
        //    }
        //}

        //string PathPrinterFiles
        //{
        //    get
        //    {
        //        return _PathPrinterFiles;
        //    }
        //    set
        //    {
        //        _PathPrinterFiles = this.CheckPath(value);
        //    }
        //}

        //string PathInternetDB
        //{
        //    get
        //    {
        //        return _PathInternetDB;
        //    }
        //    set
        //    {
        //        _PathInternetDB = this.CheckPath(value);
        //    }
        //}

        //string LanguageOutput
        //{
        //    get
        //    {
        //        return _LanguageOutput;
        //    }
        //    set
        //    {
        //        _LanguageOutput = value;
        //    }
        //}

        //string LanguageSystem
        //{
        //    get
        //    {
        //        return _LanguageSystem;
        //    }
        //    set
        //    {
        //        _LanguageSystem = value;
        //    }
        //}

        //string PathReports
        //{
        //    get
        //    {
        //        return _PathReports;
        //    }
        //    set
        //    {
        //        _PathReports = this.CheckPath(value);
        //    }
        //}

        //bool UseHippoDrive
        //{
        //    get
        //    {
        //        return _UseHippoDrive;
        //    }
        //    set
        //    {
        //        _UseHippoDrive = value;
        //    }
        //}

        bool UseUTF8
        {
            get
            {
                return _UseUTF8;
            }
            set
            {
                _UseUTF8 = value;
            }
        }

        private string CheckPath(string path)
        {
            if (path == "")
                return path;

            if (path.Substring(path.Length - 1) != @"\")
                path += @"\";
            while (path.IndexOf(@"\\") > 0)
                path = path.Replace(@"\\", @"\");
            return path;
        }

        public bool ExceptAsiaNames(string nation)
        {
            if ((nation.ToUpper() == "TPE"))
            {
                return true;
            }

            if ((nation.ToUpper() == "CHN"))
            {
                return true;
            }

            return false;
        }

        //

        private decimal _TimeAllowedRound;

        public decimal TimeAllowedRound
        {
            get
            {
                return _TimeAllowedRound;
            }

            set
            {
                _TimeAllowedRound = value;
            }
        }
    }
}
