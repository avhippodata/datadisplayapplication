﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDisplay
{
    public class ClassStarter
    {
        public string Reiter; // lokale Kopie
        public string Pferd; // lokale Kopie
        public string KNR; // lokale Kopie
        public string Nation; // lokale Kopie

        public void Assign(ClassStarter x)
        {
            Reiter = x.Reiter;
            Pferd = x.Pferd;
            KNR = x.KNR;
            Nation = x.Nation;
        }
    }
}
