﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDisplay
{
    public class ClassTiming
    {
        public string Status = ""; // lokale Kopie
        public decimal Zeit = 0; // lokale Kopie
        public decimal Differenz = 0;
        public decimal Fehler = 0; // lokale Kopie
        public decimal FehlerTotal = 0; // lokale Kopie
        public decimal Zeitfehler = 0; // lokale Kopie
        public decimal ZeitTotal = 0; // lokale Kopie
        public decimal ZeitVorgabe = 0; // lokale Kopie
        public decimal FehlerVorgabe = 0; // lokale Kopie
        public decimal Umlauf = 1; // lokale Kopie
        public string LS = ""; // lokale Kopie
        public decimal Erstellt = 0;

        public void Assign(ClassTiming x)
        {
            Status = x.Status;
            Zeit = x.Zeit;
            Fehler = x.Fehler;
            FehlerTotal = x.FehlerTotal;
            Differenz = x.Differenz;
            Zeitfehler = x.Zeitfehler;
            ZeitVorgabe = x.ZeitVorgabe;
            FehlerVorgabe = x.FehlerVorgabe;
            ZeitTotal = x.ZeitTotal;
            Umlauf = x.Umlauf;
            LS = x.LS;
            Erstellt = x.Erstellt;
        }


        public bool Compare(ClassTiming x)
        {
            bool CompareRet;
            CompareRet = true;
            if (Status != x.Status)
                CompareRet = false;
            if (Zeit != x.Zeit)
                CompareRet = false;
            if (Fehler != x.Fehler)
                CompareRet = false;
            if (FehlerTotal != x.FehlerTotal)
                CompareRet = false;
            if (Zeitfehler != x.Zeitfehler)
                CompareRet = false;
            if (ZeitVorgabe != x.ZeitVorgabe)
                CompareRet = false;
            if (Zeitfehler != x.Zeitfehler)
                CompareRet = false;
            if (ZeitTotal != x.ZeitTotal)
                CompareRet = false;
            if (FehlerVorgabe != x.FehlerVorgabe)
                CompareRet = false;
            if (Umlauf != x.Umlauf)
                CompareRet = false;
            return CompareRet;
        }
    }
}
