﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace DataDisplay
{
    public class ClassTimingData
    {
        public ClassStarter Starter = new ClassStarter();
        public ClassTiming Timing = new ClassTiming();
        public ClassTiming LetztesErgebnis = new ClassTiming();
        public bool Verarbeitet = false;
        public bool Aktualisiert = false;
        public decimal PhaseTime;
        //ClassConfig Config = new ClassConfig();

        public void AssignJSON(string text)
        {
            if (text.Length < 10)
                return;
            //Debug.Print(text);

            bool cmp = false;
            ClassTimingData ss;
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(text));
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(this.GetType());

            try
            {
                //int _fno;
                ss = (ClassTimingData)serializer.ReadObject(ms);
                Starter.Assign(ss.Starter);
                Timing.Assign(ss.Timing);

                if (Timing.Status != "ImZiel")
                    Verarbeitet = false;
                else
                {
                    if (Timing.Erstellt != LetztesErgebnis.Erstellt)
                    {
                        cmp = LetztesErgebnis.Compare(ss.Timing);
                        if (Timing.Erstellt > LetztesErgebnis.Erstellt + 10)
                            cmp = false;
                        LetztesErgebnis.Assign(ss.Timing);
                        if (cmp == false)
                        {
                            Verarbeitet = false;
                            Aktualisiert = true;
                        }
                    }
                    if (Timing.Umlauf == Convert.ToDecimal(1.5) & (Timing.Fehler == 0 & Timing.Zeitfehler == 0))
                        PhaseTime = Timing.Zeit;
                }

                ms.Close();
                ms.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                //PutFile(Config.PathReiten + @"\error.log", ex.Message + Constants.vbCrLf + text);
            }

            Debug.Print("Aktualisiert: " + Aktualisiert);
            Debug.Print("Verarbeitet: " + Verarbeitet);
            Debug.Print(Convert.ToString(LetztesErgebnis.Zeit));
            Debug.Print(text);
        }

        public bool PutFile(string filename, string content)
        {
            bool PutFileRet;
            PutFileRet = false;
            try
            {
                File.WriteAllText(filename, content);
                PutFileRet = true;
            }
            catch (Exception E)
            {
                Console.WriteLine(E);
                //Fehlerbehandlung[E];
                //return default;
            }

            return PutFileRet;
        }
    }

}
