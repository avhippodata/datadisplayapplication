﻿namespace DataDisplay
{
    partial class CompetitionSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.singleRiderradioButton = new System.Windows.Forms.RadioButton();
            this.allRidersradioButton = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.noradioButton = new System.Windows.Forms.RadioButton();
            this.yesradioButton = new System.Windows.Forms.RadioButton();
            this.noOfRidersLabel = new System.Windows.Forms.Label();
            this.okButton = new DevExpress.XtraEditors.SimpleButton();
            this.withJUmpOffLabel = new System.Windows.Forms.Label();
            this.noOfTeamsLabel = new System.Windows.Forms.Label();
            this.NoOfTeamsForNextRoundtextEdit = new DevExpress.XtraEditors.TextEdit();
            this.competitionSettingLabel = new DevExpress.XtraEditors.LabelControl();
            this.timeAllowedForRoundstextEdit = new DevExpress.XtraEditors.TextEdit();
            this.cancelButton = new DevExpress.XtraEditors.SimpleButton();
            this.noOfRoundsLabel = new System.Windows.Forms.Label();
            this.timeAllowedLabel = new System.Windows.Forms.Label();
            this.noOfRoundstextEdit = new DevExpress.XtraEditors.TextEdit();
            this.timeAllowedForJumpofftextEdit = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.noOfResultCountstextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TeamsForNextRoundPanel = new System.Windows.Forms.Panel();
            this.bestradioButton = new System.Windows.Forms.RadioButton();
            this.allradioButton = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NoOfTeamsForNextRoundtextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeAllowedForRoundstextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.noOfRoundstextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeAllowedForJumpofftextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.noOfResultCountstextEdit.Properties)).BeginInit();
            this.TeamsForNextRoundPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.singleRiderradioButton);
            this.panel2.Controls.Add(this.allRidersradioButton);
            this.panel2.Location = new System.Drawing.Point(364, 378);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(159, 31);
            this.panel2.TabIndex = 37;
            // 
            // singleRiderradioButton
            // 
            this.singleRiderradioButton.AutoSize = true;
            this.singleRiderradioButton.Location = new System.Drawing.Point(5, 6);
            this.singleRiderradioButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.singleRiderradioButton.Name = "singleRiderradioButton";
            this.singleRiderradioButton.Size = new System.Drawing.Size(68, 21);
            this.singleRiderradioButton.TabIndex = 24;
            this.singleRiderradioButton.TabStop = true;
            this.singleRiderradioButton.Text = "Single";
            this.singleRiderradioButton.UseVisualStyleBackColor = true;
            // 
            // allRidersradioButton
            // 
            this.allRidersradioButton.AutoSize = true;
            this.allRidersradioButton.Location = new System.Drawing.Point(98, 6);
            this.allRidersradioButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.allRidersradioButton.Name = "allRidersradioButton";
            this.allRidersradioButton.Size = new System.Drawing.Size(44, 21);
            this.allRidersradioButton.TabIndex = 23;
            this.allRidersradioButton.TabStop = true;
            this.allRidersradioButton.Text = "All";
            this.allRidersradioButton.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.noradioButton);
            this.panel1.Controls.Add(this.yesradioButton);
            this.panel1.Location = new System.Drawing.Point(364, 322);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(159, 31);
            this.panel1.TabIndex = 36;
            // 
            // noradioButton
            // 
            this.noradioButton.AutoSize = true;
            this.noradioButton.Location = new System.Drawing.Point(98, 6);
            this.noradioButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.noradioButton.Name = "noradioButton";
            this.noradioButton.Size = new System.Drawing.Size(47, 21);
            this.noradioButton.TabIndex = 21;
            this.noradioButton.TabStop = true;
            this.noradioButton.Text = "No";
            this.noradioButton.UseVisualStyleBackColor = true;
            // 
            // yesradioButton
            // 
            this.yesradioButton.AutoSize = true;
            this.yesradioButton.Location = new System.Drawing.Point(4, 6);
            this.yesradioButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.yesradioButton.Name = "yesradioButton";
            this.yesradioButton.Size = new System.Drawing.Size(53, 21);
            this.yesradioButton.TabIndex = 20;
            this.yesradioButton.TabStop = true;
            this.yesradioButton.Text = "Yes";
            this.yesradioButton.UseVisualStyleBackColor = true;
            // 
            // noOfRidersLabel
            // 
            this.noOfRidersLabel.AutoSize = true;
            this.noOfRidersLabel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.noOfRidersLabel.ForeColor = System.Drawing.Color.Black;
            this.noOfRidersLabel.Location = new System.Drawing.Point(154, 384);
            this.noOfRidersLabel.Name = "noOfRidersLabel";
            this.noOfRidersLabel.Size = new System.Drawing.Size(187, 21);
            this.noOfRidersLabel.TabIndex = 38;
            this.noOfRidersLabel.Text = "No of riders for jump off";
            // 
            // okButton
            // 
            this.okButton.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.okButton.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.okButton.Appearance.ForeColor = System.Drawing.Color.Black;
            this.okButton.Appearance.Options.UseBackColor = true;
            this.okButton.Appearance.Options.UseFont = true;
            this.okButton.Appearance.Options.UseForeColor = true;
            this.okButton.Location = new System.Drawing.Point(624, 505);
            this.okButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(100, 27);
            this.okButton.TabIndex = 32;
            this.okButton.Text = "Ok";
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // withJUmpOffLabel
            // 
            this.withJUmpOffLabel.AutoSize = true;
            this.withJUmpOffLabel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.withJUmpOffLabel.ForeColor = System.Drawing.Color.Black;
            this.withJUmpOffLabel.Location = new System.Drawing.Point(268, 328);
            this.withJUmpOffLabel.Name = "withJUmpOffLabel";
            this.withJUmpOffLabel.Size = new System.Drawing.Size(73, 21);
            this.withJUmpOffLabel.TabIndex = 35;
            this.withJUmpOffLabel.Text = "Jump off";
            // 
            // noOfTeamsLabel
            // 
            this.noOfTeamsLabel.AutoSize = true;
            this.noOfTeamsLabel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.noOfTeamsLabel.ForeColor = System.Drawing.Color.Black;
            this.noOfTeamsLabel.Location = new System.Drawing.Point(132, 227);
            this.noOfTeamsLabel.Name = "noOfTeamsLabel";
            this.noOfTeamsLabel.Size = new System.Drawing.Size(209, 21);
            this.noOfTeamsLabel.TabIndex = 33;
            this.noOfTeamsLabel.Text = "No of teams for next round";
            // 
            // NoOfTeamsForNextRoundtextEdit
            // 
            this.NoOfTeamsForNextRoundtextEdit.Location = new System.Drawing.Point(364, 225);
            this.NoOfTeamsForNextRoundtextEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.NoOfTeamsForNextRoundtextEdit.Name = "NoOfTeamsForNextRoundtextEdit";
            this.NoOfTeamsForNextRoundtextEdit.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NoOfTeamsForNextRoundtextEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.NoOfTeamsForNextRoundtextEdit.Properties.Appearance.Options.UseFont = true;
            this.NoOfTeamsForNextRoundtextEdit.Properties.Appearance.Options.UseForeColor = true;
            this.NoOfTeamsForNextRoundtextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.NoOfTeamsForNextRoundtextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.NoOfTeamsForNextRoundtextEdit.Size = new System.Drawing.Size(159, 26);
            this.NoOfTeamsForNextRoundtextEdit.TabIndex = 34;
            // 
            // competitionSettingLabel
            // 
            this.competitionSettingLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.competitionSettingLabel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.competitionSettingLabel.Appearance.Options.UseFont = true;
            this.competitionSettingLabel.Appearance.Options.UseForeColor = true;
            this.competitionSettingLabel.Location = new System.Drawing.Point(97, 35);
            this.competitionSettingLabel.Name = "competitionSettingLabel";
            this.competitionSettingLabel.Size = new System.Drawing.Size(172, 21);
            this.competitionSettingLabel.TabIndex = 31;
            this.competitionSettingLabel.Text = "Competition Setting";
            // 
            // timeAllowedForRoundstextEdit
            // 
            this.timeAllowedForRoundstextEdit.EditValue = "";
            this.timeAllowedForRoundstextEdit.Location = new System.Drawing.Point(364, 78);
            this.timeAllowedForRoundstextEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.timeAllowedForRoundstextEdit.Name = "timeAllowedForRoundstextEdit";
            this.timeAllowedForRoundstextEdit.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeAllowedForRoundstextEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.timeAllowedForRoundstextEdit.Properties.Appearance.Options.UseFont = true;
            this.timeAllowedForRoundstextEdit.Properties.Appearance.Options.UseForeColor = true;
            this.timeAllowedForRoundstextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.timeAllowedForRoundstextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.timeAllowedForRoundstextEdit.Size = new System.Drawing.Size(159, 26);
            this.timeAllowedForRoundstextEdit.TabIndex = 30;
            // 
            // cancelButton
            // 
            this.cancelButton.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cancelButton.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.Appearance.ForeColor = System.Drawing.Color.Black;
            this.cancelButton.Appearance.Options.UseBackColor = true;
            this.cancelButton.Appearance.Options.UseFont = true;
            this.cancelButton.Appearance.Options.UseForeColor = true;
            this.cancelButton.Location = new System.Drawing.Point(499, 505);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(100, 27);
            this.cancelButton.TabIndex = 28;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // noOfRoundsLabel
            // 
            this.noOfRoundsLabel.AutoSize = true;
            this.noOfRoundsLabel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.noOfRoundsLabel.ForeColor = System.Drawing.Color.Black;
            this.noOfRoundsLabel.Location = new System.Drawing.Point(232, 130);
            this.noOfRoundsLabel.Name = "noOfRoundsLabel";
            this.noOfRoundsLabel.Size = new System.Drawing.Size(109, 21);
            this.noOfRoundsLabel.TabIndex = 26;
            this.noOfRoundsLabel.Text = "No of Rounds";
            // 
            // timeAllowedLabel
            // 
            this.timeAllowedLabel.AutoSize = true;
            this.timeAllowedLabel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.timeAllowedLabel.ForeColor = System.Drawing.Color.Black;
            this.timeAllowedLabel.Location = new System.Drawing.Point(152, 83);
            this.timeAllowedLabel.Name = "timeAllowedLabel";
            this.timeAllowedLabel.Size = new System.Drawing.Size(189, 21);
            this.timeAllowedLabel.TabIndex = 29;
            this.timeAllowedLabel.Text = "Time allowed for rounds";
            // 
            // noOfRoundstextEdit
            // 
            this.noOfRoundstextEdit.Location = new System.Drawing.Point(364, 128);
            this.noOfRoundstextEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.noOfRoundstextEdit.Name = "noOfRoundstextEdit";
            this.noOfRoundstextEdit.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noOfRoundstextEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.noOfRoundstextEdit.Properties.Appearance.Options.UseFont = true;
            this.noOfRoundstextEdit.Properties.Appearance.Options.UseForeColor = true;
            this.noOfRoundstextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.noOfRoundstextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.noOfRoundstextEdit.Size = new System.Drawing.Size(159, 26);
            this.noOfRoundstextEdit.TabIndex = 27;
            // 
            // timeAllowedForJumpofftextEdit
            // 
            this.timeAllowedForJumpofftextEdit.EditValue = "";
            this.timeAllowedForJumpofftextEdit.Location = new System.Drawing.Point(364, 432);
            this.timeAllowedForJumpofftextEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.timeAllowedForJumpofftextEdit.Name = "timeAllowedForJumpofftextEdit";
            this.timeAllowedForJumpofftextEdit.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeAllowedForJumpofftextEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.timeAllowedForJumpofftextEdit.Properties.Appearance.Options.UseFont = true;
            this.timeAllowedForJumpofftextEdit.Properties.Appearance.Options.UseForeColor = true;
            this.timeAllowedForJumpofftextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.timeAllowedForJumpofftextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.timeAllowedForJumpofftextEdit.Size = new System.Drawing.Size(159, 26);
            this.timeAllowedForJumpofftextEdit.TabIndex = 40;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(141, 434);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(200, 21);
            this.label1.TabIndex = 39;
            this.label1.Text = "Time allowed for jump off";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(100, 276);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(241, 21);
            this.label2.TabIndex = 41;
            this.label2.Text = "No results counts of each team";
            // 
            // noOfResultCountstextEdit
            // 
            this.noOfResultCountstextEdit.EditValue = "3";
            this.noOfResultCountstextEdit.Location = new System.Drawing.Point(364, 271);
            this.noOfResultCountstextEdit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.noOfResultCountstextEdit.Name = "noOfResultCountstextEdit";
            this.noOfResultCountstextEdit.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noOfResultCountstextEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.noOfResultCountstextEdit.Properties.Appearance.Options.UseFont = true;
            this.noOfResultCountstextEdit.Properties.Appearance.Options.UseForeColor = true;
            this.noOfResultCountstextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.noOfResultCountstextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.noOfResultCountstextEdit.Size = new System.Drawing.Size(159, 26);
            this.noOfResultCountstextEdit.TabIndex = 42;
            // 
            // TeamsForNextRoundPanel
            // 
            this.TeamsForNextRoundPanel.Controls.Add(this.bestradioButton);
            this.TeamsForNextRoundPanel.Controls.Add(this.allradioButton);
            this.TeamsForNextRoundPanel.Location = new System.Drawing.Point(364, 173);
            this.TeamsForNextRoundPanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TeamsForNextRoundPanel.Name = "TeamsForNextRoundPanel";
            this.TeamsForNextRoundPanel.Size = new System.Drawing.Size(159, 31);
            this.TeamsForNextRoundPanel.TabIndex = 38;
            // 
            // bestradioButton
            // 
            this.bestradioButton.AutoSize = true;
            this.bestradioButton.Location = new System.Drawing.Point(5, 6);
            this.bestradioButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bestradioButton.Name = "bestradioButton";
            this.bestradioButton.Size = new System.Drawing.Size(87, 21);
            this.bestradioButton.TabIndex = 21;
            this.bestradioButton.TabStop = true;
            this.bestradioButton.Text = "Best time";
            this.bestradioButton.UseVisualStyleBackColor = true;
            // 
            // allradioButton
            // 
            this.allradioButton.AutoSize = true;
            this.allradioButton.Location = new System.Drawing.Point(98, 6);
            this.allradioButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.allradioButton.Name = "allradioButton";
            this.allradioButton.Size = new System.Drawing.Size(44, 21);
            this.allradioButton.TabIndex = 20;
            this.allradioButton.TabStop = true;
            this.allradioButton.Text = "All";
            this.allradioButton.UseVisualStyleBackColor = true;
            this.allradioButton.CheckedChanged += new System.EventHandler(this.allradioButton_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 10F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(172, 179);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 21);
            this.label3.TabIndex = 37;
            this.label3.Text = "Teams for next round";
            // 
            // CompetitionSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 559);
            this.Controls.Add(this.TeamsForNextRoundPanel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.noOfResultCountstextEdit);
            this.Controls.Add(this.timeAllowedForJumpofftextEdit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.noOfRidersLabel);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.withJUmpOffLabel);
            this.Controls.Add(this.noOfTeamsLabel);
            this.Controls.Add(this.NoOfTeamsForNextRoundtextEdit);
            this.Controls.Add(this.competitionSettingLabel);
            this.Controls.Add(this.timeAllowedForRoundstextEdit);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.noOfRoundsLabel);
            this.Controls.Add(this.timeAllowedLabel);
            this.Controls.Add(this.noOfRoundstextEdit);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "CompetitionSetting";
            this.Text = "CompetitionSetting";
            this.Load += new System.EventHandler(this.CompetitionSetting_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NoOfTeamsForNextRoundtextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeAllowedForRoundstextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.noOfRoundstextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeAllowedForJumpofftextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.noOfResultCountstextEdit.Properties)).EndInit();
            this.TeamsForNextRoundPanel.ResumeLayout(false);
            this.TeamsForNextRoundPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton singleRiderradioButton;
        private System.Windows.Forms.RadioButton allRidersradioButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton noradioButton;
        private System.Windows.Forms.RadioButton yesradioButton;
        private System.Windows.Forms.Label noOfRidersLabel;
        private DevExpress.XtraEditors.SimpleButton okButton;
        private System.Windows.Forms.Label withJUmpOffLabel;
        private System.Windows.Forms.Label noOfTeamsLabel;
        private DevExpress.XtraEditors.TextEdit NoOfTeamsForNextRoundtextEdit;
        private DevExpress.XtraEditors.LabelControl competitionSettingLabel;
        private DevExpress.XtraEditors.TextEdit timeAllowedForRoundstextEdit;
        private DevExpress.XtraEditors.SimpleButton cancelButton;
        private System.Windows.Forms.Label noOfRoundsLabel;
        private System.Windows.Forms.Label timeAllowedLabel;
        private DevExpress.XtraEditors.TextEdit noOfRoundstextEdit;
        private DevExpress.XtraEditors.TextEdit timeAllowedForJumpofftextEdit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit noOfResultCountstextEdit;
        private System.Windows.Forms.Panel TeamsForNextRoundPanel;
        private System.Windows.Forms.RadioButton bestradioButton;
        private System.Windows.Forms.RadioButton allradioButton;
        private System.Windows.Forms.Label label3;
    }
}