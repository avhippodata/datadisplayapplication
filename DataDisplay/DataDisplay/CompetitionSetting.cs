﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    public partial class CompetitionSetting : Form
    {
        public CompetitionSetting()
        {
            InitializeComponent();
        }

        private void CompetitionSetting_Load(object sender, EventArgs e)
        {
            try
            {
                timeAllowedForRoundstextEdit.Text = Convert.ToString(NationCup.obj.Event.timeAllowedForRounds);
                noOfRoundstextEdit.Text = Convert.ToString(NationCup.obj.Event.totalRounds);
                NoOfTeamsForNextRoundtextEdit.Text = Convert.ToString(NationCup.obj.Event.noOfteamsForNextRound);
                noOfResultCountstextEdit.Text = Convert.ToString(NationCup.obj.Event.noOfResultCounts);

                //if (Convert.ToInt32(timeAllowedForRoundstextEdit.Text) > 0)
                //{
                //    timeAllowedForRoundstextEdit.Text = Convert.ToString(NationCup.obj.Event.timeAllowedForRounds);
                //    timeAllowedForRoundstextEdit.Enabled = false;
                //}
                //else
                //{
                //    timeAllowedForRoundstextEdit.Text = Convert.ToString(NationCup.obj.Event.timeAllowedForRounds);
                //    timeAllowedForRoundstextEdit.Enabled = true;
                //}

                if (NationCup.obj.Event.teamsForNextRound == true)
                {
                    bestradioButton.Checked = true;
                    NoOfTeamsForNextRoundtextEdit.Text = Convert.ToString(NationCup.obj.Event.noOfteamsForNextRound);
                }
                else
                {
                    allradioButton.Checked = true;
                    NoOfTeamsForNextRoundtextEdit.Text = NationCup.obj.Teams.Count.ToString();
                    //NoOfTeamsForNextRoundtextEdit.Enabled = false;
                }

                if (NationCup.obj.Event.jumpOff == true)
                    yesradioButton.Checked = true;
                else
                    noradioButton.Checked = true;

                if (NationCup.obj.Event.ridersForJumpOff == true)
                    singleRiderradioButton.Checked = true;
                else
                    allRidersradioButton.Checked = true;

                timeAllowedForJumpofftextEdit.Text = Convert.ToString(NationCup.obj.Event.timeAllowedForJumpoff);
            }

            catch(Exception ex)
            {
                MessageBox.Show("Problem with loading competition settings, please check previously saved data of the JSON!\n" + ex.Message);
            }
           
        }

        UpdateJson updateJson = new UpdateJson();
        private void okButton_Click(object sender, EventArgs e)
        {
            try
            {
                NationCup.obj.Event.timeAllowedForRounds = float.Parse(timeAllowedForRoundstextEdit.Text, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                NationCup.obj.Event.totalRounds = Convert.ToInt32(noOfRoundstextEdit.Text);
                //NationCup.obj.Event.noOfteamsForNextRound = Convert.ToInt32(NoOfTeamsForNextRoundtextEdit.Text);
                NationCup.obj.Event.noOfResultCounts = Convert.ToInt32(noOfResultCountstextEdit.Text);
                NationCup.obj.Event.timeAllowedForJumpoff = Convert.ToInt32(timeAllowedForJumpofftextEdit.Text);

                if (bestradioButton.Checked)
                {
                    NationCup.obj.Event.teamsForNextRound = true;
                    NationCup.obj.Event.noOfteamsForNextRound = Convert.ToInt32(NoOfTeamsForNextRoundtextEdit.Text);
                }
                else 
                {
                    NationCup.obj.Event.teamsForNextRound = false;
                    NationCup.obj.Event.noOfteamsForNextRound = NationCup.obj.Teams.Count;
                }

                if (yesradioButton.Checked)
                    NationCup.obj.Event.jumpOff = true;
                else
                    NationCup.obj.Event.jumpOff = false;

                if (singleRiderradioButton.Checked)
                    NationCup.obj.Event.ridersForJumpOff = true;
                else
                    NationCup.obj.Event.ridersForJumpOff = false;

                // Json updates
                updateJson.modifyJson(NationCup.obj);
                this.Close();
                //Form.ActiveForm.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with saving cometition settings data, Please enter the data correctly!\n" + ex.Message);
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Form.ActiveForm.Close();
        }

        private void allradioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (allradioButton.Checked)            
                NoOfTeamsForNextRoundtextEdit.Text = NationCup.obj.Teams.Count.ToString();            
        }
    }
}
