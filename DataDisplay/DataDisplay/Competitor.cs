﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    [Serializable]
    public class Competitor
    {
        public int startPosition;
        public Athlete athlete;
        public Horse horse;

        public void setCompetitorInfo()
        {

            Constants.listOfIndivCompetitor = new List<Competitor>();
            for (int i = 0; i < Constants.numberOfLoop; i++)
            {
                Competitor competitor = new Competitor();
                competitor.getCompetitorInfo(i);
                Constants.listOfIndivCompetitor.Add(competitor);
            }
        }

        public void getCompetitorInfo(int x)
        {
            startPosition = x + 1;
            athlete = Constants.allAthelete[x];
            horse = Constants.allHorse[x];
        }
    }
}
