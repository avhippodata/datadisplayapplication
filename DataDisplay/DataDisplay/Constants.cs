﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DataDisplay
{
    public class Constants
    {
        public static string[] lines;                                           // reading all lines from sta file
        public static List<List<string>> reportInfo = new List<List<string>>(); // collect report and athlete-horse data into an array
        public static Array firstLine;
        public static int numberOfLoop;
        public static List<Horse> allHorse;                                     // collect all horses data into a list
        public static List<Athlete> allAthelete;                                // collect all athletes data into a list
        public static string sourceFilePath;                                    // current location of sta file in local system
        public static string Event_Json = Environment.GetFolderPath(Environment.SpecialFolder.Desktop); // location, where json format of athete data will be stored
        public static int lastIndexOfCsv;                                       // to get the last index where csv data ends and json data starts, so that we can read sta file further from this index
        public static string jsonString;
        public static RootObject jsonInfo = new RootObject();
        public static List<TeamCompetition> listOfTeamCompetition;  
        public static List<Competitor> listOfIndivCompetitor; 
        public static List<Ranking> listOfRanking;
        public static string[] generalSettingJsonFile;
        public static List<List<string>> generalSettingReportInfo = new List<List<string>>();
        public static string generalSettingJsonString;
        public static InternalJson generalSettingJsonInfo = new InternalJson();
        public static string internalJsonPath = System.IO.Path.Combine(Path.GetTempPath(), "dataDisplay" + ".json");
        public static List<DataTableR1> listOfDataTableR1;

        public static List<TeamCompetition> listOfResultR1;
        public static List<TeamCompetition> listOfResultR2;
        public static List<TeamCompetition> listOfFinalResult;
    }
}
