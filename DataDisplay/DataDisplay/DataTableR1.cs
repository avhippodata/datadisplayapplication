﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDisplay
{
    [Serializable]
    public class DataTableR1
    {
        public int Pos { get; set; }
        public string IOC { get; set; }
        public string CNR { get; set; }
        public string HorseName { get; set; }
        public string RiderName { get; set; }
        public string Round1_penalty { get; set; }
        public string Round1_time { get; set; }

        public List<string[]> g1;
        public List<string[]> g2;
        public List<string[]> g3;
        public List<string[]> g4;
     
        public void create_DataTableR1()
        {
            g1 = new List<string[]>();
            g2 = new List<string[]>();
            g3 = new List<string[]>();
            g4 = new List<string[]>();
            int it = 0;

            Constants.listOfDataTableR1 = new List<DataTableR1>();
            for (int i = 0; i < Constants.listOfTeamCompetition.Count; i++)
            {
                bool flag = false;
                for (int m = 0; m < Constants.listOfTeamCompetition[i].MemberDetails.Count; m++)
                {
                    if (Constants.listOfTeamCompetition[i].MemberDetails[m].CNR == null)
                        flag = true;
                }

                if (!flag)
                {
                    g1.Add(new string[] { "", Constants.listOfTeamCompetition[i].IocCode, Constants.listOfTeamCompetition[i].MemberDetails[0].CNR, Constants.listOfTeamCompetition[i].MemberDetails[0].HorseName, Constants.listOfTeamCompetition[i].MemberDetails[0].RiderName, Constants.listOfTeamCompetition[i].MemberDetails[0].Round1_Total });

                    g2.Add(new string[] { "", Constants.listOfTeamCompetition[i].IocCode, Constants.listOfTeamCompetition[i].MemberDetails[1].CNR, Constants.listOfTeamCompetition[i].MemberDetails[1].HorseName, Constants.listOfTeamCompetition[i].MemberDetails[1].RiderName, Constants.listOfTeamCompetition[i].MemberDetails[1].Round1_Total });

                    g3.Add(new string[] { "", Constants.listOfTeamCompetition[i].IocCode, Constants.listOfTeamCompetition[i].MemberDetails[2].CNR, Constants.listOfTeamCompetition[i].MemberDetails[2].HorseName, Constants.listOfTeamCompetition[i].MemberDetails[2].RiderName, Constants.listOfTeamCompetition[i].MemberDetails[2].Round1_Total });

                    g4.Add(new string[] { "", Constants.listOfTeamCompetition[i].IocCode, Constants.listOfTeamCompetition[i].MemberDetails[3].CNR, Constants.listOfTeamCompetition[i].MemberDetails[3].HorseName, Constants.listOfTeamCompetition[i].MemberDetails[3].RiderName, Constants.listOfTeamCompetition[i].MemberDetails[3].Round1_Total });
                }

                else
                {
                    g2.Add(new string[] { "", Constants.listOfTeamCompetition[i].IocCode, Constants.listOfTeamCompetition[i].MemberDetails[1].CNR, Constants.listOfTeamCompetition[i].MemberDetails[1].HorseName, Constants.listOfTeamCompetition[i].MemberDetails[1].RiderName, Constants.listOfTeamCompetition[i].MemberDetails[1].Round1_Total });

                    g3.Add(new string[] { "", Constants.listOfTeamCompetition[i].IocCode, Constants.listOfTeamCompetition[i].MemberDetails[2].CNR, Constants.listOfTeamCompetition[i].MemberDetails[2].HorseName, Constants.listOfTeamCompetition[i].MemberDetails[2].RiderName, Constants.listOfTeamCompetition[i].MemberDetails[2].Round1_Total });

                    g4.Add(new string[] { "", Constants.listOfTeamCompetition[i].IocCode, Constants.listOfTeamCompetition[i].MemberDetails[3].CNR, Constants.listOfTeamCompetition[i].MemberDetails[3].HorseName, Constants.listOfTeamCompetition[i].MemberDetails[3].RiderName, Constants.listOfTeamCompetition[i].MemberDetails[3].Round1_Total });
                }
            }

            for (int i = 0; i < g1.Count; i++)
            {
                it += 1;
                DataTableR1 dataTableR1 = new DataTableR1();
                dataTableR1.Pos = it;
                dataTableR1.IOC = g1[i][1];
                dataTableR1.CNR = g1[i][2];
                dataTableR1.HorseName = g1[i][3];
                dataTableR1.RiderName = g1[i][4];
                dataTableR1.Round1_penalty = g1[i][5];
                Constants.listOfDataTableR1.Add(dataTableR1);
            }
            for (int i = 0; i < g2.Count; i++)
            {
                it += 1;
                DataTableR1 dataTableR1 = new DataTableR1();
                dataTableR1.Pos = it;
                dataTableR1.IOC = g2[i][1];
                dataTableR1.CNR = g2[i][2];
                dataTableR1.HorseName = g2[i][3];
                dataTableR1.RiderName = g2[i][4];
                dataTableR1.Round1_penalty = g2[i][5];
                Constants.listOfDataTableR1.Add(dataTableR1);
            }
            for (int i = 0; i < g3.Count; i++)
            {
                it += 1;
                DataTableR1 dataTableR1 = new DataTableR1();
                dataTableR1.Pos = it;
                dataTableR1.IOC = g3[i][1];
                dataTableR1.CNR = g3[i][2];
                dataTableR1.HorseName = g3[i][3];
                dataTableR1.RiderName = g3[i][4];
                dataTableR1.Round1_penalty = g3[i][5];
                Constants.listOfDataTableR1.Add(dataTableR1);
            }
            for (int i = 0; i < g4.Count; i++)
            {
                it += 1;
                DataTableR1 dataTableR1 = new DataTableR1();
                dataTableR1.Pos = it;
                dataTableR1.IOC = g4[i][1];
                dataTableR1.CNR = g4[i][2];
                dataTableR1.HorseName = g4[i][3];
                dataTableR1.RiderName = g4[i][4];
                dataTableR1.Round1_penalty = g4[i][5];
                Constants.listOfDataTableR1.Add(dataTableR1);
            }

        }
    }
}
