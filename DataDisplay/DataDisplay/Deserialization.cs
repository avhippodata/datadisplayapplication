﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    public class RootObject
    {
        public Event Event { get; set; }
        public CompetitionData Competition { get; set; }
    }

    public class Event
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Start { get; set; }
        public int Days { get; set; }
    }

    public class CompetitionData
    {
        public string Nr { get; set; }
        public string Name { get; set; }
        public int Entries { get; set; }
        public CompetitionProperties Properties { get; set; }
        public List<object> Jury { get; set; }
        public List<CompetitionTeam> Teams { get; set; }
        public Header Header { get; set; }
        public Footer Footer { get; set; }
    }

    public class CompetitionProperties
    {
        public string Type { get; set; }
        public int Entry_fee { get; set; }
        public int Start_fee { get; set; }
        public int Prize_money { get; set; }
        public int Prize_payout { get; set; }
        public bool Prize_float { get; set; }
        public bool LPO { get; set; }
        public bool Relay { get; set; }
        public bool Team { get; set; }
        public bool Freestyle { get; set; }
        public bool AllowOutOfCompetition { get; set; }
        public bool LockStartingPos { get; set; }
        public string Starting_letter { get; set; }
        public string Judge_order { get; set; }
        public string Divided_by { get; set; }
        public string Currency { get; set; }
        public string Key { get; set; }
        public int TeamMembers { get; set; }
        public string Level { get; set; }
    }

    public class CompetitionTeam
    {
        public int Nr { get; set; }
        public string Name { get; set; }
        public string Nation { get; set; }
        public string Trainer { get; set; }
        public List<TeamMember> Members { get; set; }
    }

    public class TeamMember
    {
        public int CNR { get; set; }
    }

    public class Header
    {
        public string __invalid_name__1 { get; set; }
        public string __invalid_name__2 { get; set; }
        public string __invalid_name__3 { get; set; }
        public string __invalid_name__4 { get; set; }
        public string __invalid_name__5 { get; set; }
    }

    public class Footer
    {
        public string __invalid_name__1 { get; set; }
        public string __invalid_name__2 { get; set; }
        public string __invalid_name__3 { get; set; }
        public string __invalid_name__4 { get; set; }
        public string __invalid_name__5 { get; set; }
    }

    public class Deserialization
    {
        public void GetJsonData()
        {
            try
            {
                Constants.jsonInfo = null;
                Constants.jsonInfo = JsonConvert.DeserializeObject<RootObject>(Constants.jsonString);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with deserialization of JSON data from STA file!\n" + ex.Message);
            }
        }
    }
}
