﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDisplay
{
    public class DropParticipants
    {
        public void EliminateParticipant_Round1(Serialization obj)
        {
            for (int i = 0; i < obj.Teams.Count; i++)
            {
                if (obj.Teams[i].MemberDetails.Count == 4)
                {
                    bool flag = false;

                    for (int j = 0; j < obj.Teams[i].MemberDetails.Count; j++)
                    {
                        if (obj.Teams[i].MemberDetails[j].CNR == null)
                        {
                            flag = true;
                        }
                    }

                    if (flag == false)
                    {
                        string mx_penalty = obj.Teams[i].MemberDetails.Max(x => x.Round1_Penalty);

                        for (int j = 0; j < obj.Teams[i].MemberDetails.Count; j++)
                        {
                            if (obj.Teams[i].MemberDetails[j].Round1_Penalty != null && obj.Teams[i].MemberDetails[j].Round1_Penalty == mx_penalty)
                            {
                                obj.Teams[i].MemberDetails[j].Round1_Penalty = "(" + obj.Teams[i].MemberDetails[j].Round1_Penalty + ")";
                                break;
                            }
                        }
                    }
                }
            }
        }

        public void EliminateParticipant_Round2(Serialization obj)
        {
            for (int i = 0; i < obj.Teams.Count; i++)
            {
                if (obj.Teams[i].MemberDetails.Count == 4)
                {   
                    bool flag = false;

                    for (int j = 0; j < obj.Teams[i].MemberDetails.Count; j++)
                    {
                        if (obj.Teams[i].MemberDetails[j].Round2_Penalty.Contains("(") || obj.Teams[i].MemberDetails[j].Round2_Penalty.Contains(")") || obj.Teams[i].MemberDetails[j].Round2_Penalty.Contains("-") 
                            || obj.Teams[i].MemberDetails[j].Round2_Penalty.Contains("DNS") || obj.Teams[i].MemberDetails[j].Round2_Penalty.Contains("EL"))
                        {
                            flag = true;
                        }
                    }

                    if (flag == false)
                    {
                        string mx_penalty = obj.Teams[i].MemberDetails.Max(x => x.Round2_Penalty);
                        float mn_time = float.Parse(obj.Teams[i].MemberDetails.Min(x => x.Round2_Time));
                        int tmp = 0;

                        for (int j = 0; j < obj.Teams[i].MemberDetails.Count; j++)
                        {

                            if (obj.Teams[i].MemberDetails[j].Round2_Penalty == mx_penalty && float.Parse(obj.Teams[i].MemberDetails[j].Round2_Time) > mn_time)
                            {
                                mn_time = float.Parse(obj.Teams[i].MemberDetails[j].Round2_Time);
                                tmp = j;
                            }
                        }

                        obj.Teams[i].MemberDetails[tmp].Round2_Penalty = "(" + obj.Teams[i].MemberDetails[tmp].Round2_Penalty + ")";
                        obj.Teams[i].MemberDetails[tmp].Round2_Time = "(" + obj.Teams[i].MemberDetails[tmp].Round2_Time + ")";
                    }
                }
            }
        }
    }
}
