﻿namespace DataDisplay
{
    partial class GeneralSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.staBrowseButton = new DevExpress.XtraEditors.SimpleButton();
            this.staFolderTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.okButton = new DevExpress.XtraEditors.SimpleButton();
            this.tvFolderPathLabel = new DevExpress.XtraEditors.LabelControl();
            this.cancelButton = new DevExpress.XtraEditors.SimpleButton();
            this.showFolderPathLabel = new DevExpress.XtraEditors.LabelControl();
            this.tvFolderButton = new DevExpress.XtraEditors.SimpleButton();
            this.showFolderButton = new DevExpress.XtraEditors.SimpleButton();
            this.networkSetingLabel = new DevExpress.XtraEditors.LabelControl();
            this.TVFoldertextEdit = new DevExpress.XtraEditors.TextEdit();
            this.showFoldertextEdit = new DevExpress.XtraEditors.TextEdit();
            this.udpPortText = new DevExpress.XtraEditors.TextEdit();
            this.udpPortLabel = new System.Windows.Forms.Label();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.htcBrowseButton = new DevExpress.XtraEditors.SimpleButton();
            this.htcFolderTextEdit = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.staFolderTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TVFoldertextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.showFoldertextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udpPortText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.htcFolderTextEdit.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Appearance.BackColor = System.Drawing.Color.White;
            this.splitContainerControl1.Panel1.Appearance.Options.UseBackColor = true;
            this.splitContainerControl1.Panel1.Controls.Add(this.labelControl2);
            this.splitContainerControl1.Panel1.Controls.Add(this.htcBrowseButton);
            this.splitContainerControl1.Panel1.Controls.Add(this.htcFolderTextEdit);
            this.splitContainerControl1.Panel1.Controls.Add(this.labelControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.staBrowseButton);
            this.splitContainerControl1.Panel1.Controls.Add(this.staFolderTextEdit);
            this.splitContainerControl1.Panel1.Controls.Add(this.okButton);
            this.splitContainerControl1.Panel1.Controls.Add(this.tvFolderPathLabel);
            this.splitContainerControl1.Panel1.Controls.Add(this.cancelButton);
            this.splitContainerControl1.Panel1.Controls.Add(this.showFolderPathLabel);
            this.splitContainerControl1.Panel1.Controls.Add(this.tvFolderButton);
            this.splitContainerControl1.Panel1.Controls.Add(this.showFolderButton);
            this.splitContainerControl1.Panel1.Controls.Add(this.networkSetingLabel);
            this.splitContainerControl1.Panel1.Controls.Add(this.TVFoldertextEdit);
            this.splitContainerControl1.Panel1.Controls.Add(this.showFoldertextEdit);
            this.splitContainerControl1.Panel1.Controls.Add(this.udpPortText);
            this.splitContainerControl1.Panel1.Controls.Add(this.udpPortLabel);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Appearance.BackColor = System.Drawing.Color.White;
            this.splitContainerControl1.Panel2.Appearance.Options.UseBackColor = true;
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.PanelVisibility = DevExpress.XtraEditors.SplitPanelVisibility.Panel1;
            this.splitContainerControl1.Size = new System.Drawing.Size(572, 398);
            this.splitContainerControl1.SplitterPosition = 339;
            this.splitContainerControl1.TabIndex = 14;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(37, 208);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(225, 21);
            this.labelControl1.TabIndex = 22;
            this.labelControl1.Text = "Competition (.sta) Folder Path";
            // 
            // staBrowseButton
            // 
            this.staBrowseButton.Location = new System.Drawing.Point(289, 230);
            this.staBrowseButton.Name = "staBrowseButton";
            this.staBrowseButton.Size = new System.Drawing.Size(94, 22);
            this.staBrowseButton.TabIndex = 21;
            this.staBrowseButton.Text = "Browse";
            this.staBrowseButton.Click += new System.EventHandler(this.staBrowseButton_Click);
            // 
            // staFolderTextEdit
            // 
            this.staFolderTextEdit.Location = new System.Drawing.Point(37, 230);
            this.staFolderTextEdit.Name = "staFolderTextEdit";
            this.staFolderTextEdit.Size = new System.Drawing.Size(246, 22);
            this.staFolderTextEdit.TabIndex = 20;
            // 
            // okButton
            // 
            this.okButton.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.okButton.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.okButton.Appearance.ForeColor = System.Drawing.Color.Black;
            this.okButton.Appearance.Options.UseBackColor = true;
            this.okButton.Appearance.Options.UseFont = true;
            this.okButton.Appearance.Options.UseForeColor = true;
            this.okButton.Location = new System.Drawing.Point(460, 334);
            this.okButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(100, 27);
            this.okButton.TabIndex = 14;
            this.okButton.Text = "Ok";
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // tvFolderPathLabel
            // 
            this.tvFolderPathLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tvFolderPathLabel.Appearance.Options.UseFont = true;
            this.tvFolderPathLabel.Location = new System.Drawing.Point(37, 148);
            this.tvFolderPathLabel.Name = "tvFolderPathLabel";
            this.tvFolderPathLabel.Size = new System.Drawing.Size(109, 21);
            this.tvFolderPathLabel.TabIndex = 19;
            this.tvFolderPathLabel.Text = "TV Folder Path";
            // 
            // cancelButton
            // 
            this.cancelButton.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cancelButton.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelButton.Appearance.ForeColor = System.Drawing.Color.Black;
            this.cancelButton.Appearance.Options.UseBackColor = true;
            this.cancelButton.Appearance.Options.UseFont = true;
            this.cancelButton.Appearance.Options.UseForeColor = true;
            this.cancelButton.Location = new System.Drawing.Point(346, 334);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(100, 27);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // showFolderPathLabel
            // 
            this.showFolderPathLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.showFolderPathLabel.Appearance.Options.UseFont = true;
            this.showFolderPathLabel.Location = new System.Drawing.Point(37, 94);
            this.showFolderPathLabel.Name = "showFolderPathLabel";
            this.showFolderPathLabel.Size = new System.Drawing.Size(129, 21);
            this.showFolderPathLabel.TabIndex = 18;
            this.showFolderPathLabel.Text = "Show Folder Path";
            // 
            // tvFolderButton
            // 
            this.tvFolderButton.Location = new System.Drawing.Point(289, 170);
            this.tvFolderButton.Name = "tvFolderButton";
            this.tvFolderButton.Size = new System.Drawing.Size(94, 22);
            this.tvFolderButton.TabIndex = 17;
            this.tvFolderButton.Text = "Browse";
            this.tvFolderButton.Click += new System.EventHandler(this.tvFolderButton_Click);
            // 
            // showFolderButton
            // 
            this.showFolderButton.Location = new System.Drawing.Point(289, 114);
            this.showFolderButton.Name = "showFolderButton";
            this.showFolderButton.Size = new System.Drawing.Size(94, 25);
            this.showFolderButton.TabIndex = 16;
            this.showFolderButton.Text = "Browse";
            this.showFolderButton.Click += new System.EventHandler(this.showFolderButton_Click);
            // 
            // networkSetingLabel
            // 
            this.networkSetingLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.networkSetingLabel.Appearance.ForeColor = System.Drawing.Color.Black;
            this.networkSetingLabel.Appearance.Options.UseFont = true;
            this.networkSetingLabel.Appearance.Options.UseForeColor = true;
            this.networkSetingLabel.Location = new System.Drawing.Point(4, 12);
            this.networkSetingLabel.Name = "networkSetingLabel";
            this.networkSetingLabel.Size = new System.Drawing.Size(140, 21);
            this.networkSetingLabel.TabIndex = 14;
            this.networkSetingLabel.Text = "Network Setting";
            // 
            // TVFoldertextEdit
            // 
            this.TVFoldertextEdit.Location = new System.Drawing.Point(37, 170);
            this.TVFoldertextEdit.Name = "TVFoldertextEdit";
            this.TVFoldertextEdit.Size = new System.Drawing.Size(246, 22);
            this.TVFoldertextEdit.TabIndex = 14;
            // 
            // showFoldertextEdit
            // 
            this.showFoldertextEdit.Location = new System.Drawing.Point(37, 116);
            this.showFoldertextEdit.Name = "showFoldertextEdit";
            this.showFoldertextEdit.Size = new System.Drawing.Size(246, 22);
            this.showFoldertextEdit.TabIndex = 13;
            // 
            // udpPortText
            // 
            this.udpPortText.EditValue = "29001";
            this.udpPortText.Location = new System.Drawing.Point(150, 56);
            this.udpPortText.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.udpPortText.Name = "udpPortText";
            this.udpPortText.Properties.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.udpPortText.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.udpPortText.Properties.Appearance.Options.UseFont = true;
            this.udpPortText.Properties.Appearance.Options.UseForeColor = true;
            this.udpPortText.Size = new System.Drawing.Size(133, 26);
            this.udpPortText.TabIndex = 10;
            // 
            // udpPortLabel
            // 
            this.udpPortLabel.AutoSize = true;
            this.udpPortLabel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.udpPortLabel.ForeColor = System.Drawing.Color.Black;
            this.udpPortLabel.Location = new System.Drawing.Point(33, 59);
            this.udpPortLabel.Name = "udpPortLabel";
            this.udpPortLabel.Size = new System.Drawing.Size(77, 21);
            this.udpPortLabel.TabIndex = 9;
            this.udpPortLabel.Text = "UDP Port";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(37, 269);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(114, 21);
            this.labelControl2.TabIndex = 25;
            this.labelControl2.Text = "Htc Folder Path";
            // 
            // htcBrowseButton
            // 
            this.htcBrowseButton.Location = new System.Drawing.Point(289, 291);
            this.htcBrowseButton.Name = "htcBrowseButton";
            this.htcBrowseButton.Size = new System.Drawing.Size(94, 22);
            this.htcBrowseButton.TabIndex = 24;
            this.htcBrowseButton.Text = "Browse";
            this.htcBrowseButton.Click += new System.EventHandler(this.htcBrowseButton_Click);
            // 
            // htcFolderTextEdit
            // 
            this.htcFolderTextEdit.Location = new System.Drawing.Point(37, 291);
            this.htcFolderTextEdit.Name = "htcFolderTextEdit";
            this.htcFolderTextEdit.Size = new System.Drawing.Size(246, 22);
            this.htcFolderTextEdit.TabIndex = 23;
            // 
            // GeneralSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 398);
            this.Controls.Add(this.splitContainerControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "GeneralSettings";
            this.Text = "GeneralSettings";
            this.Load += new System.EventHandler(this.GeneralSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.staFolderTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TVFoldertextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.showFoldertextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udpPortText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.htcFolderTextEdit.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.LabelControl tvFolderPathLabel;
        private DevExpress.XtraEditors.LabelControl showFolderPathLabel;
        private DevExpress.XtraEditors.SimpleButton tvFolderButton;
        private DevExpress.XtraEditors.SimpleButton showFolderButton;
        private DevExpress.XtraEditors.LabelControl networkSetingLabel;
        private DevExpress.XtraEditors.TextEdit TVFoldertextEdit;
        private DevExpress.XtraEditors.TextEdit showFoldertextEdit;
        private DevExpress.XtraEditors.TextEdit udpPortText;
        private System.Windows.Forms.Label udpPortLabel;
        private DevExpress.XtraEditors.SimpleButton okButton;
        private DevExpress.XtraEditors.SimpleButton cancelButton;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton staBrowseButton;
        private DevExpress.XtraEditors.TextEdit staFolderTextEdit;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton htcBrowseButton;
        private DevExpress.XtraEditors.TextEdit htcFolderTextEdit;
    }
}