﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    public partial class GeneralSettings : Form
    {
        public GeneralSettings()
        {
            InitializeComponent();
        }

        UpdateJson updateJson = new UpdateJson();
    
        private void GeneralSettings_Load(object sender, EventArgs e)
        {
            try
            {
                if (NationCup.obj_internal != null)
                {
                    udpPortText.Text = NationCup.obj_internal.GeneralSetting.udpPort;
                    showFoldertextEdit.Text = NationCup.obj_internal.GeneralSetting.showFolderPath;
                    TVFoldertextEdit.Text = NationCup.obj_internal.GeneralSetting.tvFolderPath;
                    staFolderTextEdit.Text = NationCup.obj_internal.GeneralSetting.staFolderPath;
                    htcFolderTextEdit.Text = NationCup.obj_internal.GeneralSetting.htcFolderPath;
                }
                else
                {
                    udpPortText.Text = "29001";

                }
            }

            catch(Exception ex)
            {
                MessageBox.Show("Problem with loading Genaral settings, Please check your internal JSON data!\n" + ex.Message);
            }
           
        }

        private void showFolderButton_Click(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    //showFoldertextEdit.Text = "S:\\bazar\\show";
                    showFoldertextEdit.Text = folderBrowserDialog1.SelectedPath;
                    NationCup.obj_internal.GeneralSetting.showFolderPath = showFoldertextEdit.Text;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("SHOW folder is not selected properly.\n" + ex.Message);
            }
        }

        private void tvFolderButton_Click(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    //TVFoldertextEdit.Text = "S:\\bazar\\tv";
                    TVFoldertextEdit.Text = folderBrowserDialog1.SelectedPath;
                    NationCup.obj_internal.GeneralSetting.tvFolderPath = TVFoldertextEdit.Text;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("TV folder is not selected properly.\n" + ex.Message);
            }
        }

        private void staBrowseButton_Click(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    //showFoldertextEdit.Text = "S:\\bazar\\show";
                    staFolderTextEdit.Text = folderBrowserDialog1.SelectedPath;
                    NationCup.obj_internal.GeneralSetting.staFolderPath = staFolderTextEdit.Text;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("STA folder is not selected properly.\n" + ex.Message);
            }
        }

        private void htcBrowseButton_Click(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    //showFoldertextEdit.Text = "S:\\bazar\\show";
                    htcFolderTextEdit.Text = folderBrowserDialog1.SelectedPath;
                    NationCup.obj_internal.GeneralSetting.htcFolderPath = htcFolderTextEdit.Text;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("HTC folder is not selected properly.\n" + ex.Message);
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            NationCup.obj_internal.GeneralSetting.udpPort = udpPortText.Text;

            try
            {
                int port = Convert.ToInt32(udpPortText.Text);
                NationCup.receivingUDPclient = new UdpClient(port);
            }
            catch
            {
                MessageBox.Show("The port is already connected!\n");
            }

            try
            {
                //if (showFoldertextEdit.Text != "")
                //    NationCup.obj_internal.GeneralSetting.showFolderPath = showFoldertextEdit.Text;
                //else
                //    MessageBox.Show("Please select the show folder!!");

                //if (TVFoldertextEdit.Text != "")
                //    NationCup.obj_internal.GeneralSetting.tvFolderPath = TVFoldertextEdit.Text;
                //else
                //    MessageBox.Show("Please select the tv folder!!");

                //if (staFolderTextEdit.Text != "")
                //    NationCup.obj_internal.GeneralSetting.staFolderPath = staFolderTextEdit.Text;
                //else
                //    MessageBox.Show("Please select the competition folder!!");

                //if (htcFolderTextEdit.Text != "")
                //    NationCup.obj_internal.GeneralSetting.htcFolderPath = htcFolderTextEdit.Text;
                //else
                //    MessageBox.Show("Please select the htc folder!!");

                //Json updates
                if (showFoldertextEdit.Text != "" && TVFoldertextEdit.Text != "" && staFolderTextEdit.Text != "" && htcFolderTextEdit.Text != "")
                {
                    NationCup.obj_internal.GeneralSetting.showFolderPath = showFoldertextEdit.Text;
                    NationCup.obj_internal.GeneralSetting.tvFolderPath = TVFoldertextEdit.Text;
                    NationCup.obj_internal.GeneralSetting.staFolderPath = staFolderTextEdit.Text;
                    NationCup.obj_internal.GeneralSetting.htcFolderPath = htcFolderTextEdit.Text;
                    updateJson.modifyJsonForGeneralSettings(NationCup.obj_internal);
                    Form.ActiveForm.Close();
                }
                else
                {
                    MessageBox.Show("Please provide the Show, TV, STA, and HTC folder path correctly!");
                }
                //updateJson.modifyJsonForGeneralSettings(NationCup.obj_internal);

                //Form.ActiveForm.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show("Problem with saving general settings data, Please provide path correctly!\n" + ex.Message);
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Form.ActiveForm.Close();
        }
    }
}
