﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    [Serializable]
    public class Horse
    {
        public int cNo { get; set; } // Combination number
        public string name { get; set; }
        public string breed { get; set; }
        public string breedingArea { get; set; }
        public string color { get; set; }
        public string sex { get; set; }
        public string fatherName { get; set; }
        public string onwer { get; set; }
        public string motherFatherName { get; set; }
        public string nation { get; set; }
        public string feiId { get; set; }
        public string nationalId { get; set; }
        public string yearOfBirth { get; set; }

        public void SetHorseData(int x)
        {
            cNo = Convert.ToInt16(Constants.reportInfo[x + 1][0]);
            name = Constants.reportInfo[x + 6][1];
            breed = Constants.reportInfo[x + 15][1];
            breedingArea = Constants.reportInfo[x + 7][1];
            color = Constants.reportInfo[x + 8][1];
            sex = Constants.reportInfo[x + 9][1];
            fatherName = Constants.reportInfo[x + 10][1];
            onwer = Constants.reportInfo[x + 12][1];
            motherFatherName = Constants.reportInfo[x + 20][1];
            nation = Constants.reportInfo[x + 26][1];
            feiId = Constants.reportInfo[x + 27][1];
            nationalId = Constants.reportInfo[x + 5][2];
            yearOfBirth = Constants.reportInfo[x + 29][1];

        }
    }
}
