﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    public class InternalJson
    {
        public string udpPort { get; set; }
        public string showFolderPath { get; set; }
        public string tvFolderPath { get; set; }
        public string staFolderPath { get; set; }
        public string htcFolderPath { get; set; }
        public string htcFilePath { get; set; }
        public string cBox { get; set; }

        public void CreateJsonForGeneralSettings()
        {
            udpPort = "29001";
            showFolderPath = null;
            tvFolderPath = null;
            staFolderPath = null;
            htcFolderPath = null;
            htcFilePath = null;
            cBox = null;
        }
    }

    public class SerializationForInternal
    {
        public InternalJson GeneralSetting = new InternalJson();

        public void CreateJson()
        {
            GeneralSetting.CreateJsonForGeneralSettings();
        }
    }
}
