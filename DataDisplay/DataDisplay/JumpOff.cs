﻿using DevExpress.XtraGrid.Views.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDisplay
{
    public class JumpOff
    {
        public List<string[]> g1;
        public List<string[]> g2;
        public List<string[]> g3;
        public List<string[]> g4;
        public DataTable dataTable;
        Rank_Calculate rank_Calculate = new Rank_Calculate();
        ChangeObjForJumpOff changeObjForJumpOff = new ChangeObjForJumpOff();

        public void change_obj(Serialization obj)
        {
            //rank_Calculate.Rank_for_JumpOff(obj);
            changeObjForJumpOff.jumpoff_Teams(obj);
        }

        public void create_datatable()
        {
            dataTable = new DataTable();
            g1 = new List<string[]>();
            g2 = new List<string[]>();
            g3 = new List<string[]>();
            g4 = new List<string[]>();

            for (int i = 0; i < changeObjForJumpOff.tmp_result.Count(); i++)
            {
                g1.Add(new string[] { Convert.ToString(changeObjForJumpOff.tmp_result[i].No), changeObjForJumpOff.tmp_result[i].IocCode, changeObjForJumpOff.tmp_result[i].MemberDetails[0].CNR,
                    changeObjForJumpOff.tmp_result[i].MemberDetails[0].HorseName, changeObjForJumpOff.tmp_result[i].MemberDetails[0].RiderName, changeObjForJumpOff.tmp_result[i].MemberDetails[0].JumpOff_Penalty,
                    changeObjForJumpOff.tmp_result[i].MemberDetails[0].JumpOff_Time});

                g2.Add(new string[] { Convert.ToString(changeObjForJumpOff.tmp_result[i].No), changeObjForJumpOff.tmp_result[i].IocCode, changeObjForJumpOff.tmp_result[i].MemberDetails[1].CNR,
                    changeObjForJumpOff.tmp_result[i].MemberDetails[1].HorseName, changeObjForJumpOff.tmp_result[i].MemberDetails[1].RiderName, changeObjForJumpOff.tmp_result[i].MemberDetails[1].JumpOff_Penalty,
                    changeObjForJumpOff.tmp_result[i].MemberDetails[1].JumpOff_Time });

                g3.Add(new string[] { Convert.ToString(changeObjForJumpOff.tmp_result[i].No), changeObjForJumpOff.tmp_result[i].IocCode, changeObjForJumpOff.tmp_result[i].MemberDetails[2].CNR,
                    changeObjForJumpOff.tmp_result[i].MemberDetails[2].HorseName, changeObjForJumpOff.tmp_result[i].MemberDetails[2].RiderName, changeObjForJumpOff.tmp_result[i].MemberDetails[2].JumpOff_Penalty,
                    changeObjForJumpOff.tmp_result[i].MemberDetails[2].JumpOff_Time });

                g4.Add(new string[] { Convert.ToString(changeObjForJumpOff.tmp_result[i].No), changeObjForJumpOff.tmp_result[i].IocCode, changeObjForJumpOff.tmp_result[i].MemberDetails[3].CNR,
                    changeObjForJumpOff.tmp_result[i].MemberDetails[3].HorseName, changeObjForJumpOff.tmp_result[i].MemberDetails[3].RiderName, changeObjForJumpOff.tmp_result[i].MemberDetails[3].JumpOff_Penalty,
                    changeObjForJumpOff.tmp_result[i].MemberDetails[3].JumpOff_Time });
            }

            int it = 0;
            dataTable.Columns.Add("Position");
            dataTable.Columns.Add("IOC");
            dataTable.Columns.Add("CNR");
            dataTable.Columns.Add("Horse Name");
            dataTable.Columns.Add("Rider Name");
            dataTable.Columns.Add("JumpOff_Penalty");
            dataTable.Columns.Add("JumpOff_Time");

            for (int i = 0; i < g1.Count;)
            {
                it = it + 1;
                dataTable.Rows.Add(it, g1[i][1], g1[i][2], g1[i][3], g1[i][4], g1[i][5], g1[i][6]);
                i++;
            }

            for (int i = 0; i < g2.Count;)
            {
                it = it + 1;
                dataTable.Rows.Add(it, g2[i][1], g2[i][2], g2[i][3], g2[i][4], g2[i][5], g2[i][6]);
                i++;
            }

            for (int i = 0; i < g3.Count;)
            {
                it = it + 1;
                dataTable.Rows.Add(it, g3[i][1], g3[i][2], g3[i][3], g3[i][4], g3[i][5], g3[i][6]);
                i++;
            }

            for (int i = 0; i < g4.Count;)
            {
                it = it + 1;
                dataTable.Rows.Add(it, g4[i][1], g4[i][2], g4[i][3], g4[i][4], g4[i][5], g4[i][6]);
                i++;
            }
        }

        public void create_storder(Serialization obj)
        {
            NpwStorderRankingJO npw_storder = new NpwStorderRankingJO();

            StringBuilder csvcontent = new StringBuilder();

            for (int i = 0; i < changeObjForJumpOff.tmp_result.Count(); i++)
            {
                for (int j = 0; j < obj.Teams.Count; j++)
                {
                    if (changeObjForJumpOff.tmp_result[i].No == obj.Teams[j].No)
                    {
                        string csv = "";
                        int order = 0;
                        csv = npw_storder.team_info(csv, obj, j, order);
                        csvcontent.AppendLine(csv);

                        for (int k = 0; k < changeObjForJumpOff.tmp_result[i].MemberDetails.Count(); k++)
                        {
                            csv = null;
                            order += 1;
                            csv = npw_storder.member_info(csv, obj, j, k, order);
                            csvcontent.AppendLine(csv);
                        }
                    }
                }

            }

            string csvpath = Path.Combine(Constants.Event_Json, "storder.npw");
            File.Create(csvpath).Dispose();
            File.AppendAllText(csvpath, csvcontent.ToString());
        }

        public void create_starter(Serialization obj, int no, string cnr)
        {
            NpwStarterErgebnisJO npw_starter = new NpwStarterErgebnisJO();
            StringBuilder csvcontent = new StringBuilder();

            string csv = "";
            int order = 0;

            for (int j = 0; j < obj.Teams.Count; j++)
            {
                if (no == obj.Teams[j].No)
                {
                    csv = npw_starter.team_info(csv, obj, j, order);
                    csvcontent.AppendLine(csv);

                    for (int k = 0; k < obj.Teams[j].MemberDetails.Count(); k++)
                    {
                        csv = null;
                        order += 1;

                        if (cnr == obj.Teams[j].MemberDetails[k].CNR)
                            csv = npw_starter.current_member_info(csv, obj, j, k, order);

                        else
                            csv = npw_starter.member_info(csv, obj, j, k, order);

                        csvcontent.AppendLine(csv);
                    }
                }
            }

            string csvpath = Path.Combine(Constants.Event_Json, "starter.npw");
            File.Create(csvpath).Dispose();
            File.AppendAllText(csvpath, csvcontent.ToString());
            Console.WriteLine(csvcontent.ToString());
        }

        public void create_ergebnis(Serialization obj, int no, string cnr)
        {
            NpwStarterErgebnisJO npw_ergebnis = new NpwStarterErgebnisJO();
            StringBuilder csvcontent = new StringBuilder();

            string csv = "";
            int order = 0;

            for (int i = 0; i < obj.Teams.Count(); i++)
            {
                if (no == obj.Teams[i].No)
                {
                    csv = npw_ergebnis.team_info(csv, obj, i, order);
                    csvcontent.AppendLine(csv);

                    for (int k = 0; k < obj.Teams[i].MemberDetails.Count(); k++)
                    {
                        csv = null;
                        order += 1;

                        if (cnr == obj.Teams[i].MemberDetails[k].CNR)
                        {
                            csv = npw_ergebnis.current_member_info(csv, obj, i, k, order);
                        }

                        else
                            csv = npw_ergebnis.member_info(csv, obj, i, k, order);

                        csvcontent.AppendLine(csv);
                    }

                    string csvpath = Path.Combine(Constants.Event_Json, "ergebnis.npw");
                    File.Create(csvpath).Dispose();
                    File.AppendAllText(csvpath, csvcontent.ToString());
                    Console.WriteLine(csvcontent.ToString());
                }
            }
        }

        public void create_ranking(Serialization obj)
        {
            changeObjForJumpOff.JumpOff_ranking(obj);

            NpwStorderRankingJO npw_ranking = new NpwStorderRankingJO();
            StringBuilder csvcontent = new StringBuilder();

            for (int i = 0; i < obj.Rank.Count(); i++)
            {
                string csv = "";
                int order = 0;
                for (int j = 0; j < obj.Teams.Count; j++)
                {
                    if (obj.Rank[i].teamName == obj.Teams[j].Name)
                    {
                        csv = npw_ranking.team_info(csv, obj, j, order);
                        csvcontent.AppendLine(csv);

                        for (int k = 0; k < obj.Teams[j].MemberDetails.Count(); k++)
                        {
                            csv = null;
                            order += 1;
                            csv = npw_ranking.member_info(csv, obj, j, k, order);
                            csvcontent.AppendLine(csv);
                        }
                    }
                }
            }

            string csvpath = Path.Combine(Constants.Event_Json, "ranking.npw");
            File.Create(csvpath).Dispose();
            File.AppendAllText(csvpath, csvcontent.ToString());
        }
    }
}
