﻿namespace DataDisplay
{
    partial class NationCup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }


        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NationCup));
            DevExpress.Utils.Animation.PushTransition pushTransition1 = new DevExpress.Utils.Animation.PushTransition();
            this.member_bandedGridView = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.Members = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colCNR = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colHorseName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colRiderName = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colRound1_Penalty = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colRound1_Time = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colRound2_Total = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colRound2_Time = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.colJumpOff = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.colJumpOff_Time = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.teamCompetitionBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.team_gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIocCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTrainer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.display_button = new DevExpress.XtraBars.BarButtonItem();
            this.import_sta_button = new DevExpress.XtraBars.BarButtonItem();
            this.load_json_button = new DevExpress.XtraBars.BarButtonItem();
            this.convert2json_button = new DevExpress.XtraBars.BarButtonItem();
            this.export_json_button = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.round1Button = new DevExpress.XtraBars.BarButtonItem();
            this.round2Button = new DevExpress.XtraBars.BarButtonItem();
            this.round3Button = new DevExpress.XtraBars.BarButtonItem();
            this.jumpoffButton = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barCheckItem1 = new DevExpress.XtraBars.BarCheckItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.configrationbarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockingMenuItem1 = new DevExpress.XtraBars.BarDockingMenuItem();
            this.barButtonGroup1 = new DevExpress.XtraBars.BarButtonGroup();
            this.barWorkspaceMenuItem1 = new DevExpress.XtraBars.BarWorkspaceMenuItem();
            this.workspaceManager1 = new DevExpress.Utils.WorkspaceManager(this.components);
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockingMenuItem2 = new DevExpress.XtraBars.BarDockingMenuItem();
            this.startlistForShowbarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.round1barButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.round2barButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.jumpOffbarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.barSubItem4 = new DevExpress.XtraBars.BarSubItem();
            this.barHeaderItem1 = new DevExpress.XtraBars.BarHeaderItem();
            this.barButtonItem12 = new DevExpress.XtraBars.BarButtonItem();
            this.startlistForTVbarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.startListR1barButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.startListR2barButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.resultOfR1barButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.resultOfR2barButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.startListOfCurrentRound = new DevExpress.XtraBars.BarButtonItem();
            this.resultOfCurrentFinishedRound = new DevExpress.XtraBars.BarButtonItem();
            this.finalReportBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.generalSettingbarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.competitionSettingbarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.startOrderForRound1barButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.startOrderForR2barButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPage3 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.BtnLock = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.Lock = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.Unlock = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.Save = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.Cancel = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.round2_gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.round1_gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.round3_gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.jumpOff_gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.teamCompetitionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.navBarControl2 = new DevExpress.XtraNavBar.NavBarControl();
            this.eventNumLabel = new DevExpress.XtraNavBar.NavBarGroup();
            this.eventNameLabel = new DevExpress.XtraNavBar.NavBarItem();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.elButton = new DevExpress.XtraEditors.SimpleButton();
            this.rtButton = new DevExpress.XtraEditors.SimpleButton();
            this.dqButton = new DevExpress.XtraEditors.SimpleButton();
            this.timeLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.dnsButton = new DevExpress.XtraEditors.SimpleButton();
            this.PenaltyLabelControl = new DevExpress.XtraEditors.LabelControl();
            this.timekeepingTimeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.timekeepingPenaltyTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.timeAllowedLabel = new DevExpress.XtraEditors.LabelControl();
            this.Label_FZ = new System.Windows.Forms.Label();
            this.ProgressBar = new System.Windows.Forms.ProgressBar();
            this.confirmButton = new DevExpress.XtraEditors.SimpleButton();
            this.chooseRound_comboBox = new DevExpress.XtraEditors.ComboBoxEdit();
            this.roundFinishButton = new DevExpress.XtraEditors.SimpleButton();
            this.takeOverButton = new DevExpress.XtraEditors.SimpleButton();
            this.okButton = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.ioctextEdit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.totaltextEdit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.timePenstextEdit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.penaltiestextEdit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.timetextEdit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.teamtextEdit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.riderNametextEdit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.horseNametextEdit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.cnrtextEdit = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.ranking_gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colteamName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colrank = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltotalPenalties = new DevExpress.XtraGrid.Columns.GridColumn();
            this.printButton = new DevExpress.XtraEditors.SimpleButton();
            this.dataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.TeamName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UDPTimer = new System.Windows.Forms.Timer(this.components);
            this.Timer_ProgressBar = new System.Windows.Forms.Timer(this.components);
            this.report1 = new FastReport.Report();
            this.teamCompetitionBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.round1databaseBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.storderReport = new FastReport.Report();
            this.startListR1Report = new FastReport.Report();
            this.startListR2Report = new FastReport.Report();
            this.resultForR1Report = new FastReport.Report();
            this.resultForR2Report = new FastReport.Report();
            this.finalResultReport = new FastReport.Report();
            ((System.ComponentModel.ISupportInitialize)(this.member_bandedGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamCompetitionBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.team_gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnLock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Unlock)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.round2_gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.round1_gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.round3_gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jumpOff_gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamCompetitionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timekeepingTimeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timekeepingPenaltyTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chooseRound_comboBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ioctextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.totaltextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timePenstextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.penaltiestextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timetextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamtextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riderNametextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.horseNametextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cnrtextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ranking_gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamCompetitionBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.round1databaseBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.storderReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startListR1Report)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startListR2Report)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultForR1Report)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultForR2Report)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.finalResultReport)).BeginInit();
            this.SuspendLayout();
            // 
            // member_bandedGridView
            // 
            this.member_bandedGridView.Appearance.DetailTip.Font = new System.Drawing.Font("Tahoma", 12F);
            this.member_bandedGridView.Appearance.DetailTip.Options.UseFont = true;
            this.member_bandedGridView.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.member_bandedGridView.Appearance.EvenRow.Options.UseBackColor = true;
            this.member_bandedGridView.Appearance.FocusedCell.BackColor = System.Drawing.Color.LavenderBlush;
            this.member_bandedGridView.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.member_bandedGridView.Appearance.FocusedCell.Options.UseBackColor = true;
            this.member_bandedGridView.Appearance.FocusedCell.Options.UseForeColor = true;
            this.member_bandedGridView.Appearance.FocusedRow.BackColor = System.Drawing.Color.PaleVioletRed;
            this.member_bandedGridView.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.member_bandedGridView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.member_bandedGridView.Appearance.FocusedRow.Options.UseForeColor = true;
            this.member_bandedGridView.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.member_bandedGridView.Appearance.FooterPanel.Font = new System.Drawing.Font("Tahoma", 9F);
            this.member_bandedGridView.Appearance.FooterPanel.Options.UseBackColor = true;
            this.member_bandedGridView.Appearance.FooterPanel.Options.UseFont = true;
            this.member_bandedGridView.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.member_bandedGridView.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.member_bandedGridView.Appearance.GroupPanel.BackColor = System.Drawing.Color.ForestGreen;
            this.member_bandedGridView.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.LightGreen;
            this.member_bandedGridView.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.member_bandedGridView.Appearance.GroupPanel.Options.UseBackColor = true;
            this.member_bandedGridView.Appearance.GroupPanel.Options.UseForeColor = true;
            this.member_bandedGridView.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.member_bandedGridView.Appearance.HorzLine.Options.UseBackColor = true;
            this.member_bandedGridView.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.member_bandedGridView.Appearance.OddRow.Options.UseBackColor = true;
            this.member_bandedGridView.Appearance.Row.BackColor = System.Drawing.Color.LightPink;
            this.member_bandedGridView.Appearance.Row.Options.UseBackColor = true;
            this.member_bandedGridView.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.member_bandedGridView.Appearance.VertLine.Options.UseBackColor = true;
            this.member_bandedGridView.AppearancePrint.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.member_bandedGridView.AppearancePrint.FooterPanel.Options.UseForeColor = true;
            this.member_bandedGridView.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.Members,
            this.gridBand1,
            this.gridBand2,
            this.gridBand3});
            this.member_bandedGridView.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.colCNR,
            this.colHorseName,
            this.colRiderName,
            this.colRound1_Penalty,
            this.colRound1_Time,
            this.colRound2_Total,
            this.colRound2_Time,
            this.colJumpOff,
            this.colJumpOff_Time});
            this.member_bandedGridView.DetailHeight = 294;
            this.member_bandedGridView.FixedLineWidth = 1;
            this.member_bandedGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.member_bandedGridView.GridControl = this.gridControl1;
            this.member_bandedGridView.Name = "member_bandedGridView";
            this.member_bandedGridView.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.Inplace;
            this.member_bandedGridView.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseUp;
            this.member_bandedGridView.OptionsDetail.ShowDetailTabs = false;
            this.member_bandedGridView.OptionsDetail.ShowEmbeddedDetailIndent = DevExpress.Utils.DefaultBoolean.True;
            this.member_bandedGridView.OptionsEditForm.ShowUpdateCancelPanel = DevExpress.Utils.DefaultBoolean.True;
            this.member_bandedGridView.OptionsMenu.ShowGroupSortSummaryItems = false;
            this.member_bandedGridView.OptionsPrint.ExpandAllDetails = true;
            this.member_bandedGridView.OptionsPrint.PrintDetails = true;
            this.member_bandedGridView.OptionsPrint.PrintFilterInfo = true;
            this.member_bandedGridView.OptionsPrint.PrintPreview = true;
            this.member_bandedGridView.OptionsSelection.MultiSelect = true;
            this.member_bandedGridView.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.member_bandedGridView.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.member_bandedGridView.OptionsView.ShowFooter = true;
            this.member_bandedGridView.OptionsView.ShowGroupPanel = false;
            this.member_bandedGridView.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.False;
            this.member_bandedGridView.PaintStyleName = "MixedXP";
            this.member_bandedGridView.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.member_bandedGridView_ShowingEditor);
            this.member_bandedGridView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.member_bandedGridView_MouseDown);
            // 
            // Members
            // 
            this.Members.AppearanceHeader.BackColor = System.Drawing.Color.RoyalBlue;
            this.Members.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.Members.AppearanceHeader.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.Members.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.Members.AppearanceHeader.Options.UseBackColor = true;
            this.Members.AppearanceHeader.Options.UseFont = true;
            this.Members.AppearanceHeader.Options.UseForeColor = true;
            this.Members.AppearanceHeader.Options.UseTextOptions = true;
            this.Members.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Members.Caption = "Members";
            this.Members.Columns.Add(this.colCNR);
            this.Members.Columns.Add(this.colHorseName);
            this.Members.Columns.Add(this.colRiderName);
            this.Members.MinWidth = 146;
            this.Members.Name = "Members";
            this.Members.VisibleIndex = 0;
            this.Members.Width = 340;
            // 
            // colCNR
            // 
            this.colCNR.AppearanceCell.Options.UseTextOptions = true;
            this.colCNR.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCNR.AppearanceHeader.BackColor = System.Drawing.Color.RoyalBlue;
            this.colCNR.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colCNR.AppearanceHeader.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.colCNR.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.colCNR.AppearanceHeader.Options.UseBackColor = true;
            this.colCNR.AppearanceHeader.Options.UseFont = true;
            this.colCNR.AppearanceHeader.Options.UseForeColor = true;
            this.colCNR.AppearanceHeader.Options.UseTextOptions = true;
            this.colCNR.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCNR.Caption = "CNR";
            this.colCNR.FieldName = "CNR";
            this.colCNR.MinWidth = 68;
            this.colCNR.Name = "colCNR";
            this.colCNR.OptionsColumn.ReadOnly = true;
            this.colCNR.Visible = true;
            this.colCNR.Width = 68;
            // 
            // colHorseName
            // 
            this.colHorseName.AppearanceCell.Options.UseTextOptions = true;
            this.colHorseName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colHorseName.AppearanceHeader.BackColor = System.Drawing.Color.RoyalBlue;
            this.colHorseName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colHorseName.AppearanceHeader.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.colHorseName.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.colHorseName.AppearanceHeader.Options.UseBackColor = true;
            this.colHorseName.AppearanceHeader.Options.UseFont = true;
            this.colHorseName.AppearanceHeader.Options.UseForeColor = true;
            this.colHorseName.AppearanceHeader.Options.UseTextOptions = true;
            this.colHorseName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colHorseName.Caption = "Horse Name";
            this.colHorseName.FieldName = "HorseName";
            this.colHorseName.MinWidth = 136;
            this.colHorseName.Name = "colHorseName";
            this.colHorseName.OptionsColumn.ReadOnly = true;
            this.colHorseName.Visible = true;
            this.colHorseName.Width = 136;
            // 
            // colRiderName
            // 
            this.colRiderName.AppearanceCell.Options.UseTextOptions = true;
            this.colRiderName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRiderName.AppearanceHeader.BackColor = System.Drawing.Color.RoyalBlue;
            this.colRiderName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colRiderName.AppearanceHeader.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.colRiderName.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.colRiderName.AppearanceHeader.Options.UseBackColor = true;
            this.colRiderName.AppearanceHeader.Options.UseFont = true;
            this.colRiderName.AppearanceHeader.Options.UseForeColor = true;
            this.colRiderName.AppearanceHeader.Options.UseTextOptions = true;
            this.colRiderName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRiderName.Caption = "Rider Name";
            this.colRiderName.FieldName = "RiderName";
            this.colRiderName.MinWidth = 136;
            this.colRiderName.Name = "colRiderName";
            this.colRiderName.OptionsColumn.ReadOnly = true;
            this.colRiderName.Visible = true;
            this.colRiderName.Width = 136;
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridBand1.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand1.AppearanceHeader.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.gridBand1.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.gridBand1.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand1.AppearanceHeader.Options.UseFont = true;
            this.gridBand1.AppearanceHeader.Options.UseForeColor = true;
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "Round 1";
            this.gridBand1.Columns.Add(this.colRound1_Penalty);
            this.gridBand1.Columns.Add(this.colRound1_Time);
            this.gridBand1.MinWidth = 175;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 1;
            this.gridBand1.Width = 175;
            // 
            // colRound1_Penalty
            // 
            this.colRound1_Penalty.AppearanceCell.Options.UseTextOptions = true;
            this.colRound1_Penalty.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRound1_Penalty.AppearanceHeader.BackColor = System.Drawing.Color.RoyalBlue;
            this.colRound1_Penalty.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colRound1_Penalty.AppearanceHeader.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.colRound1_Penalty.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.colRound1_Penalty.AppearanceHeader.Options.UseBackColor = true;
            this.colRound1_Penalty.AppearanceHeader.Options.UseFont = true;
            this.colRound1_Penalty.AppearanceHeader.Options.UseForeColor = true;
            this.colRound1_Penalty.AppearanceHeader.Options.UseTextOptions = true;
            this.colRound1_Penalty.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRound1_Penalty.Caption = "Penalty";
            this.colRound1_Penalty.FieldName = "Round1_Total";
            this.colRound1_Penalty.MinWidth = 58;
            this.colRound1_Penalty.Name = "colRound1_Penalty";
            this.colRound1_Penalty.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Round1_Total", "Total = {0:0.##}")});
            this.colRound1_Penalty.Visible = true;
            this.colRound1_Penalty.Width = 68;
            // 
            // colRound1_Time
            // 
            this.colRound1_Time.AppearanceCell.Options.UseTextOptions = true;
            this.colRound1_Time.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRound1_Time.AppearanceHeader.BackColor = System.Drawing.Color.RoyalBlue;
            this.colRound1_Time.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colRound1_Time.AppearanceHeader.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.colRound1_Time.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.colRound1_Time.AppearanceHeader.Options.UseBackColor = true;
            this.colRound1_Time.AppearanceHeader.Options.UseFont = true;
            this.colRound1_Time.AppearanceHeader.Options.UseForeColor = true;
            this.colRound1_Time.AppearanceHeader.Options.UseTextOptions = true;
            this.colRound1_Time.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRound1_Time.Caption = "Time";
            this.colRound1_Time.FieldName = "Round1_Time";
            this.colRound1_Time.MinWidth = 58;
            this.colRound1_Time.Name = "colRound1_Time";
            this.colRound1_Time.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Round1_Time", "SUM={0:0.##}")});
            this.colRound1_Time.Visible = true;
            this.colRound1_Time.Width = 107;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridBand2.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand2.AppearanceHeader.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.gridBand2.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.gridBand2.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand2.AppearanceHeader.Options.UseFont = true;
            this.gridBand2.AppearanceHeader.Options.UseForeColor = true;
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "Round 2";
            this.gridBand2.Columns.Add(this.colRound2_Total);
            this.gridBand2.Columns.Add(this.colRound2_Time);
            this.gridBand2.MinWidth = 185;
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 2;
            this.gridBand2.Width = 185;
            // 
            // colRound2_Total
            // 
            this.colRound2_Total.AppearanceCell.Options.UseTextOptions = true;
            this.colRound2_Total.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRound2_Total.AppearanceHeader.BackColor = System.Drawing.Color.RoyalBlue;
            this.colRound2_Total.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colRound2_Total.AppearanceHeader.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.colRound2_Total.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.colRound2_Total.AppearanceHeader.Options.UseBackColor = true;
            this.colRound2_Total.AppearanceHeader.Options.UseFont = true;
            this.colRound2_Total.AppearanceHeader.Options.UseForeColor = true;
            this.colRound2_Total.AppearanceHeader.Options.UseTextOptions = true;
            this.colRound2_Total.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRound2_Total.Caption = "Penalty";
            this.colRound2_Total.FieldName = "Round2_Total";
            this.colRound2_Total.MinWidth = 58;
            this.colRound2_Total.Name = "colRound2_Total";
            this.colRound2_Total.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Round2_Total", "Total = {0:0.##}")});
            this.colRound2_Total.Visible = true;
            this.colRound2_Total.Width = 60;
            // 
            // colRound2_Time
            // 
            this.colRound2_Time.AppearanceCell.Options.UseTextOptions = true;
            this.colRound2_Time.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRound2_Time.AppearanceHeader.BackColor = System.Drawing.Color.RoyalBlue;
            this.colRound2_Time.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colRound2_Time.AppearanceHeader.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.colRound2_Time.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.colRound2_Time.AppearanceHeader.Options.UseBackColor = true;
            this.colRound2_Time.AppearanceHeader.Options.UseFont = true;
            this.colRound2_Time.AppearanceHeader.Options.UseForeColor = true;
            this.colRound2_Time.AppearanceHeader.Options.UseTextOptions = true;
            this.colRound2_Time.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRound2_Time.Caption = "Time";
            this.colRound2_Time.FieldName = "Round2_Time";
            this.colRound2_Time.MinWidth = 125;
            this.colRound2_Time.Name = "colRound2_Time";
            this.colRound2_Time.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Round2_Time", "Total = {0:0.##}")});
            this.colRound2_Time.Visible = true;
            this.colRound2_Time.Width = 125;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.BackColor = System.Drawing.Color.RoyalBlue;
            this.gridBand3.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.gridBand3.AppearanceHeader.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.gridBand3.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.gridBand3.AppearanceHeader.Options.UseBackColor = true;
            this.gridBand3.AppearanceHeader.Options.UseFont = true;
            this.gridBand3.AppearanceHeader.Options.UseForeColor = true;
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "Jump Off";
            this.gridBand3.Columns.Add(this.colJumpOff);
            this.gridBand3.Columns.Add(this.colJumpOff_Time);
            this.gridBand3.MinWidth = 185;
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 3;
            this.gridBand3.Width = 185;
            // 
            // colJumpOff
            // 
            this.colJumpOff.AppearanceCell.Options.UseTextOptions = true;
            this.colJumpOff.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colJumpOff.AppearanceHeader.BackColor = System.Drawing.Color.RoyalBlue;
            this.colJumpOff.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colJumpOff.AppearanceHeader.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.colJumpOff.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.colJumpOff.AppearanceHeader.Options.UseBackColor = true;
            this.colJumpOff.AppearanceHeader.Options.UseFont = true;
            this.colJumpOff.AppearanceHeader.Options.UseForeColor = true;
            this.colJumpOff.AppearanceHeader.Options.UseTextOptions = true;
            this.colJumpOff.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colJumpOff.Caption = "Penalty";
            this.colJumpOff.FieldName = "JumpOff_Penalty";
            this.colJumpOff.MinWidth = 58;
            this.colJumpOff.Name = "colJumpOff";
            this.colJumpOff.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "JumpOff_Penalty", "Total = {0:0.##}")});
            this.colJumpOff.Visible = true;
            this.colJumpOff.Width = 60;
            // 
            // colJumpOff_Time
            // 
            this.colJumpOff_Time.AppearanceCell.Options.UseTextOptions = true;
            this.colJumpOff_Time.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colJumpOff_Time.AppearanceHeader.BackColor = System.Drawing.Color.RoyalBlue;
            this.colJumpOff_Time.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colJumpOff_Time.AppearanceHeader.FontStyleDelta = System.Drawing.FontStyle.Bold;
            this.colJumpOff_Time.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.colJumpOff_Time.AppearanceHeader.Options.UseBackColor = true;
            this.colJumpOff_Time.AppearanceHeader.Options.UseFont = true;
            this.colJumpOff_Time.AppearanceHeader.Options.UseForeColor = true;
            this.colJumpOff_Time.AppearanceHeader.Options.UseTextOptions = true;
            this.colJumpOff_Time.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colJumpOff_Time.Caption = "Time";
            this.colJumpOff_Time.FieldName = "JumpOff_Time";
            this.colJumpOff_Time.MinWidth = 125;
            this.colJumpOff_Time.Name = "colJumpOff_Time";
            this.colJumpOff_Time.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "JumpOff_Time", "Total = {0:0.##}")});
            this.colJumpOff_Time.Visible = true;
            this.colJumpOff_Time.Width = 125;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.teamCompetitionBindingSource2;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.gridControl1.EmbeddedNavigator.CustomButtons.AddRange(new DevExpress.XtraEditors.NavigatorCustomButton[] {
            new DevExpress.XtraEditors.NavigatorCustomButton()});
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            gridLevelNode1.LevelTemplate = this.member_bandedGridView;
            gridLevelNode1.RelationName = "MemberDetails";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.LookAndFeel.SkinName = "DevExpress Dark Style";
            this.gridControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.gridControl1.MainView = this.team_gridView;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gridControl1.MenuManager = this.ribbon;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.BtnLock,
            this.Lock,
            this.Unlock,
            this.Save,
            this.Cancel,
            this.repositoryItemButtonEdit1});
            this.gridControl1.ShowOnlyPredefinedDetails = true;
            this.gridControl1.Size = new System.Drawing.Size(730, 549);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.TabStop = false;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.team_gridView,
            this.member_bandedGridView});
            // 
            // teamCompetitionBindingSource2
            // 
            this.teamCompetitionBindingSource2.DataSource = typeof(DataDisplay.TeamCompetition);
            // 
            // team_gridView
            // 
            this.team_gridView.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.team_gridView.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.team_gridView.Appearance.DetailTip.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.team_gridView.Appearance.DetailTip.BorderColor = System.Drawing.Color.Blue;
            this.team_gridView.Appearance.DetailTip.Font = new System.Drawing.Font("Tahoma", 12F);
            this.team_gridView.Appearance.DetailTip.ForeColor = System.Drawing.Color.Black;
            this.team_gridView.Appearance.DetailTip.Options.UseBackColor = true;
            this.team_gridView.Appearance.DetailTip.Options.UseBorderColor = true;
            this.team_gridView.Appearance.DetailTip.Options.UseFont = true;
            this.team_gridView.Appearance.DetailTip.Options.UseForeColor = true;
            this.team_gridView.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.team_gridView.Appearance.EvenRow.Options.UseBackColor = true;
            this.team_gridView.Appearance.FocusedCell.BackColor = System.Drawing.Color.Silver;
            this.team_gridView.Appearance.FocusedCell.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.team_gridView.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.team_gridView.Appearance.FocusedCell.Options.UseBackColor = true;
            this.team_gridView.Appearance.FocusedCell.Options.UseFont = true;
            this.team_gridView.Appearance.FocusedCell.Options.UseForeColor = true;
            this.team_gridView.Appearance.FocusedRow.BackColor = System.Drawing.Color.Gray;
            this.team_gridView.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.team_gridView.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.team_gridView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.team_gridView.Appearance.FocusedRow.Options.UseFont = true;
            this.team_gridView.Appearance.FocusedRow.Options.UseForeColor = true;
            this.team_gridView.Appearance.FooterPanel.BackColor = System.Drawing.Color.SeaGreen;
            this.team_gridView.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.YellowGreen;
            this.team_gridView.Appearance.FooterPanel.Options.UseBackColor = true;
            this.team_gridView.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.team_gridView.Appearance.GroupButton.Options.UseBackColor = true;
            this.team_gridView.Appearance.GroupPanel.BackColor = System.Drawing.Color.ForestGreen;
            this.team_gridView.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.LightGreen;
            this.team_gridView.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.team_gridView.Appearance.GroupPanel.Options.UseBackColor = true;
            this.team_gridView.Appearance.GroupPanel.Options.UseForeColor = true;
            this.team_gridView.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.team_gridView.Appearance.GroupRow.Options.UseBackColor = true;
            this.team_gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.team_gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.team_gridView.Appearance.HorzLine.BackColor = System.Drawing.Color.Silver;
            this.team_gridView.Appearance.HorzLine.Options.UseBackColor = true;
            this.team_gridView.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.team_gridView.Appearance.OddRow.Options.UseBackColor = true;
            this.team_gridView.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.team_gridView.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.team_gridView.Appearance.Row.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.team_gridView.Appearance.Row.Options.UseBackColor = true;
            this.team_gridView.Appearance.Row.Options.UseFont = true;
            this.team_gridView.Appearance.VertLine.BackColor = System.Drawing.Color.Silver;
            this.team_gridView.Appearance.VertLine.Options.UseBackColor = true;
            this.team_gridView.AppearancePrint.Lines.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.team_gridView.AppearancePrint.Lines.Options.UseBackColor = true;
            this.team_gridView.AppearancePrint.Preview.Options.UseTextOptions = true;
            this.team_gridView.AppearancePrint.Preview.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.team_gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNo,
            this.colName,
            this.colIocCode,
            this.colTrainer});
            this.team_gridView.DetailHeight = 294;
            this.team_gridView.FixedLineWidth = 1;
            this.team_gridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.team_gridView.GridControl = this.gridControl1;
            this.team_gridView.Name = "team_gridView";
            this.team_gridView.OptionsBehavior.Editable = false;
            this.team_gridView.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.MouseDown;
            this.team_gridView.OptionsDetail.ShowDetailTabs = false;
            this.team_gridView.OptionsPrint.AutoWidth = false;
            this.team_gridView.OptionsPrint.EnableAppearanceOddRow = true;
            this.team_gridView.OptionsPrint.ExpandAllGroups = false;
            this.team_gridView.OptionsPrint.PrintDetails = true;
            this.team_gridView.OptionsPrint.PrintFixedColumnsOnEveryPage = true;
            this.team_gridView.OptionsPrint.PrintHeader = false;
            this.team_gridView.OptionsPrint.PrintPreview = true;
            this.team_gridView.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.team_gridView.OptionsView.ShowPreviewRowLines = DevExpress.Utils.DefaultBoolean.True;
            this.team_gridView.PaintStyleName = "MixedXP";
            // 
            // colNo
            // 
            this.colNo.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colNo.AppearanceCell.Options.UseFont = true;
            this.colNo.AppearanceCell.Options.UseTextOptions = true;
            this.colNo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNo.AppearanceHeader.BackColor = System.Drawing.Color.Brown;
            this.colNo.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colNo.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.colNo.AppearanceHeader.Options.UseBackColor = true;
            this.colNo.AppearanceHeader.Options.UseFont = true;
            this.colNo.AppearanceHeader.Options.UseForeColor = true;
            this.colNo.AppearanceHeader.Options.UseTextOptions = true;
            this.colNo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNo.FieldName = "No";
            this.colNo.MinWidth = 23;
            this.colNo.Name = "colNo";
            this.colNo.Visible = true;
            this.colNo.VisibleIndex = 0;
            this.colNo.Width = 50;
            // 
            // colName
            // 
            this.colName.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colName.AppearanceCell.Options.UseFont = true;
            this.colName.AppearanceCell.Options.UseTextOptions = true;
            this.colName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.AppearanceHeader.BackColor = System.Drawing.Color.Brown;
            this.colName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colName.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.colName.AppearanceHeader.Options.UseBackColor = true;
            this.colName.AppearanceHeader.Options.UseFont = true;
            this.colName.AppearanceHeader.Options.UseForeColor = true;
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.FieldName = "Name";
            this.colName.MinWidth = 23;
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 1;
            this.colName.Width = 150;
            // 
            // colIocCode
            // 
            this.colIocCode.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colIocCode.AppearanceCell.Options.UseFont = true;
            this.colIocCode.AppearanceCell.Options.UseTextOptions = true;
            this.colIocCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIocCode.AppearanceHeader.BackColor = System.Drawing.Color.Brown;
            this.colIocCode.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colIocCode.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.colIocCode.AppearanceHeader.Options.UseBackColor = true;
            this.colIocCode.AppearanceHeader.Options.UseFont = true;
            this.colIocCode.AppearanceHeader.Options.UseForeColor = true;
            this.colIocCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colIocCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIocCode.FieldName = "IocCode";
            this.colIocCode.MinWidth = 23;
            this.colIocCode.Name = "colIocCode";
            this.colIocCode.Visible = true;
            this.colIocCode.VisibleIndex = 2;
            this.colIocCode.Width = 70;
            // 
            // colTrainer
            // 
            this.colTrainer.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colTrainer.AppearanceCell.Options.UseFont = true;
            this.colTrainer.AppearanceCell.Options.UseTextOptions = true;
            this.colTrainer.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTrainer.AppearanceHeader.BackColor = System.Drawing.Color.Brown;
            this.colTrainer.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colTrainer.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.colTrainer.AppearanceHeader.Options.UseBackColor = true;
            this.colTrainer.AppearanceHeader.Options.UseFont = true;
            this.colTrainer.AppearanceHeader.Options.UseForeColor = true;
            this.colTrainer.AppearanceHeader.Options.UseTextOptions = true;
            this.colTrainer.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTrainer.FieldName = "Trainer";
            this.colTrainer.MinWidth = 23;
            this.colTrainer.Name = "colTrainer";
            this.colTrainer.Visible = true;
            this.colTrainer.VisibleIndex = 3;
            this.colTrainer.Width = 200;
            // 
            // ribbon
            // 
            this.ribbon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ribbon.BackColor = System.Drawing.Color.White;
            this.ribbon.Dock = System.Windows.Forms.DockStyle.None;
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.ForeColor = System.Drawing.Color.White;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.ribbon.SearchEditItem,
            this.display_button,
            this.import_sta_button,
            this.load_json_button,
            this.convert2json_button,
            this.export_json_button,
            this.barButtonItem1,
            this.round1Button,
            this.round2Button,
            this.round3Button,
            this.jumpoffButton,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barCheckItem1,
            this.barButtonItem4,
            this.barButtonItem5,
            this.configrationbarButtonItem,
            this.barButtonItem6,
            this.barDockingMenuItem1,
            this.barButtonGroup1,
            this.barWorkspaceMenuItem1,
            this.barButtonItem7,
            this.barDockingMenuItem2,
            this.startlistForShowbarButtonItem,
            this.barSubItem1,
            this.barSubItem2,
            this.barSubItem3,
            this.barSubItem4,
            this.barHeaderItem1,
            this.round1barButtonItem,
            this.round2barButtonItem,
            this.jumpOffbarButtonItem,
            this.barButtonItem12,
            this.startlistForTVbarButtonItem,
            this.startListR1barButtonItem,
            this.startListR2barButtonItem,
            this.resultOfR1barButtonItem,
            this.resultOfR2barButtonItem,
            this.startListOfCurrentRound,
            this.resultOfCurrentFinishedRound,
            this.finalReportBarButtonItem,
            this.barButtonItem8,
            this.barButtonItem9,
            this.barButtonItem10,
            this.generalSettingbarButtonItem,
            this.competitionSettingbarButtonItem,
            this.startOrderForRound1barButtonItem,
            this.startOrderForR2barButtonItem});
            this.ribbon.Location = new System.Drawing.Point(0, -1);
            this.ribbon.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ribbon.MaxItemId = 56;
            this.ribbon.Name = "ribbon";
            this.ribbon.OptionsPageCategories.AutoCorrectForeColor = DevExpress.Utils.DefaultBoolean.True;
            this.ribbon.OptionsStubGlyphs.ColorMode = DevExpress.Utils.Drawing.GlyphColorMode.Blue;
            this.ribbon.OptionsStubGlyphs.RandomizeColors = false;
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1,
            this.ribbonPage2,
            this.ribbonPage3});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2019;
            this.ribbon.Size = new System.Drawing.Size(3395, 183);
            // 
            // display_button
            // 
            this.display_button.Id = 6;
            this.display_button.Name = "display_button";
            // 
            // import_sta_button
            // 
            this.import_sta_button.Caption = "Import .sta";
            this.import_sta_button.Id = 2;
            this.import_sta_button.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("import_sta_button.ImageOptions.Image")));
            this.import_sta_button.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("import_sta_button.ImageOptions.LargeImage")));
            this.import_sta_button.Name = "import_sta_button";
            this.import_sta_button.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.import_sta_button_ItemClick);
            // 
            // load_json_button
            // 
            this.load_json_button.Caption = "Load HTC";
            this.load_json_button.Id = 3;
            this.load_json_button.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("load_json_button.ImageOptions.Image")));
            this.load_json_button.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("load_json_button.ImageOptions.LargeImage")));
            this.load_json_button.Name = "load_json_button";
            this.load_json_button.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.load_json_button_ItemClick);
            // 
            // convert2json_button
            // 
            this.convert2json_button.Caption = "Convert to HTC";
            this.convert2json_button.Id = 4;
            this.convert2json_button.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("convert2json_button.ImageOptions.Image")));
            this.convert2json_button.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("convert2json_button.ImageOptions.LargeImage")));
            this.convert2json_button.Name = "convert2json_button";
            this.convert2json_button.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.convert2json_button_ItemClick);
            // 
            // export_json_button
            // 
            this.export_json_button.Caption = "Export HTC";
            this.export_json_button.Id = 5;
            this.export_json_button.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("export_json_button.ImageOptions.Image")));
            this.export_json_button.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("export_json_button.ImageOptions.LargeImage")));
            this.export_json_button.Name = "export_json_button";
            this.export_json_button.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.export_json_button_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Report";
            this.barButtonItem1.Id = 8;
            this.barButtonItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.Image")));
            this.barButtonItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.LargeImage")));
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // round1Button
            // 
            this.round1Button.Caption = "Round 1";
            this.round1Button.Id = 11;
            this.round1Button.Name = "round1Button";
            this.round1Button.Tag = "round1";
            // 
            // round2Button
            // 
            this.round2Button.Caption = "Round 2";
            this.round2Button.Id = 12;
            this.round2Button.Name = "round2Button";
            // 
            // round3Button
            // 
            this.round3Button.Caption = "Round 3";
            this.round3Button.Id = 13;
            this.round3Button.Name = "round3Button";
            // 
            // jumpoffButton
            // 
            this.jumpoffButton.Caption = "Jump Off";
            this.jumpoffButton.Id = 14;
            this.jumpoffButton.Name = "jumpoffButton";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Round 1";
            this.barButtonItem2.Id = 15;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Round 2";
            this.barButtonItem3.Id = 16;
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barCheckItem1
            // 
            this.barCheckItem1.Caption = "r1";
            this.barCheckItem1.Id = 17;
            this.barCheckItem1.Name = "barCheckItem1";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "r2";
            this.barButtonItem4.Id = 18;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Id = 19;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // configrationbarButtonItem
            // 
            this.configrationbarButtonItem.Caption = "Configration";
            this.configrationbarButtonItem.Id = 20;
            this.configrationbarButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("configrationbarButtonItem.ImageOptions.Image")));
            this.configrationbarButtonItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("configrationbarButtonItem.ImageOptions.LargeImage")));
            this.configrationbarButtonItem.Name = "configrationbarButtonItem";
            this.configrationbarButtonItem.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "barButtonItem6";
            this.barButtonItem6.Id = 21;
            this.barButtonItem6.Name = "barButtonItem6";
            // 
            // barDockingMenuItem1
            // 
            this.barDockingMenuItem1.Id = 25;
            this.barDockingMenuItem1.Name = "barDockingMenuItem1";
            // 
            // barButtonGroup1
            // 
            this.barButtonGroup1.Caption = "barButtonGroup1";
            this.barButtonGroup1.Id = 23;
            this.barButtonGroup1.Name = "barButtonGroup1";
            // 
            // barWorkspaceMenuItem1
            // 
            this.barWorkspaceMenuItem1.Id = 27;
            this.barWorkspaceMenuItem1.Name = "barWorkspaceMenuItem1";
            this.barWorkspaceMenuItem1.WorkspaceManager = this.workspaceManager1;
            // 
            // workspaceManager1
            // 
            this.workspaceManager1.TargetControl = this;
            this.workspaceManager1.TransitionType = pushTransition1;
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Id = 29;
            this.barButtonItem7.Name = "barButtonItem7";
            // 
            // barDockingMenuItem2
            // 
            this.barDockingMenuItem2.Id = 30;
            this.barDockingMenuItem2.Name = "barDockingMenuItem2";
            // 
            // startlistForShowbarButtonItem
            // 
            this.startlistForShowbarButtonItem.ActAsDropDown = true;
            this.startlistForShowbarButtonItem.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.startlistForShowbarButtonItem.Caption = "StartList For Show";
            this.startlistForShowbarButtonItem.DropDownControl = this.popupMenu1;
            this.startlistForShowbarButtonItem.Id = 31;
            this.startlistForShowbarButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("startlistForShowbarButtonItem.ImageOptions.Image")));
            this.startlistForShowbarButtonItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("startlistForShowbarButtonItem.ImageOptions.LargeImage")));
            this.startlistForShowbarButtonItem.Name = "startlistForShowbarButtonItem";
            // 
            // popupMenu1
            // 
            this.popupMenu1.ItemLinks.Add(this.round1barButtonItem);
            this.popupMenu1.ItemLinks.Add(this.round2barButtonItem);
            this.popupMenu1.ItemLinks.Add(this.jumpOffbarButtonItem);
            this.popupMenu1.Name = "popupMenu1";
            this.popupMenu1.Ribbon = this.ribbon;
            // 
            // round1barButtonItem
            // 
            this.round1barButtonItem.Caption = "Round1";
            this.round1barButtonItem.Id = 37;
            this.round1barButtonItem.Name = "round1barButtonItem";
            this.round1barButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.round1barButtonItem_ItemClick);
            // 
            // round2barButtonItem
            // 
            this.round2barButtonItem.Caption = "Round2";
            this.round2barButtonItem.Id = 38;
            this.round2barButtonItem.Name = "round2barButtonItem";
            this.round2barButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.round2barButtonItem_ItemClick);
            // 
            // jumpOffbarButtonItem
            // 
            this.jumpOffbarButtonItem.Caption = "JumpOff";
            this.jumpOffbarButtonItem.Id = 39;
            this.jumpOffbarButtonItem.Name = "jumpOffbarButtonItem";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Round1";
            this.barSubItem1.Id = 32;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "barSubItem2";
            this.barSubItem2.Id = 33;
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barSubItem3
            // 
            this.barSubItem3.Caption = "Round2";
            this.barSubItem3.Id = 34;
            this.barSubItem3.Name = "barSubItem3";
            // 
            // barSubItem4
            // 
            this.barSubItem4.Caption = "JumpOff";
            this.barSubItem4.Id = 35;
            this.barSubItem4.Name = "barSubItem4";
            // 
            // barHeaderItem1
            // 
            this.barHeaderItem1.Id = 36;
            this.barHeaderItem1.Name = "barHeaderItem1";
            // 
            // barButtonItem12
            // 
            this.barButtonItem12.Caption = "TV1.DAT";
            this.barButtonItem12.Id = 40;
            this.barButtonItem12.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem12.ImageOptions.Image")));
            this.barButtonItem12.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem12.ImageOptions.LargeImage")));
            this.barButtonItem12.Name = "barButtonItem12";
            // 
            // startlistForTVbarButtonItem
            // 
            this.startlistForTVbarButtonItem.Caption = "StartList For TV";
            this.startlistForTVbarButtonItem.Id = 41;
            this.startlistForTVbarButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("startlistForTVbarButtonItem.ImageOptions.Image")));
            this.startlistForTVbarButtonItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("startlistForTVbarButtonItem.ImageOptions.LargeImage")));
            this.startlistForTVbarButtonItem.Name = "startlistForTVbarButtonItem";
            this.startlistForTVbarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.startlistForTVbarButtonItem_ItemClick);
            // 
            // startListR1barButtonItem
            // 
            this.startListR1barButtonItem.Caption = "StartList R1";
            this.startListR1barButtonItem.Id = 42;
            this.startListR1barButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("startListR1barButtonItem.ImageOptions.Image")));
            this.startListR1barButtonItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("startListR1barButtonItem.ImageOptions.LargeImage")));
            this.startListR1barButtonItem.Name = "startListR1barButtonItem";
            this.startListR1barButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.startListR1barButtonItem_ItemClick);
            this.startListR1barButtonItem.ItemPress += new DevExpress.XtraBars.ItemClickEventHandler(this.startListR1barButtonItem_ItemPress);
            // 
            // startListR2barButtonItem
            // 
            this.startListR2barButtonItem.Caption = "StartList R2";
            this.startListR2barButtonItem.Id = 43;
            this.startListR2barButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("startListR2barButtonItem.ImageOptions.Image")));
            this.startListR2barButtonItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("startListR2barButtonItem.ImageOptions.LargeImage")));
            this.startListR2barButtonItem.Name = "startListR2barButtonItem";
            this.startListR2barButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.startListR2barButtonItem_ItemClick);
            this.startListR2barButtonItem.ItemPress += new DevExpress.XtraBars.ItemClickEventHandler(this.startListR2barButtonItem_ItemPress);
            // 
            // resultOfR1barButtonItem
            // 
            this.resultOfR1barButtonItem.Caption = "Result R1";
            this.resultOfR1barButtonItem.Id = 44;
            this.resultOfR1barButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resultOfR1barButtonItem.ImageOptions.Image")));
            this.resultOfR1barButtonItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("resultOfR1barButtonItem.ImageOptions.LargeImage")));
            this.resultOfR1barButtonItem.Name = "resultOfR1barButtonItem";
            this.resultOfR1barButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.resultOfR1barButtonItem_ItemClick);
            this.resultOfR1barButtonItem.ItemPress += new DevExpress.XtraBars.ItemClickEventHandler(this.resultOfR1barButtonItem_ItemPress);
            // 
            // resultOfR2barButtonItem
            // 
            this.resultOfR2barButtonItem.Caption = "Result R2";
            this.resultOfR2barButtonItem.Id = 45;
            this.resultOfR2barButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resultOfR2barButtonItem.ImageOptions.Image")));
            this.resultOfR2barButtonItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("resultOfR2barButtonItem.ImageOptions.LargeImage")));
            this.resultOfR2barButtonItem.Name = "resultOfR2barButtonItem";
            this.resultOfR2barButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.resultOfR2barButtonItem_ItemClick);
            this.resultOfR2barButtonItem.ItemPress += new DevExpress.XtraBars.ItemClickEventHandler(this.resultOfR2barButtonItem_ItemPress);
            // 
            // startListOfCurrentRound
            // 
            this.startListOfCurrentRound.Caption = "Current Round StartList";
            this.startListOfCurrentRound.Id = 46;
            this.startListOfCurrentRound.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("startListOfCurrentRound.ImageOptions.Image")));
            this.startListOfCurrentRound.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("startListOfCurrentRound.ImageOptions.LargeImage")));
            this.startListOfCurrentRound.Name = "startListOfCurrentRound";
            this.startListOfCurrentRound.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.startListOfCurrentRound_ItemClick);
            this.startListOfCurrentRound.ItemPress += new DevExpress.XtraBars.ItemClickEventHandler(this.startListOfCurrentRound_ItemPress);
            // 
            // resultOfCurrentFinishedRound
            // 
            this.resultOfCurrentFinishedRound.Caption = "Current Finished Round Result";
            this.resultOfCurrentFinishedRound.Id = 47;
            this.resultOfCurrentFinishedRound.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resultOfCurrentFinishedRound.ImageOptions.Image")));
            this.resultOfCurrentFinishedRound.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("resultOfCurrentFinishedRound.ImageOptions.LargeImage")));
            this.resultOfCurrentFinishedRound.Name = "resultOfCurrentFinishedRound";
            this.resultOfCurrentFinishedRound.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.resultOfCurrentFinishedRound_ItemClick);
            this.resultOfCurrentFinishedRound.ItemPress += new DevExpress.XtraBars.ItemClickEventHandler(this.resultOfCurrentFinishedRound_ItemPress);
            // 
            // finalReportBarButtonItem
            // 
            this.finalReportBarButtonItem.Caption = "Final Report";
            this.finalReportBarButtonItem.Id = 48;
            this.finalReportBarButtonItem.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("finalReportBarButtonItem.ImageOptions.Image")));
            this.finalReportBarButtonItem.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("finalReportBarButtonItem.ImageOptions.LargeImage")));
            this.finalReportBarButtonItem.Name = "finalReportBarButtonItem";
            this.finalReportBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.finalReportBarButtonItem_ItemClick);
            this.finalReportBarButtonItem.ItemPress += new DevExpress.XtraBars.ItemClickEventHandler(this.finalReportBarButtonItem_ItemPress);
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "Lock";
            this.barButtonItem8.Id = 49;
            this.barButtonItem8.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem8.ImageOptions.LargeImage")));
            this.barButtonItem8.Name = "barButtonItem8";
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "lock";
            this.barButtonItem9.Id = 50;
            this.barButtonItem9.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem9.ImageOptions.Image")));
            this.barButtonItem9.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem9.ImageOptions.LargeImage")));
            this.barButtonItem9.Name = "barButtonItem9";
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "barButtonItem10";
            this.barButtonItem10.Id = 51;
            this.barButtonItem10.Name = "barButtonItem10";
            // 
            // generalSettingbarButtonItem
            // 
            this.generalSettingbarButtonItem.Caption = "General Settings";
            this.generalSettingbarButtonItem.Id = 52;
            this.generalSettingbarButtonItem.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("generalSettingbarButtonItem.ImageOptions.SvgImage")));
            this.generalSettingbarButtonItem.Name = "generalSettingbarButtonItem";
            this.generalSettingbarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.generalSettingbarButtonItem_ItemClick);
            // 
            // competitionSettingbarButtonItem
            // 
            this.competitionSettingbarButtonItem.Caption = "Competition Settings";
            this.competitionSettingbarButtonItem.Id = 53;
            this.competitionSettingbarButtonItem.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("competitionSettingbarButtonItem.ImageOptions.SvgImage")));
            this.competitionSettingbarButtonItem.Name = "competitionSettingbarButtonItem";
            this.competitionSettingbarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.competitionSettingbarButtonItem_ItemClick);
            // 
            // startOrderForRound1barButtonItem
            // 
            this.startOrderForRound1barButtonItem.Caption = "Starting Order R1";
            this.startOrderForRound1barButtonItem.Id = 54;
            this.startOrderForRound1barButtonItem.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("startOrderForRound1barButtonItem.ImageOptions.SvgImage")));
            this.startOrderForRound1barButtonItem.Name = "startOrderForRound1barButtonItem";
            this.startOrderForRound1barButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.startOrderForRound1barButtonItem_ItemClick);
            this.startOrderForRound1barButtonItem.ItemPress += new DevExpress.XtraBars.ItemClickEventHandler(this.startOrderForRound1barButtonItem_ItemPress);
            // 
            // startOrderForR2barButtonItem
            // 
            this.startOrderForR2barButtonItem.Caption = "Starting Order R2";
            this.startOrderForR2barButtonItem.Id = 55;
            this.startOrderForR2barButtonItem.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("startOrderForR2barButtonItem.ImageOptions.SvgImage")));
            this.startOrderForR2barButtonItem.Name = "startOrderForR2barButtonItem";
            this.startOrderForR2barButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.startOrderForR2barButtonItem_ItemClick);
            this.startOrderForR2barButtonItem.ItemPress += new DevExpress.XtraBars.ItemClickEventHandler(this.startOrderForR2barButtonItem_ItemPress);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup5,
            this.ribbonPageGroup6});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Home";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.import_sta_button);
            this.ribbonPageGroup1.ItemLinks.Add(this.convert2json_button);
            this.ribbonPageGroup1.ItemLinks.Add(this.load_json_button, true);
            this.ribbonPageGroup1.ItemLinks.Add(this.export_json_button);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "Report";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.startlistForShowbarButtonItem);
            this.ribbonPageGroup5.ItemLinks.Add(this.startlistForTVbarButtonItem);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "Startlists";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.configrationbarButtonItem);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.Text = "Setup";
            this.ribbonPageGroup6.Visible = false;
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2,
            this.ribbonPageGroup3,
            this.ribbonPageGroup4});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "Fast Report";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.startOrderForRound1barButtonItem);
            this.ribbonPageGroup2.ItemLinks.Add(this.startListR1barButtonItem);
            this.ribbonPageGroup2.ItemLinks.Add(this.resultOfR1barButtonItem);
            this.ribbonPageGroup2.ItemLinks.Add(this.startOrderForR2barButtonItem);
            this.ribbonPageGroup2.ItemLinks.Add(this.startListR2barButtonItem);
            this.ribbonPageGroup2.ItemLinks.Add(this.resultOfR2barButtonItem);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "All Fast Report";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.startListOfCurrentRound);
            this.ribbonPageGroup3.ItemLinks.Add(this.resultOfCurrentFinishedRound);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Quick Fast Report";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.finalReportBarButtonItem);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "Final Report";
            // 
            // ribbonPage3
            // 
            this.ribbonPage3.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup7});
            this.ribbonPage3.Name = "ribbonPage3";
            this.ribbonPage3.Text = "Setting";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.ItemLinks.Add(this.generalSettingbarButtonItem);
            this.ribbonPageGroup7.ItemLinks.Add(this.competitionSettingbarButtonItem);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.Text = "All Settings";
            // 
            // BtnLock
            // 
            this.BtnLock.AutoHeight = false;
            this.BtnLock.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)});
            this.BtnLock.Name = "BtnLock";
            this.BtnLock.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // Lock
            // 
            this.Lock.AutoHeight = false;
            this.Lock.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)});
            this.Lock.Name = "Lock";
            this.Lock.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // Unlock
            // 
            this.Unlock.AutoHeight = false;
            this.Unlock.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)});
            this.Unlock.Name = "Unlock";
            this.Unlock.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // Save
            // 
            this.Save.AutoHeight = false;
            this.Save.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)});
            this.Save.Name = "Save";
            this.Save.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // Cancel
            // 
            this.Cancel.AutoHeight = false;
            this.Cancel.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)});
            this.Cancel.Name = "Cancel";
            this.Cancel.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            // 
            // round2_gridView
            // 
            this.round2_gridView.Appearance.EvenRow.BackColor = System.Drawing.Color.Pink;
            this.round2_gridView.Appearance.EvenRow.Font = new System.Drawing.Font("Tahoma", 9F);
            this.round2_gridView.Appearance.EvenRow.Options.UseBackColor = true;
            this.round2_gridView.Appearance.EvenRow.Options.UseFont = true;
            this.round2_gridView.Appearance.EvenRow.Options.UseTextOptions = true;
            this.round2_gridView.Appearance.EvenRow.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.round2_gridView.Appearance.FilterPanel.BackColor = System.Drawing.Color.ForestGreen;
            this.round2_gridView.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.LightGreen;
            this.round2_gridView.Appearance.FilterPanel.Options.UseBackColor = true;
            this.round2_gridView.Appearance.FocusedCell.BackColor = System.Drawing.Color.Silver;
            this.round2_gridView.Appearance.FocusedCell.Options.UseBackColor = true;
            this.round2_gridView.Appearance.FocusedRow.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.round2_gridView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.round2_gridView.Appearance.GroupPanel.BackColor = System.Drawing.Color.ForestGreen;
            this.round2_gridView.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.LightGreen;
            this.round2_gridView.Appearance.GroupPanel.Options.UseBackColor = true;
            this.round2_gridView.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Maroon;
            this.round2_gridView.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.round2_gridView.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.White;
            this.round2_gridView.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.round2_gridView.Appearance.HeaderPanel.Options.UseFont = true;
            this.round2_gridView.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.round2_gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.round2_gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.round2_gridView.Appearance.HorzLine.Font = new System.Drawing.Font("Tahoma", 9F);
            this.round2_gridView.Appearance.HorzLine.Options.UseFont = true;
            this.round2_gridView.Appearance.HorzLine.Options.UseTextOptions = true;
            this.round2_gridView.Appearance.HorzLine.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.round2_gridView.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.round2_gridView.Appearance.OddRow.Font = new System.Drawing.Font("Tahoma", 9F);
            this.round2_gridView.Appearance.OddRow.Options.UseBackColor = true;
            this.round2_gridView.Appearance.OddRow.Options.UseFont = true;
            this.round2_gridView.Appearance.OddRow.Options.UseTextOptions = true;
            this.round2_gridView.Appearance.OddRow.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.round2_gridView.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.round2_gridView.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.round2_gridView.Appearance.Row.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.round2_gridView.Appearance.Row.Options.UseBackColor = true;
            this.round2_gridView.Appearance.Row.Options.UseFont = true;
            this.round2_gridView.Appearance.Row.Options.UseTextOptions = true;
            this.round2_gridView.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.round2_gridView.Appearance.VertLine.Font = new System.Drawing.Font("Tahoma", 9F);
            this.round2_gridView.Appearance.VertLine.Options.UseFont = true;
            this.round2_gridView.Appearance.VertLine.Options.UseTextOptions = true;
            this.round2_gridView.Appearance.VertLine.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.round2_gridView.DetailHeight = 296;
            this.round2_gridView.FixedLineWidth = 3;
            this.round2_gridView.GridControl = this.gridControl2;
            this.round2_gridView.Name = "round2_gridView";
            this.round2_gridView.OptionsBehavior.Editable = false;
            this.round2_gridView.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.round2_gridView.OptionsBehavior.KeepGroupExpandedOnSorting = false;
            this.round2_gridView.OptionsCustomization.AllowSort = false;
            this.round2_gridView.OptionsMenu.ShowGroupSortSummaryItems = false;
            this.round2_gridView.OptionsPrint.PrintHorzLines = false;
            this.round2_gridView.OptionsPrint.PrintVertLines = false;
            this.round2_gridView.OptionsView.ShowGroupPanel = false;
            this.round2_gridView.PaintStyleName = "UltraFlat";
            this.round2_gridView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.round2_gridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.round2_gridView_KeyDown);
            // 
            // gridControl2
            // 
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.round1_gridView;
            this.gridControl2.MenuManager = this.ribbon;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(748, 251);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.TabStop = false;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.round1_gridView,
            this.round3_gridView,
            this.jumpOff_gridView,
            this.round2_gridView});
            // 
            // round1_gridView
            // 
            this.round1_gridView.Appearance.FocusedCell.BackColor = System.Drawing.Color.Silver;
            this.round1_gridView.Appearance.FocusedCell.Options.UseBackColor = true;
            this.round1_gridView.Appearance.FocusedRow.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.round1_gridView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.round1_gridView.Appearance.GroupPanel.BackColor = System.Drawing.Color.ForestGreen;
            this.round1_gridView.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.LightGreen;
            this.round1_gridView.Appearance.GroupPanel.Options.UseBackColor = true;
            this.round1_gridView.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Maroon;
            this.round1_gridView.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.round1_gridView.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.White;
            this.round1_gridView.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.round1_gridView.Appearance.HeaderPanel.Options.UseFont = true;
            this.round1_gridView.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.round1_gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.round1_gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.round1_gridView.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.round1_gridView.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.round1_gridView.Appearance.Row.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.round1_gridView.Appearance.Row.Options.UseBackColor = true;
            this.round1_gridView.Appearance.Row.Options.UseFont = true;
            this.round1_gridView.Appearance.Row.Options.UseTextOptions = true;
            this.round1_gridView.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.round1_gridView.Appearance.SelectedRow.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.round1_gridView.Appearance.SelectedRow.Options.UseBackColor = true;
            this.round1_gridView.DetailHeight = 431;
            this.round1_gridView.FixedLineWidth = 3;
            this.round1_gridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.round1_gridView.GridControl = this.gridControl2;
            this.round1_gridView.Name = "round1_gridView";
            this.round1_gridView.OptionsBehavior.Editable = false;
            this.round1_gridView.OptionsBehavior.EditorShowMode = DevExpress.Utils.EditorShowMode.Click;
            this.round1_gridView.OptionsBehavior.KeepGroupExpandedOnSorting = false;
            this.round1_gridView.OptionsCustomization.AllowSort = false;
            this.round1_gridView.OptionsDetail.EnableMasterViewMode = false;
            this.round1_gridView.OptionsMenu.ShowGroupSortSummaryItems = false;
            this.round1_gridView.OptionsView.ShowGroupPanel = false;
            this.round1_gridView.PaintStyleName = "UltraFlat";
            this.round1_gridView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.round1_gridView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.round1_gridView_KeyDown);
            // 
            // round3_gridView
            // 
            this.round3_gridView.Appearance.EvenRow.BackColor = System.Drawing.Color.Pink;
            this.round3_gridView.Appearance.EvenRow.Font = new System.Drawing.Font("Tahoma", 9F);
            this.round3_gridView.Appearance.EvenRow.Options.UseBackColor = true;
            this.round3_gridView.Appearance.EvenRow.Options.UseFont = true;
            this.round3_gridView.Appearance.EvenRow.Options.UseTextOptions = true;
            this.round3_gridView.Appearance.EvenRow.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.round3_gridView.Appearance.FilterPanel.BackColor = System.Drawing.Color.ForestGreen;
            this.round3_gridView.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.LightGreen;
            this.round3_gridView.Appearance.FilterPanel.Options.UseBackColor = true;
            this.round3_gridView.Appearance.FocusedCell.BackColor = System.Drawing.Color.LightPink;
            this.round3_gridView.Appearance.FocusedCell.Font = new System.Drawing.Font("Tahoma", 9F);
            this.round3_gridView.Appearance.FocusedCell.Options.UseBackColor = true;
            this.round3_gridView.Appearance.FocusedCell.Options.UseFont = true;
            this.round3_gridView.Appearance.FocusedRow.BackColor = System.Drawing.Color.PaleVioletRed;
            this.round3_gridView.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9F);
            this.round3_gridView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.round3_gridView.Appearance.FocusedRow.Options.UseFont = true;
            this.round3_gridView.Appearance.GroupPanel.BackColor = System.Drawing.Color.ForestGreen;
            this.round3_gridView.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.LightGreen;
            this.round3_gridView.Appearance.GroupPanel.Options.UseBackColor = true;
            this.round3_gridView.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Brown;
            this.round3_gridView.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.round3_gridView.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.White;
            this.round3_gridView.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.round3_gridView.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.round3_gridView.Appearance.HeaderPanel.Options.UseFont = true;
            this.round3_gridView.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.round3_gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.round3_gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.round3_gridView.Appearance.OddRow.BackColor = System.Drawing.Color.PaleVioletRed;
            this.round3_gridView.Appearance.OddRow.Font = new System.Drawing.Font("Tahoma", 9F);
            this.round3_gridView.Appearance.OddRow.Options.UseBackColor = true;
            this.round3_gridView.Appearance.OddRow.Options.UseFont = true;
            this.round3_gridView.Appearance.OddRow.Options.UseTextOptions = true;
            this.round3_gridView.Appearance.OddRow.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.round3_gridView.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.round3_gridView.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.round3_gridView.Appearance.Row.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.round3_gridView.Appearance.Row.Options.UseBackColor = true;
            this.round3_gridView.Appearance.Row.Options.UseBorderColor = true;
            this.round3_gridView.Appearance.Row.Options.UseFont = true;
            this.round3_gridView.Appearance.Row.Options.UseForeColor = true;
            this.round3_gridView.Appearance.Row.Options.UseTextOptions = true;
            this.round3_gridView.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.round3_gridView.DetailHeight = 296;
            this.round3_gridView.FixedLineWidth = 3;
            this.round3_gridView.GridControl = this.gridControl2;
            this.round3_gridView.Name = "round3_gridView";
            this.round3_gridView.OptionsBehavior.Editable = false;
            this.round3_gridView.OptionsBehavior.KeepGroupExpandedOnSorting = false;
            this.round3_gridView.OptionsCustomization.AllowSort = false;
            this.round3_gridView.OptionsMenu.ShowGroupSortSummaryItems = false;
            this.round3_gridView.OptionsView.ShowGroupPanel = false;
            this.round3_gridView.PaintStyleName = "UltraFlat";
            // 
            // jumpOff_gridView
            // 
            this.jumpOff_gridView.Appearance.EvenRow.BackColor = System.Drawing.Color.Pink;
            this.jumpOff_gridView.Appearance.EvenRow.Font = new System.Drawing.Font("Tahoma", 9F);
            this.jumpOff_gridView.Appearance.EvenRow.Options.UseBackColor = true;
            this.jumpOff_gridView.Appearance.EvenRow.Options.UseFont = true;
            this.jumpOff_gridView.Appearance.EvenRow.Options.UseTextOptions = true;
            this.jumpOff_gridView.Appearance.EvenRow.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.jumpOff_gridView.Appearance.FocusedCell.BackColor = System.Drawing.Color.Silver;
            this.jumpOff_gridView.Appearance.FocusedCell.Font = new System.Drawing.Font("Tahoma", 9F);
            this.jumpOff_gridView.Appearance.FocusedCell.Options.UseBackColor = true;
            this.jumpOff_gridView.Appearance.FocusedCell.Options.UseFont = true;
            this.jumpOff_gridView.Appearance.FocusedCell.Options.UseTextOptions = true;
            this.jumpOff_gridView.Appearance.FocusedCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.jumpOff_gridView.Appearance.FocusedRow.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.jumpOff_gridView.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9F);
            this.jumpOff_gridView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.jumpOff_gridView.Appearance.FocusedRow.Options.UseFont = true;
            this.jumpOff_gridView.Appearance.FocusedRow.Options.UseTextOptions = true;
            this.jumpOff_gridView.Appearance.FocusedRow.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.jumpOff_gridView.Appearance.GroupPanel.BackColor = System.Drawing.Color.ForestGreen;
            this.jumpOff_gridView.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.LightGreen;
            this.jumpOff_gridView.Appearance.GroupPanel.Options.UseBackColor = true;
            this.jumpOff_gridView.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Brown;
            this.jumpOff_gridView.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.jumpOff_gridView.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.White;
            this.jumpOff_gridView.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.jumpOff_gridView.Appearance.HeaderPanel.Options.UseFont = true;
            this.jumpOff_gridView.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.jumpOff_gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.jumpOff_gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.jumpOff_gridView.Appearance.OddRow.BackColor = System.Drawing.Color.PaleVioletRed;
            this.jumpOff_gridView.Appearance.OddRow.Options.UseBackColor = true;
            this.jumpOff_gridView.Appearance.OddRow.Options.UseTextOptions = true;
            this.jumpOff_gridView.Appearance.OddRow.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.jumpOff_gridView.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.jumpOff_gridView.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.jumpOff_gridView.Appearance.Row.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.jumpOff_gridView.Appearance.Row.Options.UseBackColor = true;
            this.jumpOff_gridView.Appearance.Row.Options.UseFont = true;
            this.jumpOff_gridView.Appearance.Row.Options.UseTextOptions = true;
            this.jumpOff_gridView.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.jumpOff_gridView.DetailHeight = 296;
            this.jumpOff_gridView.FixedLineWidth = 3;
            this.jumpOff_gridView.GridControl = this.gridControl2;
            this.jumpOff_gridView.Name = "jumpOff_gridView";
            this.jumpOff_gridView.OptionsBehavior.Editable = false;
            this.jumpOff_gridView.OptionsBehavior.KeepGroupExpandedOnSorting = false;
            this.jumpOff_gridView.OptionsCustomization.AllowSort = false;
            this.jumpOff_gridView.OptionsMenu.ShowGroupSortSummaryItems = false;
            this.jumpOff_gridView.OptionsView.ShowGroupPanel = false;
            this.jumpOff_gridView.PaintStyleName = "UltraFlat";
            this.jumpOff_gridView.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.jumpoff_ergebnis_CellValueChanged);
            // 
            // teamCompetitionBindingSource
            // 
            this.teamCompetitionBindingSource.DataSource = typeof(DataDisplay.TeamCompetition);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerControl1.Location = new System.Drawing.Point(9, 190);
            this.splitContainerControl1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl3);
            this.splitContainerControl1.Panel2.Controls.Add(this.printButton);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1490, 693);
            this.splitContainerControl1.SplitterPosition = 748;
            this.splitContainerControl1.TabIndex = 18;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.navBarControl2);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.splitContainerControl4);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(748, 693);
            this.splitContainerControl2.SplitterPosition = 79;
            this.splitContainerControl2.TabIndex = 0;
            // 
            // navBarControl2
            // 
            this.navBarControl2.ActiveGroup = this.eventNumLabel;
            this.navBarControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navBarControl2.Font = new System.Drawing.Font("Tahoma", 7F);
            this.navBarControl2.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.eventNumLabel});
            this.navBarControl2.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.eventNameLabel});
            this.navBarControl2.Location = new System.Drawing.Point(0, 0);
            this.navBarControl2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.navBarControl2.Name = "navBarControl2";
            this.navBarControl2.OptionsNavPane.ExpandedWidth = 748;
            this.navBarControl2.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.ExplorerBar;
            this.navBarControl2.Size = new System.Drawing.Size(748, 79);
            this.navBarControl2.TabIndex = 5;
            this.navBarControl2.Text = "navBarControl2";
            this.navBarControl2.View = new DevExpress.XtraNavBar.ViewInfo.StandardSkinExplorerBarViewInfoRegistrator("VS2010");
            // 
            // eventNumLabel
            // 
            this.eventNumLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.eventNumLabel.Appearance.Options.UseFont = true;
            this.eventNumLabel.Caption = "Event No    Competition Name";
            this.eventNumLabel.Expanded = true;
            this.eventNumLabel.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.eventNameLabel)});
            this.eventNumLabel.Name = "eventNumLabel";
            // 
            // eventNameLabel
            // 
            this.eventNameLabel.Appearance.Font = new System.Drawing.Font("Tahoma", 8F);
            this.eventNameLabel.Appearance.Options.UseFont = true;
            this.eventNameLabel.Caption = "Event Name                Number of Starter";
            this.eventNameLabel.Enabled = false;
            this.eventNameLabel.Name = "eventNameLabel";
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl4.Horizontal = false;
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.Controls.Add(this.gridControl2);
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel2.Controls.Add(this.elButton);
            this.splitContainerControl4.Panel2.Controls.Add(this.rtButton);
            this.splitContainerControl4.Panel2.Controls.Add(this.dqButton);
            this.splitContainerControl4.Panel2.Controls.Add(this.timeLabelControl);
            this.splitContainerControl4.Panel2.Controls.Add(this.dnsButton);
            this.splitContainerControl4.Panel2.Controls.Add(this.PenaltyLabelControl);
            this.splitContainerControl4.Panel2.Controls.Add(this.timekeepingTimeTextEdit);
            this.splitContainerControl4.Panel2.Controls.Add(this.timekeepingPenaltyTextEdit);
            this.splitContainerControl4.Panel2.Controls.Add(this.timeAllowedLabel);
            this.splitContainerControl4.Panel2.Controls.Add(this.Label_FZ);
            this.splitContainerControl4.Panel2.Controls.Add(this.ProgressBar);
            this.splitContainerControl4.Panel2.Controls.Add(this.confirmButton);
            this.splitContainerControl4.Panel2.Controls.Add(this.chooseRound_comboBox);
            this.splitContainerControl4.Panel2.Controls.Add(this.roundFinishButton);
            this.splitContainerControl4.Panel2.Controls.Add(this.takeOverButton);
            this.splitContainerControl4.Panel2.Controls.Add(this.okButton);
            this.splitContainerControl4.Panel2.Controls.Add(this.labelControl11);
            this.splitContainerControl4.Panel2.Controls.Add(this.ioctextEdit);
            this.splitContainerControl4.Panel2.Controls.Add(this.labelControl10);
            this.splitContainerControl4.Panel2.Controls.Add(this.labelControl9);
            this.splitContainerControl4.Panel2.Controls.Add(this.totaltextEdit);
            this.splitContainerControl4.Panel2.Controls.Add(this.labelControl8);
            this.splitContainerControl4.Panel2.Controls.Add(this.timePenstextEdit);
            this.splitContainerControl4.Panel2.Controls.Add(this.labelControl7);
            this.splitContainerControl4.Panel2.Controls.Add(this.penaltiestextEdit);
            this.splitContainerControl4.Panel2.Controls.Add(this.labelControl6);
            this.splitContainerControl4.Panel2.Controls.Add(this.timetextEdit);
            this.splitContainerControl4.Panel2.Controls.Add(this.labelControl5);
            this.splitContainerControl4.Panel2.Controls.Add(this.teamtextEdit);
            this.splitContainerControl4.Panel2.Controls.Add(this.labelControl4);
            this.splitContainerControl4.Panel2.Controls.Add(this.riderNametextEdit);
            this.splitContainerControl4.Panel2.Controls.Add(this.labelControl3);
            this.splitContainerControl4.Panel2.Controls.Add(this.horseNametextEdit);
            this.splitContainerControl4.Panel2.Controls.Add(this.labelControl2);
            this.splitContainerControl4.Panel2.Controls.Add(this.cnrtextEdit);
            this.splitContainerControl4.Panel2.Controls.Add(this.labelControl1);
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.Size = new System.Drawing.Size(748, 602);
            this.splitContainerControl4.SplitterPosition = 251;
            this.splitContainerControl4.TabIndex = 59;
            // 
            // elButton
            // 
            this.elButton.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.elButton.Appearance.Options.UseBackColor = true;
            this.elButton.Location = new System.Drawing.Point(551, 173);
            this.elButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.elButton.Name = "elButton";
            this.elButton.Size = new System.Drawing.Size(44, 29);
            this.elButton.TabIndex = 22;
            this.elButton.TabStop = false;
            this.elButton.Text = "EL";
            this.elButton.Click += new System.EventHandler(this.elButton_Click);
            // 
            // rtButton
            // 
            this.rtButton.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.rtButton.Appearance.Options.UseBackColor = true;
            this.rtButton.Location = new System.Drawing.Point(600, 173);
            this.rtButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.rtButton.Name = "rtButton";
            this.rtButton.Size = new System.Drawing.Size(44, 29);
            this.rtButton.TabIndex = 92;
            this.rtButton.TabStop = false;
            this.rtButton.Text = "RT";
            this.rtButton.Click += new System.EventHandler(this.rtButton_Click);
            // 
            // dqButton
            // 
            this.dqButton.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dqButton.Appearance.Options.UseBackColor = true;
            this.dqButton.Location = new System.Drawing.Point(551, 207);
            this.dqButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dqButton.Name = "dqButton";
            this.dqButton.Size = new System.Drawing.Size(44, 29);
            this.dqButton.TabIndex = 93;
            this.dqButton.TabStop = false;
            this.dqButton.Text = "DQ";
            this.dqButton.Click += new System.EventHandler(this.dqButton_Click);
            // 
            // timeLabelControl
            // 
            this.timeLabelControl.Location = new System.Drawing.Point(280, 178);
            this.timeLabelControl.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.timeLabelControl.Name = "timeLabelControl";
            this.timeLabelControl.Size = new System.Drawing.Size(29, 16);
            this.timeLabelControl.TabIndex = 104;
            this.timeLabelControl.Text = "Time";
            // 
            // dnsButton
            // 
            this.dnsButton.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.dnsButton.Appearance.Options.UseBackColor = true;
            this.dnsButton.Location = new System.Drawing.Point(600, 207);
            this.dnsButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dnsButton.Name = "dnsButton";
            this.dnsButton.Size = new System.Drawing.Size(44, 29);
            this.dnsButton.TabIndex = 94;
            this.dnsButton.TabStop = false;
            this.dnsButton.Text = "DNS";
            this.dnsButton.Click += new System.EventHandler(this.dnsButton_Click);
            // 
            // PenaltyLabelControl
            // 
            this.PenaltyLabelControl.Location = new System.Drawing.Point(268, 205);
            this.PenaltyLabelControl.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.PenaltyLabelControl.Name = "PenaltyLabelControl";
            this.PenaltyLabelControl.Size = new System.Drawing.Size(41, 16);
            this.PenaltyLabelControl.TabIndex = 103;
            this.PenaltyLabelControl.Text = "Penalty";
            // 
            // timekeepingTimeTextEdit
            // 
            this.timekeepingTimeTextEdit.Enabled = false;
            this.timekeepingTimeTextEdit.Location = new System.Drawing.Point(324, 174);
            this.timekeepingTimeTextEdit.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.timekeepingTimeTextEdit.MenuManager = this.ribbon;
            this.timekeepingTimeTextEdit.Name = "timekeepingTimeTextEdit";
            this.timekeepingTimeTextEdit.Size = new System.Drawing.Size(67, 22);
            this.timekeepingTimeTextEdit.TabIndex = 102;
            this.timekeepingTimeTextEdit.TabStop = false;
            // 
            // timekeepingPenaltyTextEdit
            // 
            this.timekeepingPenaltyTextEdit.Enabled = false;
            this.timekeepingPenaltyTextEdit.Location = new System.Drawing.Point(324, 201);
            this.timekeepingPenaltyTextEdit.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.timekeepingPenaltyTextEdit.MenuManager = this.ribbon;
            this.timekeepingPenaltyTextEdit.Name = "timekeepingPenaltyTextEdit";
            this.timekeepingPenaltyTextEdit.Size = new System.Drawing.Size(67, 22);
            this.timekeepingPenaltyTextEdit.TabIndex = 101;
            this.timekeepingPenaltyTextEdit.TabStop = false;
            // 
            // timeAllowedLabel
            // 
            this.timeAllowedLabel.Location = new System.Drawing.Point(412, 205);
            this.timeAllowedLabel.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.timeAllowedLabel.Name = "timeAllowedLabel";
            this.timeAllowedLabel.Size = new System.Drawing.Size(78, 16);
            this.timeAllowedLabel.TabIndex = 100;
            this.timeAllowedLabel.Text = "Time Allowed";
            // 
            // Label_FZ
            // 
            this.Label_FZ.AutoSize = true;
            this.Label_FZ.Location = new System.Drawing.Point(321, 145);
            this.Label_FZ.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label_FZ.Name = "Label_FZ";
            this.Label_FZ.Size = new System.Drawing.Size(77, 17);
            this.Label_FZ.TabIndex = 97;
            this.Label_FZ.Text = "0.00 / 0.00";
            this.Label_FZ.TextChanged += new System.EventHandler(this.Label_FZ_TextChanged);
            // 
            // ProgressBar
            // 
            this.ProgressBar.Location = new System.Drawing.Point(584, 254);
            this.ProgressBar.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(78, 19);
            this.ProgressBar.TabIndex = 96;
            // 
            // confirmButton
            // 
            this.confirmButton.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.confirmButton.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.confirmButton.Appearance.Options.UseBackColor = true;
            this.confirmButton.Appearance.Options.UseFont = true;
            this.confirmButton.Location = new System.Drawing.Point(324, 48);
            this.confirmButton.Name = "confirmButton";
            this.confirmButton.Size = new System.Drawing.Size(65, 22);
            this.confirmButton.TabIndex = 95;
            this.confirmButton.TabStop = false;
            this.confirmButton.Text = "Confirm";
            this.confirmButton.Click += new System.EventHandler(this.confirmButton_Click);
            // 
            // chooseRound_comboBox
            // 
            this.chooseRound_comboBox.EditValue = "Choose Round";
            this.chooseRound_comboBox.Location = new System.Drawing.Point(104, 8);
            this.chooseRound_comboBox.MenuManager = this.ribbon;
            this.chooseRound_comboBox.Name = "chooseRound_comboBox";
            this.chooseRound_comboBox.Properties.Appearance.BackColor = System.Drawing.Color.RoyalBlue;
            this.chooseRound_comboBox.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.chooseRound_comboBox.Properties.Appearance.ForeColor = System.Drawing.Color.White;
            this.chooseRound_comboBox.Properties.Appearance.Options.UseBackColor = true;
            this.chooseRound_comboBox.Properties.Appearance.Options.UseFont = true;
            this.chooseRound_comboBox.Properties.Appearance.Options.UseForeColor = true;
            this.chooseRound_comboBox.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.chooseRound_comboBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.chooseRound_comboBox.Properties.Items.AddRange(new object[] {
            "Round 1",
            "Round 2",
            "Round 3",
            "Jump Off"});
            this.chooseRound_comboBox.Size = new System.Drawing.Size(163, 28);
            this.chooseRound_comboBox.TabIndex = 88;
            this.chooseRound_comboBox.TabStop = false;
            this.chooseRound_comboBox.SelectedIndexChanged += new System.EventHandler(this.chooseRound_comboBox_SelectedIndexChanged);
            // 
            // roundFinishButton
            // 
            this.roundFinishButton.Appearance.BackColor = System.Drawing.Color.RoyalBlue;
            this.roundFinishButton.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.roundFinishButton.Appearance.ForeColor = System.Drawing.Color.Black;
            this.roundFinishButton.Appearance.Options.UseBackColor = true;
            this.roundFinishButton.Appearance.Options.UseFont = true;
            this.roundFinishButton.Appearance.Options.UseForeColor = true;
            this.roundFinishButton.Appearance.Options.UseTextOptions = true;
            this.roundFinishButton.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.roundFinishButton.Location = new System.Drawing.Point(577, 45);
            this.roundFinishButton.Name = "roundFinishButton";
            this.roundFinishButton.Size = new System.Drawing.Size(116, 29);
            this.roundFinishButton.TabIndex = 82;
            this.roundFinishButton.TabStop = false;
            this.roundFinishButton.Text = "Finish Round";
            this.roundFinishButton.Visible = false;
            // 
            // takeOverButton
            // 
            this.takeOverButton.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.takeOverButton.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.takeOverButton.Appearance.Options.UseBackColor = true;
            this.takeOverButton.Appearance.Options.UseFont = true;
            this.takeOverButton.Location = new System.Drawing.Point(412, 173);
            this.takeOverButton.Name = "takeOverButton";
            this.takeOverButton.Size = new System.Drawing.Size(76, 23);
            this.takeOverButton.TabIndex = 86;
            this.takeOverButton.TabStop = false;
            this.takeOverButton.Text = "Take Over";
            this.takeOverButton.Click += new System.EventHandler(this.takeOverButton_Click);
            // 
            // okButton
            // 
            this.okButton.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.okButton.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.okButton.Appearance.Options.UseBackColor = true;
            this.okButton.Appearance.Options.UseFont = true;
            this.okButton.Location = new System.Drawing.Point(203, 289);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(47, 19);
            this.okButton.TabIndex = 83;
            this.okButton.Text = "OK";
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            this.okButton.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.okButton_PreviewKeyDown);
            // 
            // labelControl11
            // 
            this.labelControl11.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelControl11.Appearance.Options.UseFont = true;
            this.labelControl11.Location = new System.Drawing.Point(216, 145);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(89, 16);
            this.labelControl11.TabIndex = 81;
            this.labelControl11.Text = "Time Keeping:";
            // 
            // ioctextEdit
            // 
            this.ioctextEdit.EditValue = "";
            this.ioctextEdit.Enabled = false;
            this.ioctextEdit.Location = new System.Drawing.Point(412, 111);
            this.ioctextEdit.MenuManager = this.ribbon;
            this.ioctextEdit.Name = "ioctextEdit";
            this.ioctextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.ioctextEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ioctextEdit.Properties.Appearance.Options.UseFont = true;
            this.ioctextEdit.Properties.Appearance.Options.UseForeColor = true;
            this.ioctextEdit.Size = new System.Drawing.Size(116, 22);
            this.ioctextEdit.TabIndex = 80;
            // 
            // labelControl10
            // 
            this.labelControl10.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelControl10.Appearance.Options.UseFont = true;
            this.labelControl10.Location = new System.Drawing.Point(324, 115);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(22, 16);
            this.labelControl10.TabIndex = 79;
            this.labelControl10.Text = "IOC";
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelControl9.Appearance.Options.UseFont = true;
            this.labelControl9.Location = new System.Drawing.Point(106, 291);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(94, 16);
            this.labelControl9.TabIndex = 75;
            this.labelControl9.Text = "Result Correct";
            // 
            // totaltextEdit
            // 
            this.totaltextEdit.Enabled = false;
            this.totaltextEdit.Location = new System.Drawing.Point(192, 259);
            this.totaltextEdit.MenuManager = this.ribbon;
            this.totaltextEdit.Name = "totaltextEdit";
            this.totaltextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.totaltextEdit.Properties.Appearance.Options.UseFont = true;
            this.totaltextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.totaltextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.totaltextEdit.Size = new System.Drawing.Size(198, 22);
            this.totaltextEdit.TabIndex = 74;
            this.totaltextEdit.TabStop = false;
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelControl8.Appearance.Options.UseFont = true;
            this.labelControl8.Location = new System.Drawing.Point(104, 262);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(32, 16);
            this.labelControl8.TabIndex = 73;
            this.labelControl8.Text = "Total";
            // 
            // timePenstextEdit
            // 
            this.timePenstextEdit.Location = new System.Drawing.Point(192, 230);
            this.timePenstextEdit.MenuManager = this.ribbon;
            this.timePenstextEdit.Name = "timePenstextEdit";
            this.timePenstextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.timePenstextEdit.Properties.Appearance.Options.UseFont = true;
            this.timePenstextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.timePenstextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.timePenstextEdit.Size = new System.Drawing.Size(198, 22);
            this.timePenstextEdit.TabIndex = 72;
            this.timePenstextEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.timePenstextEdit_KeyPress);
            this.timePenstextEdit.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.timePenstextEdit_PreviewKeyDown);
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelControl7.Appearance.Options.UseFont = true;
            this.labelControl7.Location = new System.Drawing.Point(104, 233);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(64, 16);
            this.labelControl7.TabIndex = 71;
            this.labelControl7.Text = "Time Pens";
            // 
            // penaltiestextEdit
            // 
            this.penaltiestextEdit.EditValue = "";
            this.penaltiestextEdit.Location = new System.Drawing.Point(192, 201);
            this.penaltiestextEdit.MenuManager = this.ribbon;
            this.penaltiestextEdit.Name = "penaltiestextEdit";
            this.penaltiestextEdit.Properties.AllowFocused = false;
            this.penaltiestextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.penaltiestextEdit.Properties.Appearance.Options.UseFont = true;
            this.penaltiestextEdit.Size = new System.Drawing.Size(67, 22);
            this.penaltiestextEdit.TabIndex = 70;
            this.penaltiestextEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.penaltiestextEdit_KeyPress);
            this.penaltiestextEdit.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.penaltiestextEdit_PreviewKeyDown);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Location = new System.Drawing.Point(104, 178);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(29, 16);
            this.labelControl6.TabIndex = 69;
            this.labelControl6.Text = "Time";
            // 
            // timetextEdit
            // 
            this.timetextEdit.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.timetextEdit.Location = new System.Drawing.Point(192, 174);
            this.timetextEdit.MenuManager = this.ribbon;
            this.timetextEdit.Name = "timetextEdit";
            this.timetextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.timetextEdit.Properties.Appearance.Options.UseFont = true;
            this.timetextEdit.Size = new System.Drawing.Size(68, 22);
            this.timetextEdit.TabIndex = 68;
            this.timetextEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.timetextEdit_KeyPress);
            this.timetextEdit.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.timetextEdit_PreviewKeyDown);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Location = new System.Drawing.Point(104, 205);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(59, 16);
            this.labelControl5.TabIndex = 67;
            this.labelControl5.Text = "Penalties";
            // 
            // teamtextEdit
            // 
            this.teamtextEdit.DataBindings.Add(new System.Windows.Forms.Binding("ReadOnly", this.teamCompetitionBindingSource, "Name", true));
            this.teamtextEdit.Enabled = false;
            this.teamtextEdit.Location = new System.Drawing.Point(412, 79);
            this.teamtextEdit.MenuManager = this.ribbon;
            this.teamtextEdit.Name = "teamtextEdit";
            this.teamtextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.teamtextEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.teamtextEdit.Properties.Appearance.Options.UseFont = true;
            this.teamtextEdit.Properties.Appearance.Options.UseForeColor = true;
            this.teamtextEdit.Size = new System.Drawing.Size(116, 22);
            this.teamtextEdit.TabIndex = 66;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Location = new System.Drawing.Point(324, 83);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(73, 16);
            this.labelControl4.TabIndex = 65;
            this.labelControl4.Text = "Team Name";
            // 
            // riderNametextEdit
            // 
            this.riderNametextEdit.Enabled = false;
            this.riderNametextEdit.Location = new System.Drawing.Point(192, 111);
            this.riderNametextEdit.MenuManager = this.ribbon;
            this.riderNametextEdit.Name = "riderNametextEdit";
            this.riderNametextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.riderNametextEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.riderNametextEdit.Properties.Appearance.Options.UseFont = true;
            this.riderNametextEdit.Properties.Appearance.Options.UseForeColor = true;
            this.riderNametextEdit.Size = new System.Drawing.Size(116, 22);
            this.riderNametextEdit.TabIndex = 64;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Location = new System.Drawing.Point(104, 115);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(73, 16);
            this.labelControl3.TabIndex = 63;
            this.labelControl3.Text = "Rider Name";
            // 
            // horseNametextEdit
            // 
            this.horseNametextEdit.Enabled = false;
            this.horseNametextEdit.Location = new System.Drawing.Point(192, 78);
            this.horseNametextEdit.MenuManager = this.ribbon;
            this.horseNametextEdit.Name = "horseNametextEdit";
            this.horseNametextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.horseNametextEdit.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.horseNametextEdit.Properties.Appearance.Options.UseFont = true;
            this.horseNametextEdit.Properties.Appearance.Options.UseForeColor = true;
            this.horseNametextEdit.Size = new System.Drawing.Size(116, 22);
            this.horseNametextEdit.TabIndex = 62;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(104, 83);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(77, 16);
            this.labelControl2.TabIndex = 61;
            this.labelControl2.Text = "Horse Name";
            // 
            // cnrtextEdit
            // 
            this.cnrtextEdit.Location = new System.Drawing.Point(192, 48);
            this.cnrtextEdit.MenuManager = this.ribbon;
            this.cnrtextEdit.Name = "cnrtextEdit";
            this.cnrtextEdit.Properties.AllowFocused = false;
            this.cnrtextEdit.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.cnrtextEdit.Properties.Appearance.Options.UseFont = true;
            this.cnrtextEdit.Size = new System.Drawing.Size(116, 22);
            this.cnrtextEdit.TabIndex = 60;
            this.cnrtextEdit.EditValueChanged += new System.EventHandler(this.cnrtextEdit_EditValueChanged);
            this.cnrtextEdit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cnrtextEdit_KeyDown);
            this.cnrtextEdit.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.cnrtextEdit_PreviewKeyDown);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Location = new System.Drawing.Point(104, 54);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(26, 16);
            this.labelControl1.TabIndex = 59;
            this.labelControl1.Text = "CNR";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Horizontal = false;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this.gridControl3);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.Size = new System.Drawing.Size(730, 693);
            this.splitContainerControl3.SplitterPosition = 549;
            this.splitContainerControl3.TabIndex = 22;
            // 
            // gridControl3
            // 
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.ranking_gridView;
            this.gridControl3.MenuManager = this.ribbon;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(730, 132);
            this.gridControl3.TabIndex = 57;
            this.gridControl3.TabStop = false;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.ranking_gridView});
            // 
            // ranking_gridView
            // 
            this.ranking_gridView.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.ranking_gridView.Appearance.EvenRow.Options.UseBackColor = true;
            this.ranking_gridView.Appearance.FilterPanel.BackColor = System.Drawing.Color.SeaGreen;
            this.ranking_gridView.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.LimeGreen;
            this.ranking_gridView.Appearance.FilterPanel.Options.UseBackColor = true;
            this.ranking_gridView.Appearance.FocusedCell.BackColor = System.Drawing.Color.Silver;
            this.ranking_gridView.Appearance.FocusedCell.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ranking_gridView.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.ranking_gridView.Appearance.FocusedCell.Options.UseBackColor = true;
            this.ranking_gridView.Appearance.FocusedCell.Options.UseFont = true;
            this.ranking_gridView.Appearance.FocusedCell.Options.UseForeColor = true;
            this.ranking_gridView.Appearance.FocusedRow.BackColor = System.Drawing.Color.Gray;
            this.ranking_gridView.Appearance.FocusedRow.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ranking_gridView.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.ranking_gridView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.ranking_gridView.Appearance.FocusedRow.Options.UseFont = true;
            this.ranking_gridView.Appearance.FocusedRow.Options.UseForeColor = true;
            this.ranking_gridView.Appearance.FooterPanel.BackColor = System.Drawing.Color.SeaGreen;
            this.ranking_gridView.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.LimeGreen;
            this.ranking_gridView.Appearance.FooterPanel.Options.UseBackColor = true;
            this.ranking_gridView.Appearance.GroupPanel.BackColor = System.Drawing.Color.ForestGreen;
            this.ranking_gridView.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.LightGreen;
            this.ranking_gridView.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.ranking_gridView.Appearance.GroupPanel.Options.UseBackColor = true;
            this.ranking_gridView.Appearance.GroupPanel.Options.UseForeColor = true;
            this.ranking_gridView.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Maroon;
            this.ranking_gridView.Appearance.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.ranking_gridView.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.White;
            this.ranking_gridView.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.ranking_gridView.Appearance.HeaderPanel.Options.UseFont = true;
            this.ranking_gridView.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.ranking_gridView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.ranking_gridView.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ranking_gridView.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ranking_gridView.Appearance.Row.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.ranking_gridView.Appearance.Row.Options.UseBackColor = true;
            this.ranking_gridView.Appearance.Row.Options.UseFont = true;
            this.ranking_gridView.Appearance.Row.Options.UseTextOptions = true;
            this.ranking_gridView.Appearance.Row.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ranking_gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colteamName,
            this.colrank,
            this.coltotalPenalties});
            this.ranking_gridView.DetailHeight = 431;
            this.ranking_gridView.FixedLineWidth = 3;
            this.ranking_gridView.GridControl = this.gridControl3;
            this.ranking_gridView.Name = "ranking_gridView";
            this.ranking_gridView.OptionsBehavior.Editable = false;
            this.ranking_gridView.OptionsView.ShowGroupPanel = false;
            this.ranking_gridView.PaintStyleName = "UltraFlat";
            // 
            // colteamName
            // 
            this.colteamName.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colteamName.AppearanceCell.Options.UseFont = true;
            this.colteamName.AppearanceCell.Options.UseTextOptions = true;
            this.colteamName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colteamName.AppearanceHeader.BackColor = System.Drawing.Color.Brown;
            this.colteamName.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colteamName.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.colteamName.AppearanceHeader.Options.UseBackColor = true;
            this.colteamName.AppearanceHeader.Options.UseFont = true;
            this.colteamName.AppearanceHeader.Options.UseForeColor = true;
            this.colteamName.AppearanceHeader.Options.UseTextOptions = true;
            this.colteamName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colteamName.Caption = "Team Name";
            this.colteamName.FieldName = "teamName";
            this.colteamName.MinWidth = 23;
            this.colteamName.Name = "colteamName";
            this.colteamName.Visible = true;
            this.colteamName.VisibleIndex = 0;
            this.colteamName.Width = 87;
            // 
            // colrank
            // 
            this.colrank.AppearanceCell.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colrank.AppearanceCell.Options.UseFont = true;
            this.colrank.AppearanceCell.Options.UseTextOptions = true;
            this.colrank.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colrank.AppearanceHeader.BackColor = System.Drawing.Color.Brown;
            this.colrank.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.colrank.AppearanceHeader.ForeColor = System.Drawing.Color.White;
            this.colrank.AppearanceHeader.Options.UseBackColor = true;
            this.colrank.AppearanceHeader.Options.UseFont = true;
            this.colrank.AppearanceHeader.Options.UseForeColor = true;
            this.colrank.AppearanceHeader.Options.UseTextOptions = true;
            this.colrank.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colrank.Caption = "Rank";
            this.colrank.FieldName = "rank";
            this.colrank.MinWidth = 23;
            this.colrank.Name = "colrank";
            this.colrank.Visible = true;
            this.colrank.VisibleIndex = 1;
            this.colrank.Width = 87;
            // 
            // coltotalPenalties
            // 
            this.coltotalPenalties.Caption = "Total_Penalties";
            this.coltotalPenalties.FieldName = "totalPenalties";
            this.coltotalPenalties.MinWidth = 25;
            this.coltotalPenalties.Name = "coltotalPenalties";
            this.coltotalPenalties.Visible = true;
            this.coltotalPenalties.VisibleIndex = 2;
            this.coltotalPenalties.Width = 94;
            // 
            // printButton
            // 
            this.printButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.printButton.Location = new System.Drawing.Point(287, 832);
            this.printButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.printButton.Name = "printButton";
            this.printButton.Size = new System.Drawing.Size(78, 24);
            this.printButton.TabIndex = 21;
            this.printButton.Text = "Print";
            // 
            // dataNavigator1
            // 
            this.dataNavigator1.Location = new System.Drawing.Point(698, 984);
            this.dataNavigator1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dataNavigator1.Name = "dataNavigator1";
            this.dataNavigator1.Size = new System.Drawing.Size(259, 31);
            this.dataNavigator1.TabIndex = 20;
            this.dataNavigator1.Text = "dataNavigator1";
            // 
            // TeamName
            // 
            this.TeamName.Name = "TeamName";
            // 
            // UDPTimer
            // 
            this.UDPTimer.Enabled = true;
            this.UDPTimer.Tick += new System.EventHandler(this.UDPTimer_Tick);
            // 
            // Timer_ProgressBar
            // 
            this.Timer_ProgressBar.Enabled = true;
            this.Timer_ProgressBar.Tick += new System.EventHandler(this.Timer_ProgressBar_Tick);
            // 
            // report1
            // 
            this.report1.ReportResourceString = resources.GetString("report1.ReportResourceString");
            this.report1.RegisterData(this.teamCompetitionBindingSource1, "teamCompetitionBindingSource1");
            // 
            // teamCompetitionBindingSource1
            // 
            this.teamCompetitionBindingSource1.DataSource = typeof(DataDisplay.TeamCompetition);
            // 
            // round1databaseBindingSource
            // 
            this.round1databaseBindingSource.DataSource = typeof(DataDisplay.Round1_database);
            // 
            // storderReport
            // 
            this.storderReport.ReportResourceString = resources.GetString("storderReport.ReportResourceString");
            this.storderReport.RegisterData(this.round1databaseBindingSource, "round1databaseBindingSource");
            // 
            // startListR1Report
            // 
            this.startListR1Report.ReportResourceString = resources.GetString("startListR1Report.ReportResourceString");
            this.startListR1Report.RegisterData(this.teamCompetitionBindingSource1, "teamCompetitionBindingSource1");
            // 
            // startListR2Report
            // 
            this.startListR2Report.ReportResourceString = resources.GetString("startListR2Report.ReportResourceString");
            this.startListR2Report.RegisterData(this.teamCompetitionBindingSource1, "teamCompetitionBindingSource1");
            // 
            // resultForR1Report
            // 
            this.resultForR1Report.ReportResourceString = resources.GetString("resultForR1Report.ReportResourceString");
            this.resultForR1Report.RegisterData(this.teamCompetitionBindingSource1, "teamCompetitionBindingSource1");
            // 
            // resultForR2Report
            // 
            this.resultForR2Report.ReportResourceString = resources.GetString("resultForR2Report.ReportResourceString");
            this.resultForR2Report.RegisterData(this.teamCompetitionBindingSource1, "teamCompetitionBindingSource1");
            // 
            // finalResultReport
            // 
            this.finalResultReport.ReportResourceString = resources.GetString("finalResultReport.ReportResourceString");
            this.finalResultReport.RegisterData(this.teamCompetitionBindingSource1, "teamCompetitionBindingSource1");
            // 
            // NationCup
            // 
            this.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.ClientSize = new System.Drawing.Size(1509, 894);
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.dataNavigator1);
            this.Controls.Add(this.ribbon);
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "NationCup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NationCup";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.NationCup_FormClosed);
            this.Load += new System.EventHandler(this.NationCup_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.NationCup_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.member_bandedGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamCompetitionBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.team_gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BtnLock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Lock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Unlock)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.round2_gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.round1_gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.round3_gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jumpOff_gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamCompetitionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.timekeepingTimeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timekeepingPenaltyTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chooseRound_comboBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ioctextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.totaltextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timePenstextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.penaltiestextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timetextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamtextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riderNametextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.horseNametextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cnrtextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ranking_gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teamCompetitionBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.round1databaseBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.storderReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startListR1Report)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startListR2Report)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultForR1Report)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultForR2Report)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.finalResultReport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.BarButtonItem display_button;
        private DevExpress.XtraBars.BarButtonItem import_sta_button;
        private DevExpress.XtraBars.BarButtonItem load_json_button;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem convert2json_button;
        private DevExpress.XtraBars.BarButtonItem export_json_button;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView team_gridView;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colTrainer;
        private DevExpress.XtraGrid.Columns.GridColumn colIocCode;
        private DevExpress.XtraGrid.Columns.GridColumn colNo;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView member_bandedGridView;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colCNR;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colHorseName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colRiderName;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colRound1_Penalty;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colRound2_Total;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colRound2_Time;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colJumpOff;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colJumpOff_Time;
        private DevExpress.XtraGrid.Columns.GridColumn TeamName;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraEditors.SimpleButton printButton;
        private System.Windows.Forms.BindingSource teamCompetitionBindingSource;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraNavBar.NavBarControl navBarControl2;
        private DevExpress.XtraNavBar.NavBarGroup eventNumLabel;
        private DevExpress.XtraNavBar.NavBarItem eventNameLabel;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
        private DevExpress.XtraEditors.DataNavigator dataNavigator1;
        private DevExpress.XtraBars.BarButtonItem round1Button;
        private DevExpress.XtraBars.BarButtonItem round2Button;
        private DevExpress.XtraBars.BarButtonItem round3Button;
        private DevExpress.XtraBars.BarButtonItem jumpoffButton;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarCheckItem barCheckItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView round1_gridView;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView ranking_gridView;
        private DevExpress.XtraGrid.Columns.GridColumn colteamName;
        private DevExpress.XtraGrid.Columns.GridColumn colrank;
        private DevExpress.XtraGrid.Views.Grid.GridView round2_gridView;
        private DevExpress.XtraGrid.Views.Grid.GridView round3_gridView;
        private DevExpress.XtraGrid.Views.Grid.GridView jumpOff_gridView;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
        private DevExpress.XtraEditors.ComboBoxEdit chooseRound_comboBox;
        private DevExpress.XtraEditors.SimpleButton roundFinishButton;
        private DevExpress.XtraEditors.SimpleButton takeOverButton;
        private DevExpress.XtraEditors.SimpleButton okButton;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit ioctextEdit;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit totaltextEdit;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.TextEdit timePenstextEdit;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit penaltiestextEdit;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit timetextEdit;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit teamtextEdit;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit riderNametextEdit;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit horseNametextEdit;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit cnrtextEdit;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraBars.BarButtonItem configrationbarButtonItem;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarDockingMenuItem barDockingMenuItem1;
        private DevExpress.XtraBars.BarButtonGroup barButtonGroup1;
        private DevExpress.XtraBars.BarWorkspaceMenuItem barWorkspaceMenuItem1;
        private DevExpress.Utils.WorkspaceManager workspaceManager1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarDockingMenuItem barDockingMenuItem2;
        private DevExpress.XtraEditors.SimpleButton dnsButton;
        private DevExpress.XtraEditors.SimpleButton dqButton;
        private DevExpress.XtraEditors.SimpleButton rtButton;
        private DevExpress.XtraEditors.SimpleButton elButton;
        private DevExpress.XtraEditors.SimpleButton confirmButton;
        private DevExpress.XtraBars.BarButtonItem startlistForShowbarButtonItem;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem3;
        private DevExpress.XtraBars.BarSubItem barSubItem4;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraBars.BarButtonItem round1barButtonItem;
        private DevExpress.XtraBars.BarButtonItem round2barButtonItem;
        private DevExpress.XtraBars.BarButtonItem jumpOffbarButtonItem;
        private DevExpress.XtraBars.BarHeaderItem barHeaderItem1;
        public System.Windows.Forms.Timer UDPTimer;
        public System.Windows.Forms.Timer Timer_ProgressBar;
        public System.Windows.Forms.ProgressBar ProgressBar;
        private System.Windows.Forms.Label Label_FZ;
        private FastReport.Report report1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem12;
        private DevExpress.XtraBars.BarButtonItem startlistForTVbarButtonItem;
        private DevExpress.XtraBars.BarButtonItem startListR1barButtonItem;
        private DevExpress.XtraBars.BarButtonItem startListR2barButtonItem;
        private DevExpress.XtraBars.BarButtonItem resultOfR1barButtonItem;
        private DevExpress.XtraBars.BarButtonItem resultOfR2barButtonItem;
        private DevExpress.XtraBars.BarButtonItem startListOfCurrentRound;
        private DevExpress.XtraBars.BarButtonItem resultOfCurrentFinishedRound;
        private DevExpress.XtraBars.BarButtonItem finalReportBarButtonItem;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraGrid.Columns.GridColumn coltotalPenalties;
        private DevExpress.XtraEditors.LabelControl timeAllowedLabel;
        private DevExpress.XtraEditors.TextEdit timekeepingTimeTextEdit;
        private DevExpress.XtraEditors.TextEdit timekeepingPenaltyTextEdit;
        private DevExpress.XtraEditors.LabelControl PenaltyLabelControl;
        private DevExpress.XtraEditors.LabelControl timeLabelControl;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.BarButtonItem generalSettingbarButtonItem;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.XtraBars.BarButtonItem competitionSettingbarButtonItem;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn colRound1_Time;
        private DevExpress.XtraBars.BarButtonItem startOrderForRound1barButtonItem;
        private System.Windows.Forms.BindingSource round1databaseBindingSource;
        private FastReport.Report storderReport;
        private DevExpress.XtraBars.BarButtonItem startOrderForR2barButtonItem;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit BtnLock;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit Lock;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit Unlock;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit Save;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit Cancel;
        private System.Windows.Forms.BindingSource teamCompetitionBindingSource1;
        private FastReport.Report startListR1Report;
        private System.Windows.Forms.BindingSource teamCompetitionBindingSource2;
        private FastReport.Report startListR2Report;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand Members;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private FastReport.Report resultForR1Report;
        private FastReport.Report resultForR2Report;
        private FastReport.Report finalResultReport;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
    }
}