﻿using System;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Net.Sockets;
using Microsoft.VisualBasic;
using System.Threading;
using System.Data;
using System.Globalization;
using Microsoft.VisualBasic.Devices;
using DevExpress.Utils;
using System.Collections.Generic;

namespace DataDisplay
{
    public partial class NationCup : DevExpress.XtraEditors.XtraForm
    {
        //
        // Restart the software
        //
        public NationCup()
        {
            InitializeComponent();
        }
        private void NationCup_Load(object sender, EventArgs e)
        {
            try
            {
                if (File.Exists(Constants.internalJsonPath))
                {                  
                    JObject data = JObject.Parse(File.ReadAllText(Constants.internalJsonPath));
                    var c = (string)data["GeneralSetting"]["udpPort"];
                    obj_internal.GeneralSetting.udpPort = (string)data["GeneralSetting"]["udpPort"];
                    obj_internal.GeneralSetting.showFolderPath = (string)data["GeneralSetting"]["showFolderPath"];
                    obj_internal.GeneralSetting.tvFolderPath = (string)data["GeneralSetting"]["tvFolderPath"];
                    obj_internal.GeneralSetting.staFolderPath = (string)data["GeneralSetting"]["staFolderPath"];
                    obj_internal.GeneralSetting.htcFolderPath = (string)data["GeneralSetting"]["htcFolderPath"];
                    obj_internal.GeneralSetting.htcFilePath = (string)data["GeneralSetting"]["htcFilePath"];
                    obj_internal.GeneralSetting.cBox = (string)data["GeneralSetting"]["cBox"];
                         
                    try
                    {
                        if (obj_internal.GeneralSetting.htcFilePath != null)
                        {
                            Constants.sourceFilePath = obj_internal.GeneralSetting.htcFilePath;
                            try
                            {
                                if (File.Exists(obj_internal.GeneralSetting.htcFilePath))
                                {
                                    //deserailization
                                    var json = File.ReadAllText(obj_internal.GeneralSetting.htcFilePath);
                                    obj = JsonConvert.DeserializeObject<Serialization>(json);

                                    startlistForShowbarButtonItem.Enabled = true;
                                    startlistForTVbarButtonItem.Enabled = true;

                                    displaySoftwareData();
                                    try
                                    {
                                        if (obj_internal.GeneralSetting.cBox != null)
                                        {                                            
                                            displayRoundData(Convert.ToInt32(obj_internal.GeneralSetting.cBox));
                                            chooseRound_comboBox.SelectedIndex = Convert.ToInt32(obj_internal.GeneralSetting.cBox);
                                            startListGridviewFoucs();
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show("Problem with combobox value, Please check cBox value in general settings (internal JSON)!\n" + ex.Message);
                                    }

                                }
                                else
                                {
                                    chooseRound_comboBox.Enabled = false;
                                    if (MessageBox.Show("Do you want to create the HTC file?", "The HTC file doesn't exist!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                                    {
                                        File.Delete(Constants.internalJsonPath);
                                    }
                                    else
                                    {
                                        MessageBox.Show("Please check the HTC file at " + obj_internal.GeneralSetting.htcFilePath);
                                    }

                                }
                            }
                            catch(Exception ex)
                            {
                                MessageBox.Show("Problem while loading HTC file, Please check HTC file at "
                                    + obj_internal.GeneralSetting.htcFilePath + "\n" + ex.Message);
                            }
                        }
                        else
                        {
                            chooseRound_comboBox.Enabled = false;
                            startlistForShowbarButtonItem.Enabled = false;
                            startlistForTVbarButtonItem.Enabled = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Problem while loading HTC file, please check file path in GS (internal JSON)!\n" + ex.Message);
                    }

                }
                else
                {
                    chooseRound_comboBox.Enabled = false;
                    startlistForShowbarButtonItem.Enabled = false;
                    startlistForTVbarButtonItem.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex.Message);
            }
            
            cnrtextEdit.Select();
            convert2json_button.Enabled = false;
            export_json_button.Enabled = false;
            if (okButton.CanFocus == true)
            {
                okButton.Focus();
            }
            //generalSettingbarButtonItem_ItemClick(sender, e as DevExpress.XtraBars.ItemClickEventArgs);
        }
        // 
        // Ribbon Buttons
        //
        private void import_sta_button_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (obj_internal.GeneralSetting.staFolderPath != null)
                {
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.Multiselect = false;
                    openFileDialog.Filter = "txt files (*.sta)|*.sta|All files (*.*)|*.*";
                    //openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                    openFileDialog.InitialDirectory = obj_internal.GeneralSetting.staFolderPath;                   
                    if (openFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        Constants.sourceFilePath = openFileDialog.FileName;
                        convert2json_button.Enabled = true;
                    }
                }
                else
                {
                    MessageBox.Show("Please provide STA folder path in general settings!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("File can not be opened.\n" + ex.Message);
            }

        }      
        private void convert2json_button_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (File.Exists(Constants.sourceFilePath))
                {
                    ReadCsv readCsv = new ReadCsv();
                    readCsv.ReadingDataFromCsv(Constants.sourceFilePath);
                }
                else
                {
                    MessageBox.Show("STA file does not existed!\n");
                }

                Serialization serialization = new Serialization();
                serialization.JsonCreation();

                var tmpJSON = Newtonsoft.Json.JsonConvert.SerializeObject(serialization, Formatting.Indented);

                var json = JToken.Parse(tmpJSON);
                var fieldsCollector = new Duplicate_JSON(json);
                var fields = fieldsCollector.GetAllFields();

                foreach (var field in fields)
                    Console.WriteLine($"{field.Key}: '{field.Value}'");

                string targetPath = System.IO.Path.Combine(Constants.Event_Json, System.IO.Path.GetFileNameWithoutExtension(Constants.sourceFilePath) + ".htc");
                
                if (!File.Exists(targetPath))
                {
                    File.WriteAllText(targetPath, JToken.FromObject(tmpJSON).ToString());
                    MessageBox.Show("Successfully converted and saved");
                }
                else
                {
                    if (MessageBox.Show("Do you want to delete the file and create again?", "File is already existed!", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        File.Delete(targetPath);
                        File.WriteAllText(targetPath, JToken.FromObject(tmpJSON).ToString());
                        MessageBox.Show("Successfully converted and saved");
                    }

                    //string targetPathCopy = System.IO.Path.Combine(Constants.Event_Json, System.IO.Path.GetFileNameWithoutExtension(Constants.sourceFilePath) + "_copy.htc");
                    //File.WriteAllText(targetPathCopy, JToken.FromObject(tmpJSON).ToString());

                    //Duplicate_JSON duplicate_JSON = new Duplicate_JSON();

                }

                convert2json_button.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to convert STA to HTC!\n" + ex.Message);
            }
        }
        public static Serialization obj;
        private void load_json_button_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (obj_internal.GeneralSetting.htcFolderPath != null)
                {
                    OpenFileDialog openFileDialog = new OpenFileDialog();
                    openFileDialog.Multiselect = false;
                    openFileDialog.Filter = "htc files (*.htc)|*.htc|All files (*.*)|*.*";

                    openFileDialog.InitialDirectory = obj_internal.GeneralSetting.htcFolderPath;

                    if (openFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        string tmp = openFileDialog.FileName;
                        if (tmp != obj_internal.GeneralSetting.htcFilePath)
                        {
                            dt_iterator = 0;
                            cnrtextEdit.Text = null;
                            timeAllowedLabel.Text = "Time Allowed : ";
                        }
                        Constants.sourceFilePath = openFileDialog.FileName;
                        obj_internal.GeneralSetting.htcFilePath = Constants.sourceFilePath;

                        obj_internal.GeneralSetting.cBox = null;
                        //Internal Json updates
                        updateJson.modifyJsonForGeneralSettings(obj_internal);

                        //deserailization
                        var json = File.ReadAllText(Constants.sourceFilePath);
                        obj = JsonConvert.DeserializeObject<Serialization>(json);

                        chooseRound_comboBox.Enabled = true;
                        startlistForShowbarButtonItem.Enabled = true;
                        startlistForTVbarButtonItem.Enabled = true;

                        chooseRound_comboBox.Text = "Choose Round";

                        displaySoftwareData();
                        //gridControl2.DataSource = null;
                        //gridControl2.MainView = round1_gridView;
                        //gridControl2.DataSource = obj.DataTableR1;
                        if (obj.Event.timeAllowedForRounds <= 0)
                        {
                            MessageBox.Show("Please provide competition settings!\n");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Please provide HTC folder path in General settings!\n");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to load HTC file.\n" + ex.Message);
            }
        }
        // Display main grid view data when load an htc file or restart the software
        public void displaySoftwareData()
        {
            try
            {
                eventNumLabel.Caption = Convert.ToString(obj.Competition.competitionNumber) + "\t" + obj.Competition.competitionName;
                eventNameLabel.Caption = obj.Event.name + "\t\t\t\t\t\t\t\t" + "Number of Starter: " + Convert.ToString(obj.Competitors.Count());
                try
                {
                    int port = Convert.ToInt32(obj_internal.GeneralSetting.udpPort);
                    NationCup.receivingUDPclient = new UdpClient(port);
                }
                catch
                {
                    MessageBox.Show("The port is already connected!\n");
                }

                export_json_button.Enabled = true;

                member_bandedGridView.Bands[3].Visible = false;
                member_bandedGridView.OptionsSelection.MultiSelect = false;
                team_gridView.RefreshData();
                gridControl1.Refresh();
                gridControl1.DataSource = null;
                gridControl1.DataSource = obj.Teams;

                gridControl2.Refresh();
                gridControl2.DataSource = null;

                gridControl3.Refresh();
                gridControl3.DataSource = null;
                gridControl3.DataSource = obj.Rank;

                ExpandAll(team_gridView);
                expand = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with displaySoftwareData method!\n" + ex.Message);
            }
            
        }
        private void export_json_button_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (chooseRound_comboBox.SelectedIndex == 0)
                {
                    var tmpJSON = Newtonsoft.Json.JsonConvert.SerializeObject(obj, Formatting.Indented);
                    if (File.Exists(Constants.sourceFilePath))
                    {
                        File.Delete(Constants.sourceFilePath);
                        File.Create(Constants.sourceFilePath).Dispose();
                    }
                    File.WriteAllText(Constants.sourceFilePath, JToken.FromObject(tmpJSON).ToString());
                    MessageBox.Show("Succesfully exported.");
                }
                else if (chooseRound_comboBox.SelectedIndex == 1)
                {
                    var tmpJSON = Newtonsoft.Json.JsonConvert.SerializeObject(obj_round2, Formatting.Indented);
                    if (File.Exists(Constants.sourceFilePath))
                    {
                        File.Delete(Constants.sourceFilePath);
                        File.Create(Constants.sourceFilePath).Dispose();
                    }
                    File.WriteAllText(Constants.sourceFilePath, JToken.FromObject(tmpJSON).ToString());
                    MessageBox.Show("Succesfully exported.");
                }
                else if (chooseRound_comboBox.SelectedIndex == 2)
                {
                    var tmpJSON = Newtonsoft.Json.JsonConvert.SerializeObject(obj, Formatting.Indented);
                    if (File.Exists(Constants.sourceFilePath))
                    {
                        File.Delete(Constants.sourceFilePath);
                        File.Create(Constants.sourceFilePath).Dispose();
                    }
                    File.WriteAllText(Constants.sourceFilePath, JToken.FromObject(tmpJSON).ToString());
                    MessageBox.Show("Succesfully exported.");
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with exporting HTC file.\n" + ex.Message);
            }           
        }
        //
        // Setting form
        //
        private void competitionSettingbarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (obj != null)
                {
                    CompetitionSetting competitionSettings = new CompetitionSetting();
                    competitionSettings.Show();
                }
                else
                {
                    MessageBox.Show("Please provide HTC file before writing competition settings!");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Problem with competition settings!\n" + ex.Message);
            }

        }
        public static SerializationForInternal obj_internal = new SerializationForInternal();
        private void generalSettingbarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (File.Exists(Constants.internalJsonPath))
                {
                    JObject data = JObject.Parse(File.ReadAllText(Constants.internalJsonPath));
                    var c = (string)data["GeneralSetting"]["udpPort"];
                    obj_internal.GeneralSetting.udpPort = (string)data["GeneralSetting"]["udpPort"];
                    obj_internal.GeneralSetting.showFolderPath = (string)data["GeneralSetting"]["showFolderPath"];
                    obj_internal.GeneralSetting.tvFolderPath = (string)data["GeneralSetting"]["tvFolderPath"];
                    obj_internal.GeneralSetting.staFolderPath = (string)data["GeneralSetting"]["staFolderPath"];
                    obj_internal.GeneralSetting.htcFolderPath = (string)data["GeneralSetting"]["htcFolderPath"];
                    obj_internal.GeneralSetting.htcFilePath = (string)data["GeneralSetting"]["htcFilePath"];
                    obj_internal.GeneralSetting.cBox = (string)data["GeneralSetting"]["cBox"];
                }
                else
                {
                    obj_internal.CreateJson();
                    var tmpJSON = Newtonsoft.Json.JsonConvert.SerializeObject(obj_internal, Formatting.Indented);
                    File.WriteAllText(Constants.internalJsonPath, JToken.FromObject(tmpJSON).ToString());
                }

                GeneralSettings generalSettings = new GeneralSettings();
                generalSettings.Show();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Problem with writing general settings data!\n" + ex.Message);
            }
            
        }
        // Perform key events (down, up, add) throughout nationcup
        bool flag = false;
        private void NationCup_KeyDown(object sender, KeyEventArgs e)
        {     
            if (e.KeyCode == Keys.Add)
            {
                flag = true;
                okButton_Click(sender, e);
            }
        }
        // 
        // Expand result gridview and change result in result gridview
        //
        bool expand = false;
        void ExpandAll(GridView view)
        {
            GridViewInfo info = view.GetViewInfo() as GridViewInfo;
            foreach (GridRowInfo ri in info.RowsInfo)
                view.ExpandMasterRow(ri.RowHandle, 0);
        }
        // change the result through double click on cell (Member_bandedGridview_MouseDown & member_bandedGridView_ShowingEditor events)
        int ClickCount = -1;
        private void member_bandedGridView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                ClickCount = e.Clicks;
        }
        private void member_bandedGridView_ShowingEditor(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = (ClickCount == 1);
            if (!e.Cancel)
                ClickCount = -1;
        }
        // 
        // Round 1
        //
        Round1 round1 = new Round1();
        Round1_database round1_Database = new Round1_database();
        public void Round1_details()
        {
            try
            {
                round1.create_storder(obj, obj.DataTableR1);
                gridControl2.DataSource = null;
                gridControl2.MainView = round1_gridView;
                gridControl2.DataSource = obj.DataTableR1;
                gridControl1.Refresh();
                gridControl1.DataSource = null;
                gridControl1.DataSource = obj.Teams;
                ExpandAll(team_gridView);
                expand = true;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Problem with Round1_details method!\n" + ex.Message);
            }            
        }
        private void round1_gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                var c = round1_gridView.GetFocusedRowCellValue(colCNR);
                cnrtextEdit.Text = Convert.ToString(c);
                ActiveControl = cnrtextEdit;
                cnrtextEdit.Focus();
                cnrtextEdit.SelectAll();
                round1_gridView.ClearSelection();
            }
        }
        //changed the round1_penalty value and store in object with confirmation messagebox(round1_ergebnis_cellValueChanged events)
        //private void round1_ergebnis_cellValueChanged(object sender, CellValueChangedEventArgs e)
        //{
        //    if (MessageBox.Show("Do you want to confirm the value?", "Penalty is changed!", MessageBoxButtons.YesNo) == DialogResult.Yes)
        //    {
        //        DevExpress.XtraGrid.Views.Grid.GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
        //        if (view == null)
        //            return;

        //        int no = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, view.Columns["Team No"]).ToString());
        //        string cnr = view.GetRowCellValue(e.RowHandle, view.Columns["CNR"]).ToString();

        //        for (int i = 0; i < obj.Teams.Count; i++)
        //        {
        //            if (obj.Teams[i].No == no)
        //            {
        //                ParticipantStatusR1 participantStatus = new ParticipantStatusR1();
        //                for (int j = 0; j < obj.Teams[i].MemberDetails.Count; j++)
        //                {
        //                    if (obj.Teams[i].MemberDetails[j].CNR == cnr)
        //                    {
        //                        obj.Teams[i].MemberDetails[j].Round1_Total = e.Value.ToString();
        //                        participantStatus.status_update(obj.Teams[i].MemberDetails, cnr, obj.Teams[i].MemberDetails[j].Round1_Total, obj.Teams[i].MemberDetails[j].Round1_Time);
        //                    }
        //                }
        //            }
        //        }

        //        round1.create_ranking(obj);
        //        round1.create_ergebnis(obj, no, cnr);

        //        gridControl3.Refresh();
        //        gridControl3.DataSource = null;
        //        gridControl3.DataSource = obj.Rank;

        //        gridControl1.Refresh();
        //        gridControl1.DataSource = null;
        //        gridControl1.DataSource = obj.Teams;

        //        ExpandAll(team_gridView);
        //        expand = true;
        //    }
        //}
        //change the round1_penalty value through double click on cell(round1_gridView_ShowingEditor events & round1_gridView_MouseDown)
        //private void round1_gridView_ShowingEditor(object sender, System.ComponentModel.CancelEventArgs e)
        //{
        //    e.Cancel = (ClickCount == 1);
        //    if (!e.Cancel)
        //        ClickCount = -1;
        //}
        //private void round1_gridView_MouseDown(object sender, MouseEventArgs e)
        //{
        //    if (round1_gridView.FocusedColumn.FieldName == "Round1_Penalty" && e.Button == MouseButtons.Left)
        //    {
        //        round1_gridView.OptionsBehavior.Editable = true;
        //        ClickCount = e.Clicks;
        //        MessageBox.Show("Are you sure you want to edit the value?", "Warning:", MessageBoxButtons.YesNo);
        //    }
        //}
        //CNR value appear in live window(CNR text box) by double clicking on CNR
        //private void round1_gridView_DoubleClick(object sender, EventArgs e)
        //{
        //    if (round1_gridView.FocusedColumn.FieldName == "CNR")
        //    {
        //        //confirmcheckEdit.Checked = false;
        //        var cnr = round1_gridView.GetFocusedValue();
        //        cnrtextEdit.Text = Convert.ToString(cnr);
        //        riderNametextEdit.Text = null;
        //        horseNametextEdit.Text = null;
        //        teamtextEdit.Text = null;
        //        ioctextEdit.Text = null;
        //    }

        //}
        // 
        // Round 2
        //
        Serialization obj_round2;
        Round2 round2 = new Round2();
        ChangeRankingOrder rankingOrder = new ChangeRankingOrder();
        public void Round2_details()
        {
            try
            {
                //obj_round2 = obj.Clone() as Serialization;
                obj_round2 = rankingOrder.AscendingRanking(obj_round2);
                round2.create_datatable(obj_round2);
                round2.create_storder(obj_round2);
                round2.getStartListR2(obj_round2);
                round1_Database.GetInfoR2(round2.dataTable, obj_round2);
                gridControl2.DataSource = null;
                gridControl2.MainView = round2_gridView;
                gridControl2.DataSource = round2.dataTable;

                gridControl1.Refresh();
                gridControl1.DataSource = null;
                gridControl1.DataSource = obj_round2.Teams;
                ExpandAll(team_gridView);
                expand = true;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Problem with Round2_details method!\n" + ex.Message);
            }
           
        }
        public void Round2_details_without_ascending()
        {
            try
            {
                round2.create_datatable(obj_round2);
                round2.create_storder(obj_round2);
                round2.getStartListR2(obj_round2);
                round1_Database.GetInfoR2(round2.dataTable, obj_round2);
                gridControl2.DataSource = null;
                gridControl2.MainView = round2_gridView;
                gridControl2.DataSource = round2.dataTable;

                gridControl1.Refresh();
                gridControl1.DataSource = null;
                gridControl1.DataSource = obj_round2.Teams;
                ExpandAll(team_gridView);
                expand = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with Round2_details method!\n" + ex.Message);
            }

        }
        private void round2_gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                var c = round2_gridView.GetFocusedRowCellValue(colCNR);
                cnrtextEdit.Text = Convert.ToString(c);
                ActiveControl = cnrtextEdit;
                cnrtextEdit.Focus();
                cnrtextEdit.Select();
                round2_gridView.ClearSelection();
            }
        }
        // CNR value appear in live window (CNR text box) by double clicking on CNR 
        //private void round2_gridView_DoubleClick(object sender, EventArgs e)
        //{
        //    if (round2_gridView.FocusedColumn.FieldName == "CNR")
        //    {
        //        //confirmcheckEdit.Checked = false;
        //        var cnr = round2_gridView.GetFocusedValue();
        //        cnrtextEdit.Text = Convert.ToString(cnr);
        //        riderNametextEdit.Text = null;
        //        horseNametextEdit.Text = null;
        //        teamtextEdit.Text = null;
        //        ioctextEdit.Text = null;
        //    }
        //}
        //// change the round2_penalty value through double click on cell (round1_gridView_ShowingEditor events & round1_gridView_MouseDown)
        //private void round2_gridView_ShowingEditor(object sender, System.ComponentModel.CancelEventArgs e)
        //{
        //    e.Cancel = (ClickCount == 1);
        //    if (!e.Cancel)
        //        ClickCount = -1;
        //}
        //private void round2_gridView_MouseDown(object sender, MouseEventArgs e)
        //{
        //    if (round2_gridView.FocusedColumn.FieldName == "Round2_Penalty" && e.Button == MouseButtons.Left)
        //    {
        //        //round1_gridView.OptionsBehavior.Editable = true;
        //        ClickCount = e.Clicks;
        //        //MessageBox.Show("Are you sure you want to edit the value?", "Warning:", MessageBoxButtons.YesNo);
        //    }
        //}
        //private void round2_ergebnis_CellValueChanged(object sender, CellValueChangedEventArgs e)
        //{
        //    if (MessageBox.Show("Do you want to confirm the value?", "Penalty is changed!", MessageBoxButtons.YesNo) == DialogResult.Yes)
        //    {
        //        DevExpress.XtraGrid.Views.Grid.GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
        //        if (view == null)
        //            return;

        //        int no = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, view.Columns["Team No"]).ToString());
        //        string cnr = view.GetRowCellValue(e.RowHandle, view.Columns["CNR"]).ToString();



        //        for (int i = 0; i < obj.Teams.Count; i++)
        //        {
        //            if (obj.Teams[i].No == no)
        //            {
        //                ParticipantStatusR1 participantStatus = new ParticipantStatusR1();
        //                for (int j = 0; j < obj.Teams[i].MemberDetails.Count; j++)
        //                {
        //                    if (obj.Teams[i].MemberDetails[j].CNR == cnr)
        //                    {                              
        //                        obj.Teams[i].MemberDetails[j].Round2_Total = e.Value.ToString();
        //                        participantStatus.status_update(obj.Teams[i].MemberDetails, cnr, obj.Teams[i].MemberDetails[j].Round2_Total, obj.Teams[i].MemberDetails[j].Round2_Time);
        //                    }
        //                }
        //            }
        //        }

        //        round2.create_ranking(obj);

        //        //round2.create_ergebnis(obj, no, cnr);


        //        gridControl3.Refresh();
        //        gridControl3.DataSource = null;
        //        gridControl3.DataSource = obj.Rank;

        //        gridControl1.Refresh();
        //        gridControl1.DataSource = null;
        //        gridControl1.DataSource = obj.Teams;

        //        ExpandAll(team_gridView);
        //        expand = true;

        //    }
        //    //DevExpress.XtraGrid.Views.Grid.GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
        //    //if (view == null)
        //    //    return;

        //    //int no = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, view.Columns["Team No"]).ToString());
        //    //string cnr = view.GetRowCellValue(e.RowHandle, view.Columns["CNR"]).ToString();

        //    //round2.create_ergebnis(obj_round2, no, cnr, e);

        //    //round2.create_ranking(obj_round2);

        //    //gridControl3.Refresh();
        //    //gridControl3.DataSource = null;
        //    //gridControl3.DataSource = obj_round2.Rank;
        //}
        // 
        // Jump Off
        //
        Serialization obj_jumpoff;
        JumpOff jumpOff = new JumpOff();
        Rank_Calculate rank_Calculate = new Rank_Calculate();
        public void JumpOff_details()
        {
            obj_jumpoff = obj.Clone() as Serialization;
            jumpOff.change_obj(obj_jumpoff);
            jumpOff.create_storder(obj_jumpoff);
            jumpOff.create_datatable();
            gridControl2.DataSource = null;
            gridControl2.MainView = jumpOff_gridView;
            gridControl2.DataSource = jumpOff.dataTable;
        }
        private void jumpoff_ergebnis_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView view = sender as DevExpress.XtraGrid.Views.Grid.GridView;
            if (view == null)
                return;

            int no = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, view.Columns["Team No"]).ToString());
            string cnr = view.GetRowCellValue(e.RowHandle, view.Columns["CNR"]).ToString();

            jumpOff.create_ergebnis(obj_jumpoff, no, cnr);

            jumpOff.create_ranking(obj_jumpoff);

            gridControl3.Refresh();
            gridControl3.DataSource = null;
            gridControl3.DataSource = obj_jumpoff.Rank;
        }
        // 
        // Common Methods
        //
        private static int dt_iterator = 0;
        public void setCnrIteratorR1(List<DataTableR1> dataTableR1)
        {
            try
            {
                if (dt_iterator < dataTableR1.Count && string.IsNullOrEmpty(dataTableR1[dt_iterator].Round1_penalty))
                {
                    string cnr = dataTableR1[dt_iterator].CNR;
                    cnrtextEdit.Text = cnr;
                }
                else if (dt_iterator < dataTableR1.Count)
                {
                    while (dt_iterator < dataTableR1.Count && !string.IsNullOrEmpty(dataTableR1[dt_iterator].Round1_penalty))
                    {
                        dt_iterator += 1;
                    }

                    if (dt_iterator < dataTableR1.Count)
                    {
                        string cnr = dataTableR1[dt_iterator].CNR;
                        cnrtextEdit.Text = cnr;
                    }
                }               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with setCnrIterator method" + ex.Message);
            }
        }
        public void setCnrIteratorR2(DataTable datatable)
        {
            try
            {
                if (dt_iterator < datatable.Rows.Count && string.IsNullOrEmpty(datatable.Rows[dt_iterator].ItemArray[5].ToString()))
                {
                    string cnr = datatable.Rows[dt_iterator].ItemArray[2].ToString();
                    cnrtextEdit.Text = cnr;
                }
                else if (dt_iterator < datatable.Rows.Count)
                {
                    while (dt_iterator < datatable.Rows.Count && !string.IsNullOrEmpty(datatable.Rows[dt_iterator].ItemArray[5].ToString()))
                    {
                        dt_iterator += 1;
                    }
                    if (dt_iterator < datatable.Rows.Count)
                    {
                        string cnr = datatable.Rows[dt_iterator].ItemArray[2].ToString();
                        cnrtextEdit.Text = cnr;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with setCnrIterator method" + ex.Message);
            }
        }
        // Compute penalty
        public void penaltycalculate()
        {
            try
            {
                PenaltyCompute penaltyCompute = new PenaltyCompute();
                if (timetextEdit.Text != "")
                {
                    // compute panelties
                    if (timetextEdit.Text.Contains('.'))
                        timetextEdit.Text = timetextEdit.Text.Replace('.', ',');
                    int pen = penaltyCompute.computePenalties(double.Parse(timetextEdit.Text));

                    timePenstextEdit.Text = Convert.ToString(pen);
                    if (penaltiestextEdit.Text != "" && penaltiestextEdit.Text == "+")
                    {
                        penaltiestextEdit.Text.Trim('+');
                        penaltiestextEdit.Text = string.Empty;
                        totaltextEdit.Text = timePenstextEdit.Text;
                        WriteDat writeDat = new WriteDat();
                        writeDat.CreateDat2(obj, cnrtextEdit.Text, totaltextEdit.Text, timetextEdit.Text, chooseRound_comboBox.Text);
                    }
                    else if (penaltiestextEdit.Text != "" && penaltiestextEdit.Text != "+")
                    {
                        totaltextEdit.Text = Convert.ToString(pen + Convert.ToInt32(penaltiestextEdit.Text));
                        WriteDat writeDat = new WriteDat();
                        writeDat.CreateDat2(obj, cnrtextEdit.Text, totaltextEdit.Text, timetextEdit.Text, chooseRound_comboBox.Text);
                    }
                    else
                    {
                        totaltextEdit.Text = timePenstextEdit.Text;
                        WriteDat writeDat = new WriteDat();
                        writeDat.CreateDat2(obj, cnrtextEdit.Text, totaltextEdit.Text, timetextEdit.Text, chooseRound_comboBox.Text);
                    }
                }
                else
                {
                    MessageBox.Show("Please provide the time!\n");
                }
               
                
            }
            catch(Exception ex)
            {
                MessageBox.Show("Problem with penaltycalculate method!\n" + ex.Message);
            }
            
        }
        public void saveResultOfLiveWindowForR1()
        {
            try
            {
                Paranthesis paranthesis = new Paranthesis();
                SaveDataObj saveDataObj = new SaveDataObj();
                ParticipantStatusR1 participantStatus = new ParticipantStatusR1();

                for (int i = 0; i < obj.Teams.Count; i++)
                {
                    for (int j = 0; j < obj.Teams[i].MemberDetails.Count; j++)
                    {
                        if (obj.Teams[i].MemberDetails[j].CNR == cnrtextEdit.Text)
                        {
                            if (totaltextEdit.Text != "")
                            {
                                if (penaltiestextEdit.Text == "" && timekeepingTimeTextEdit.BackColor == Color.Green)
                                {
                                    participantStatus.status_update(obj.Teams[i].MemberDetails, cnrtextEdit.Text, totaltextEdit.Text, timekeepingTimeTextEdit.Text);
                                    obj.Teams[i].MemberDetails[j].Round1_Time = timekeepingTimeTextEdit.Text;
                                    obj.Teams[i].MemberDetails[j].Round1_Penalty = timekeepingPenaltyTextEdit.Text;
                                    //saveDataObj.savePenalty(obj, chooseRound_comboBox.SelectedIndex, cnrtextEdit.Text, timekeepingPenaltyTextEdit.Text);
                                }
                                else if (penaltiestextEdit.Text != null || timetextEdit.Text != null)
                                {
                                    participantStatus.status_update(obj.Teams[i].MemberDetails, cnrtextEdit.Text, totaltextEdit.Text, timetextEdit.Text);
                                    obj.Teams[i].MemberDetails[j].Round1_Time = timetextEdit.Text;
                                    obj.Teams[i].MemberDetails[j].Round1_Penalty = penaltiestextEdit.Text;
                                    //saveDataObj.savePenalty(obj, chooseRound_comboBox.SelectedIndex, cnrtextEdit.Text, penaltiestextEdit.Text);
                                }

                                obj.Teams[i].MemberDetails[j].Round1_Time_Penalty = timePenstextEdit.Text;
                                obj.Teams[i].MemberDetails[j].Round1_Total = totaltextEdit.Text;

                                paranthesis.update_paranthesisR1(obj.Teams[i].MemberDetails);

                                for (int k = 0; k < obj.DataTableR1.Count; k++)
                                {
                                    for (int m = 0; m < obj.Teams.Count; m++)
                                    {
                                        for (int n = 0; n < obj.Teams[m].MemberDetails.Count; n++)
                                        {
                                            if (obj.Teams[m].MemberDetails[n].CNR == obj.DataTableR1[k].CNR)
                                            {
                                                obj.DataTableR1[k].Round1_penalty = obj.Teams[m].MemberDetails[n].Round1_Total;
                                                obj.DataTableR1[k].Round1_time = obj.Teams[m].MemberDetails[n].Round1_Time;
                                            }
                                        }

                                    }

                                }

                                round1.create_ranking(obj);
                                round1.create_ergebnis(obj, obj.Teams[i].No, cnrtextEdit.Text);

                                gridControl3.Refresh();
                                gridControl3.DataSource = null;
                                gridControl3.DataSource = obj.Rank;

                                gridControl2.DataSource = null;
                                gridControl2.MainView = round1_gridView;
                                gridControl2.DataSource = obj.DataTableR1;

                                gridControl1.Refresh();
                                gridControl1.DataSource = null;
                                gridControl1.DataSource = obj.Teams;

                                ExpandAll(team_gridView);
                                expand = true;
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Problem with saveResultOfLiveWindowForR1 method!\n" + ex.Message);
            }
            
            cnrtextEdit.Text = null;
            //confirmcheckEdit.Checked = false;
            horseNametextEdit.Text = null;
            riderNametextEdit.Text = null;
            ioctextEdit.Text = null;
            timekeepingPenaltyTextEdit.Text = null;
            timekeepingTimeTextEdit.Text = null;
            timetextEdit.Text = null;
            penaltiestextEdit.Text = null;
            timePenstextEdit.Text = null;
            totaltextEdit.Text = null;
            teamtextEdit.Text = null;
        }
        public void saveResultOfLiveWindowForR2()
        {
            try
            {
                SaveDataObj saveDataObj = new SaveDataObj();
                Paranthesis paranthesis = new Paranthesis();
                ParticipantStatusR2 participantStatusR2 = new ParticipantStatusR2();

                for (int i = 0; i < obj_round2.Teams.Count; i++)
                {
                    for (int j = 0; j < obj_round2.Teams[i].MemberDetails.Count; j++)
                    {
                        if (obj_round2.Teams[i].MemberDetails[j].CNR == cnrtextEdit.Text)
                        {
                            if (totaltextEdit.Text != "")
                            {
                                if (penaltiestextEdit.Text == "" && timekeepingTimeTextEdit.BackColor == Color.Green)
                                {
                                    participantStatusR2.status_update(obj_round2.Teams[i].MemberDetails, cnrtextEdit.Text, totaltextEdit.Text, timekeepingTimeTextEdit.Text);
                                    obj_round2.Teams[i].MemberDetails[j].Round2_Time = timekeepingTimeTextEdit.Text;
                                    obj_round2.Teams[i].MemberDetails[j].Round2_Penalty = timekeepingPenaltyTextEdit.Text;
                                    //saveDataObj.saveTime(obj, chooseRound_comboBox.SelectedIndex, cnrtextEdit.Text, timekeepingTimeTextEdit.Text);
                                    //saveDataObj.savePenalty(obj, chooseRound_comboBox.SelectedIndex, cnrtextEdit.Text, timekeepingPenaltyTextEdit.Text);
                                }
                                else if (penaltiestextEdit.Text != null || timetextEdit.Text != null)
                                {
                                    participantStatusR2.status_update(obj_round2.Teams[i].MemberDetails, cnrtextEdit.Text, totaltextEdit.Text, timetextEdit.Text);
                                    obj_round2.Teams[i].MemberDetails[j].Round2_Time = timetextEdit.Text;
                                    obj_round2.Teams[i].MemberDetails[j].Round2_Penalty = penaltiestextEdit.Text;
                                    //saveDataObj.saveTime(obj, chooseRound_comboBox.SelectedIndex, cnrtextEdit.Text, timetextEdit.Text);
                                    //saveDataObj.savePenalty(obj, chooseRound_comboBox.SelectedIndex, cnrtextEdit.Text, penaltiestextEdit.Text);
                                }

                                obj_round2.Teams[i].MemberDetails[j].Round2_Time_Penalty = timePenstextEdit.Text;
                                obj_round2.Teams[i].MemberDetails[j].Round2_Total = totaltextEdit.Text;
                                //saveDataObj.saveTimePenalty(obj, chooseRound_comboBox.SelectedIndex, cnrtextEdit.Text, timePenstextEdit.Text);
                                //saveDataObj.saveTotalPenalty(obj, chooseRound_comboBox.SelectedIndex, cnrtextEdit.Text, totaltextEdit.Text);

                                paranthesis.update_paranthesisR2(obj_round2.Teams[i].MemberDetails);

                                for (int m = 0; m < obj_round2.Teams.Count; m++)
                                {
                                    int it = 0;
                                    while (it < obj.Teams.Count && obj_round2.Teams[m].Name != obj.Teams[it].Name)
                                    {
                                        it++;
                                    }
                                    if (obj_round2.Teams[m].Name == obj.Teams[it].Name)
                                    {
                                        saveDataObj.saveResultObj(obj_round2.Teams[m].MemberDetails, obj.Teams[it].MemberDetails);
                                    }
                                }

                                round2.create_ranking(obj_round2);
                                round2.create_ergebnis(obj_round2, obj_round2.Teams[i].No, cnrtextEdit.Text);

                                gridControl3.Refresh();
                                gridControl3.DataSource = null;
                                gridControl3.DataSource = obj_round2.Rank;

                                round2.create_datatable(obj_round2);
                                gridControl2.DataSource = null;
                                gridControl2.MainView = round2_gridView;
                                gridControl2.DataSource = round2.dataTable;

                                gridControl1.Refresh();
                                gridControl1.DataSource = null;
                                gridControl1.DataSource = obj_round2.Teams;

                                ExpandAll(team_gridView);
                                expand = true;
                            }

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with saveResultOfLiveWindowForR2 method!\n" + ex.Message);
            }
            

            cnrtextEdit.Text = null;
            //confirmcheckEdit.Checked = false;
            horseNametextEdit.Text = null;
            riderNametextEdit.Text = null;
            ioctextEdit.Text = null;
            timekeepingPenaltyTextEdit.Text = null;
            timekeepingTimeTextEdit.Text = null;
            timetextEdit.Text = null;
            penaltiestextEdit.Text = null;
            timePenstextEdit.Text = null;
            totaltextEdit.Text = null;
            teamtextEdit.Text = null;
        }
        // 
        // Common functionalities for live window 
        //
        private void chooseRound_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var cBox = sender as ComboBoxEdit;
                obj_internal.GeneralSetting.cBox = Convert.ToString(cBox.SelectedIndex);
                //Internal Json updates
                updateJson.modifyJsonForGeneralSettings(obj_internal);
                displayRoundData(cBox.SelectedIndex);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with chooseRound_comboBox event!\n" + ex.Message);
            }
            
        }
        public void displayRoundData(int cBox)
        {     
            try
            {
                if (cBox == 0)
                {
                    Round1_details();
                    timeAllowedLabel.Text = "Time Allowed : " + Convert.ToString(obj.Event.timeAllowedForRounds);
                    WriteDat dt = new WriteDat();
                    string rnd = "Round " + (cBox + 1);
                    dt.CreateDat4R1(obj.DataTableR1, obj, rnd);
                    setCnrIteratorR1(obj.DataTableR1);
                    cnrtextEdit.Focus();
                    cnrtextEdit.Select();
                    try
                    {
                        NationCup.ThreadReceive = new Thread(ReceiveMessages);
                        NationCup.ThreadReceive.Start();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }
                else if (cBox == 1)
                {
                    if (obj_round2 == null)
                    {
                        obj_round2 = obj.Clone() as Serialization;
                    }

                    bool flag = false;
                    for (int i = 0; i < obj_round2.Teams.Count(); i++)
                    {
                        for (int j = 0; j < obj_round2.Teams[i].MemberDetails.Count(); j++)
                        {
                            if (obj_round2.Teams[i].MemberDetails[j].Round2_Penalty != null || obj_round2.Teams[i].MemberDetails[j].Round2_Time != null)
                            {
                                flag = true;
                                break;
                            }

                        }

                        if (flag)
                            break;
                    }
                    if (flag)
                    {
                        Round2_details_without_ascending();
                        timeAllowedLabel.Text = "Time Allowed : " + Convert.ToString(obj_round2.Event.timeAllowedForRounds);
                        dt_iterator = 0;
                        WriteDat dt = new WriteDat();
                        string rnd = "Round " + (cBox + 1);
                        dt.CreateDat4R2(round2.dataTable, obj_round2, rnd);
                        setCnrIteratorR2(round2.dataTable);
                        cnrtextEdit.Focus();
                        cnrtextEdit.Select();
                        try
                        {
                            //NationCup.receivingUDPclient = new UdpClient(Convert.ToInt32(ConfigrationForm.TimingPort));
                            NationCup.ThreadReceive = new Thread(ReceiveMessages);
                            NationCup.ThreadReceive.Start();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                    else
                    {
                        Round2_details();
                        timeAllowedLabel.Text = "Time Allowed : " + Convert.ToString(obj_round2.Event.timeAllowedForRounds);
                        dt_iterator = 0;
                        WriteDat dt = new WriteDat();
                        string rnd = "Round " + (cBox + 1);
                        dt.CreateDat4R2(round2.dataTable, obj_round2, rnd);
                        setCnrIteratorR2(round2.dataTable);
                        cnrtextEdit.Focus();
                        cnrtextEdit.Select();
                        try
                        {
                            //NationCup.receivingUDPclient = new UdpClient(Convert.ToInt32(ConfigrationForm.TimingPort));
                            NationCup.ThreadReceive = new Thread(ReceiveMessages);
                            NationCup.ThreadReceive.Start();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                else if (cBox == 2)
                {

                }
                else if (cBox == 3)
                {
                    member_bandedGridView.Bands[3].Visible = true;
                    member_bandedGridView.OptionsSelection.MultiSelect = true;
                    JumpOff_details();
                }
                else if (String.IsNullOrEmpty(cBox.ToString()))
                {
                    MessageBox.Show("Please select the round!\n");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Problem with displayRoundData!\n" + ex.Message);
            }
          
        }
        public void startListGridviewFoucs()
        {
            if (chooseRound_comboBox.SelectedIndex == 0)
            {
                try
                {
                    if (obj.DataTableR1 != null)
                    {
                        for (int i = 0; i < obj.DataTableR1.Count; i++)
                        {
                            if (cnrtextEdit.Text == obj.DataTableR1[i].CNR)
                            {
                                round1_gridView.FocusedRowHandle = i;
                                round1_gridView.Focus();
                                round1_gridView.FocusedColumn = round1_gridView.Columns["CNR"];
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Problem with round 1 startListGridviewFocus!\n" + ex.Message);
                }
                
            }
            else if (chooseRound_comboBox.SelectedIndex == 1)
            {
                try
                {
                    if (round2.dataTable != null)
                    {
                        for (int i = 0; i < round2.dataTable.Rows.Count; i++)
                        {
                            if (cnrtextEdit.Text == round2.dataTable.Rows[i].ItemArray[2].ToString())
                            {
                                round2_gridView.FocusedRowHandle = i;
                                round2_gridView.Focus();
                                round2_gridView.FocusedColumn = round2_gridView.Columns["CNR"];
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Problem with round 2 startListGridviewFocus!\n" + ex.Message);
                }
               
            }
        }
        private void cnrtextEdit_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyData == Keys.Tab)
            {
                e.IsInputKey = true;

                timetextEdit.SelectAll();
                timetextEdit.Focus();
            }
        }
        private void cnrtextEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            { 
                var c = e.KeyCode;
                e.SuppressKeyPress = true;
                confirmButton_Click(sender, e);

                timetextEdit.Focus();
                timetextEdit.Select();
            }
            if (e.KeyCode == Keys.Down || e.KeyCode == Keys.Up)
            {
                startListGridviewFoucs();
            }
        }
        private void cnrtextEdit_EditValueChanged(object sender, EventArgs e)
        {
            startListGridviewFoucs();
        }
        WriteDat dt = new WriteDat();
        private void confirmButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (chooseRound_comboBox.SelectedIndex == 0)
                {
                    dt.CreateDat1(obj, cnrtextEdit.Text, chooseRound_comboBox.Text);
                    for (int i = 0; i < obj.DataTableR1.Count; i++)
                    {
                        if (cnrtextEdit.Text == obj.DataTableR1[i].CNR)
                        {
                            round1_gridView.FocusedRowHandle = i;
                            dt_iterator = i + 1;
                        }
                    }

                    for (int i = 0; i < obj.Teams.Count; i++)
                    {
                        for (int j = 0; j < obj.Teams[i].MemberDetails.Count; j++)
                        {
                            if (obj.Teams[i].MemberDetails[j].CNR == cnrtextEdit.Text)
                            {
                                round1.create_starter(obj, obj.Teams[i].No, cnrtextEdit.Text);
                                horseNametextEdit.Text = obj.Teams[i].MemberDetails[j].HorseName;
                                riderNametextEdit.Text = obj.Teams[i].MemberDetails[j].RiderName;
                                teamtextEdit.Text = obj.Teams[i].Name;
                                ioctextEdit.Text = obj.Teams[i].IocCode;

                                if (obj.Teams[i].MemberDetails[j].Round1_Time != null && obj.Teams[i].MemberDetails[j].Round1_Total != null)
                                {
                                    timetextEdit.Text = obj.Teams[i].MemberDetails[j].Round1_Time;
                                    penaltiestextEdit.Text = obj.Teams[i].MemberDetails[j].Round1_Penalty;
                                    timePenstextEdit.Text = obj.Teams[i].MemberDetails[j].Round1_Time_Penalty;
                                    totaltextEdit.Text = obj.Teams[i].MemberDetails[j].Round1_Total;
                                }
                                else if (Label_FZ.Text != "0.00 / 0.00")
                                {
                                    Label_FZ_TextChanged(sender, e);
                                }

                                try
                                {
                                    //NationCup.receivingUDPclient = new UdpClient(Convert.ToInt32(ConfigrationForm.TimingPort));
                                    NationCup.ThreadReceive = new Thread(ReceiveMessages);
                                    NationCup.ThreadReceive.Start();
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message);
                                }
                            }
                        }
                    }
                }

                if (chooseRound_comboBox.SelectedIndex == 1)
                {
                    dt.CreateDat1(obj_round2, cnrtextEdit.Text, chooseRound_comboBox.Text);
                    for (int i = 0; i < round2.dataTable.Rows.Count; i++)
                    {
                        if (cnrtextEdit.Text == round2.dataTable.Rows[i].ItemArray[2].ToString())
                        {
                            round2_gridView.FocusedRowHandle = i;
                            dt_iterator = i + 1;
                        }
                    }

                    for (int i = 0; i < obj_round2.Teams.Count; i++)
                    {
                        for (int j = 0; j < obj_round2.Teams[i].MemberDetails.Count; j++)
                        {
                            if (obj_round2.Teams[i].MemberDetails[j].CNR == cnrtextEdit.Text)
                            {
                                round2.create_starter(obj_round2, obj_round2.Teams[i].No, cnrtextEdit.Text);
                                horseNametextEdit.Text = obj_round2.Teams[i].MemberDetails[j].HorseName;
                                riderNametextEdit.Text = obj_round2.Teams[i].MemberDetails[j].RiderName;
                                teamtextEdit.Text = obj_round2.Teams[i].Name;
                                ioctextEdit.Text = obj_round2.Teams[i].IocCode;

                                timetextEdit.Text = obj_round2.Teams[i].MemberDetails[j].Round2_Time;
                                penaltiestextEdit.Text = obj_round2.Teams[i].MemberDetails[j].Round2_Penalty;
                                timePenstextEdit.Text = obj_round2.Teams[i].MemberDetails[j].Round2_Time_Penalty;
                                totaltextEdit.Text = obj_round2.Teams[i].MemberDetails[j].Round2_Total;

                                if (obj_round2.Teams[i].MemberDetails[j].Round2_Time != null && obj_round2.Teams[i].MemberDetails[j].Round2_Total != null)
                                {
                                    timetextEdit.Text = obj_round2.Teams[i].MemberDetails[j].Round2_Time;
                                    penaltiestextEdit.Text = obj_round2.Teams[i].MemberDetails[j].Round2_Penalty;
                                    timePenstextEdit.Text = obj_round2.Teams[i].MemberDetails[j].Round2_Time_Penalty;
                                    totaltextEdit.Text = obj_round2.Teams[i].MemberDetails[j].Round2_Total;
                                }
                                else if (Label_FZ.Text != "0,00 / 0,00")
                                {
                                    Label_FZ_TextChanged(sender, e);
                                }

                                try
                                {
                                    //NationCup.receivingUDPclient = new UdpClient(Convert.ToInt32(ConfigrationForm.TimingPort));
                                    NationCup.ThreadReceive = new Thread(ReceiveMessages);
                                    NationCup.ThreadReceive.Start();
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message);
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with confirm button!\n" + ex.Message);
            }
            
            penaltiestextEdit.ForeColor = Color.Black;
            timetextEdit.ForeColor = Color.Black;
        }
        UpdateJson updateJson = new UpdateJson();
        private void timetextEdit_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyData == Keys.Tab)
            {
                e.IsInputKey = true;
                penaltiestextEdit.SelectAll();
                penaltiestextEdit.Focus();

                timekeepingTimeTextEdit.BackColor = Color.White;
                timekeepingTimeTextEdit.ForeColor = Color.Black;

                timekeepingPenaltyTextEdit.BackColor = Color.White;
                timekeepingPenaltyTextEdit.ForeColor = Color.Black;

                timetextEdit.BackColor = Color.Green;
                timetextEdit.ForeColor = Color.White;

                penaltiestextEdit.BackColor = Color.Green;
                penaltiestextEdit.ForeColor = Color.White;

                penaltycalculate();

            }
        }
        private void timetextEdit_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (flag == true)
            {
                e.Handled = true;
                flag = false;
            }

            if (e.KeyChar == (char)Keys.Enter)
            {
                timekeepingTimeTextEdit.BackColor = Color.White;
                timekeepingTimeTextEdit.ForeColor = Color.Black;

                timekeepingPenaltyTextEdit.BackColor = Color.White;
                timekeepingPenaltyTextEdit.ForeColor = Color.Black;

                timetextEdit.BackColor = Color.Green;
                timetextEdit.ForeColor = Color.White;

                penaltiestextEdit.BackColor = Color.Green;
                penaltiestextEdit.ForeColor = Color.White;

                penaltycalculate();
                if (timetextEdit.Text == "")
                {
                    timetextEdit.Focus();
                    timetextEdit.SelectAll();
                }
                else
                {
                    penaltiestextEdit.Focus();
                    penaltiestextEdit.SelectAll();
                }
            }
        }
        private void penaltiestextEdit_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyData == Keys.Tab)
            {
                e.IsInputKey = true;
                timePenstextEdit.SelectAll();
                timePenstextEdit.Focus();

                timekeepingTimeTextEdit.BackColor = Color.White;
                timekeepingTimeTextEdit.ForeColor = Color.Black;

                timekeepingPenaltyTextEdit.BackColor = Color.White;
                timekeepingPenaltyTextEdit.ForeColor = Color.Black;

                penaltiestextEdit.BackColor = Color.Green;
                penaltiestextEdit.ForeColor = Color.White;

                penaltiestextEdit.BackColor = Color.Green;
                penaltiestextEdit.ForeColor = Color.White;


                penaltycalculate();

            }
        }
        private void penaltiestextEdit_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (flag == true)
            {
                e.Handled = true;
                flag = false;
            }

            if (e.KeyChar == (char)Keys.Enter)
            {
                timekeepingTimeTextEdit.BackColor = Color.White;
                timekeepingTimeTextEdit.ForeColor = Color.Black;

                timekeepingPenaltyTextEdit.BackColor = Color.White;
                timekeepingPenaltyTextEdit.ForeColor = Color.Black;

                penaltiestextEdit.BackColor = Color.Green;
                penaltiestextEdit.ForeColor = Color.White;

                penaltiestextEdit.BackColor = Color.Green;
                penaltiestextEdit.ForeColor = Color.White;

                penaltycalculate();
                timePenstextEdit.Focus();
                timePenstextEdit.Select();
            }
        }
        private void timePenstextEdit_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            try
            {
                if (e.KeyData == Keys.Tab)
                {
                    e.IsInputKey = true;
                    okButton.Focus();

                    PenaltyCompute penaltyCompute = new PenaltyCompute();
                    if (timetextEdit.Text != "" && timetextEdit.BackColor == Color.Green)
                    {
                        if (penaltiestextEdit.Text == "")
                            totaltextEdit.Text = timePenstextEdit.Text;
                        else
                            totaltextEdit.Text = Convert.ToString(Convert.ToInt32(timePenstextEdit.Text) + Convert.ToInt32(penaltiestextEdit.Text));
                    }

                    if (timetextEdit.Text == "" && penaltiestextEdit.Text == "" && timekeepingTimeTextEdit.BackColor == Color.Green && timekeepingPenaltyTextEdit.BackColor == Color.Green)
                    {
                        if (timekeepingPenaltyTextEdit.Text == "")
                            totaltextEdit.Text = timePenstextEdit.Text;
                        if (timekeepingPenaltyTextEdit.Text != "")
                        {
                            totaltextEdit.Text = Convert.ToString(Convert.ToInt32(timePenstextEdit.Text) + Convert.ToInt32(timekeepingPenaltyTextEdit.Text));
                        }
                    }

                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Problem with timePenstextEdit_PreviewKeyDown!\n" + ex.Message);
            }
           
        }
        private void timePenstextEdit_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (flag == true)
                {
                    e.Handled = true;
                    flag = false;
                }

                if (e.KeyChar == (char)Keys.Enter)
                {
                    if (timetextEdit.Text != "" && timetextEdit.BackColor == Color.Green)
                    {
                        if (penaltiestextEdit.Text == "")
                            totaltextEdit.Text = timePenstextEdit.Text;
                        else
                            totaltextEdit.Text = Convert.ToString(Convert.ToInt32(timePenstextEdit.Text) + Convert.ToInt32(penaltiestextEdit.Text));
                    }

                    if (timetextEdit.Text == "" && penaltiestextEdit.Text == "" && timekeepingTimeTextEdit.BackColor == Color.Green && timekeepingPenaltyTextEdit.BackColor == Color.Green)
                    {
                        if (timekeepingPenaltyTextEdit.Text == "")
                            totaltextEdit.Text = timePenstextEdit.Text;
                        if (timekeepingPenaltyTextEdit.Text != "")
                        {
                            totaltextEdit.Text = Convert.ToString((Convert.ToInt32(timePenstextEdit.Text)) + (Convert.ToDouble(timekeepingPenaltyTextEdit.Text)));
                        }
                    }
                    okButton.Focus();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Problem with timePenstextEdit_KeyPress event!\n" + ex.Message);
            }
            
        }
        private void okButton_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyData == Keys.Tab)
            {
                e.IsInputKey = true;
                cnrtextEdit.SelectAll();
                cnrtextEdit.Focus();
            }
        }
        private void okButton_Click(object sender, EventArgs e)
        {       
            try
            {              
                if (chooseRound_comboBox.SelectedIndex == 0)
                {
                    saveResultOfLiveWindowForR1();
                    setCnrIteratorR1(obj.DataTableR1);
                    updateJson.modifyJson(obj);
                }

                else if (chooseRound_comboBox.SelectedIndex == 1)
                {
                    saveResultOfLiveWindowForR2();                  
                    setCnrIteratorR2(round2.dataTable);
                    updateJson.modifyJson(obj_round2);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Problem with ok button!\n" + ex.Message);
            }
            //WriteDat writeDat = new WriteDat();
            //writeDat.CreateDat3(obj, chooseRound_comboBox.Text);

            penaltiestextEdit.BackColor = Color.White;
            timetextEdit.BackColor = Color.White;  
            
            cnrtextEdit.Focus();
            cnrtextEdit.SelectAll();
        }
        private void takeOverButton_Click(object sender, EventArgs e)
        {
            try
            {
                char[] spearator = { '/', ' ' };
                Int32 count = 2;

                // Using the Method 
                String[] strlist = Label_FZ.Text.Split(spearator,
                       count, StringSplitOptions.None);
                char[] charsToTrim = { '/' };

                timekeepingPenaltyTextEdit.Text = strlist[0].Trim(charsToTrim);
                timekeepingTimeTextEdit.Text = strlist[1].Trim(charsToTrim);

                timetextEdit.Text = "";
                timetextEdit.BackColor = Color.White;
                timetextEdit.ForeColor = Color.Black;

                penaltiestextEdit.Text = "";
                penaltiestextEdit.BackColor = Color.White;
                penaltiestextEdit.ForeColor = Color.Black;

                timekeepingTimeTextEdit.BackColor = Color.Green;
                timekeepingTimeTextEdit.ForeColor = Color.White;

                timekeepingPenaltyTextEdit.BackColor = Color.Green;
                timekeepingPenaltyTextEdit.ForeColor = Color.White;
                takingDataFromTimeKeeper();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Problem with takeOver button!\n" + ex.Message);
            }           
        }
        // Set penalty based on EL, RT, DQ, DNS buttons
        private void elButton_Click(object sender, EventArgs e)
        {
            try
            {
                timetextEdit.BackColor = Color.White;
                timetextEdit.ForeColor = Color.Black;

                penaltiestextEdit.BackColor = Color.White;
                penaltiestextEdit.ForeColor = Color.Black;

                timekeepingTimeTextEdit.BackColor = Color.Green;
                timekeepingTimeTextEdit.ForeColor = Color.White;

                timekeepingPenaltyTextEdit.BackColor = Color.Green;
                timekeepingPenaltyTextEdit.ForeColor = Color.White;

                timekeepingPenaltyTextEdit.Text = "1001";
                timekeepingTimeTextEdit.Text = "";
                timePenstextEdit.Text = "";
                timetextEdit.Text = "";
                penaltiestextEdit.Text = "";
                timePenstextEdit.Text = "";
                totaltextEdit.Text = "EL";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with EL button!\n" + ex.Message);
            }
            
        }
        private void rtButton_Click(object sender, EventArgs e)
        {
            try
            {
                timetextEdit.BackColor = Color.White;
                timetextEdit.ForeColor = Color.Black;

                penaltiestextEdit.BackColor = Color.White;
                penaltiestextEdit.ForeColor = Color.Black;

                timekeepingTimeTextEdit.BackColor = Color.Green;
                timekeepingTimeTextEdit.ForeColor = Color.White;

                timekeepingPenaltyTextEdit.BackColor = Color.Green;
                timekeepingPenaltyTextEdit.ForeColor = Color.White;

                timekeepingPenaltyTextEdit.Text = "1002";
                timekeepingTimeTextEdit.Text = "";
                timePenstextEdit.Text = "";
                timetextEdit.Text = "";
                penaltiestextEdit.Text = "";
                timePenstextEdit.Text = "";

                totaltextEdit.Text = "RT";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with RT button!\n" + ex.Message);
            }
            
        }
        private void dqButton_Click(object sender, EventArgs e)
        {
            try
            {
                timetextEdit.BackColor = Color.White;
                timetextEdit.ForeColor = Color.Black;

                penaltiestextEdit.BackColor = Color.White;
                penaltiestextEdit.ForeColor = Color.Black;

                timekeepingTimeTextEdit.BackColor = Color.Green;
                timekeepingTimeTextEdit.ForeColor = Color.White;

                timekeepingPenaltyTextEdit.BackColor = Color.Green;
                timekeepingPenaltyTextEdit.ForeColor = Color.White;

                timekeepingPenaltyTextEdit.Text = "1003";
                timekeepingTimeTextEdit.Text = "";
                timePenstextEdit.Text = "";
                timetextEdit.Text = "";
                penaltiestextEdit.Text = "";
                timePenstextEdit.Text = "";

                totaltextEdit.Text = "DQ";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with DQ button!\n" + ex.Message);
            }
           
        }
        private void dnsButton_Click(object sender, EventArgs e)
        {
            try
            {
                timetextEdit.BackColor = Color.White;
                timetextEdit.ForeColor = Color.Black;

                penaltiestextEdit.BackColor = Color.White;
                penaltiestextEdit.ForeColor = Color.Black;

                timekeepingTimeTextEdit.BackColor = Color.Green;
                timekeepingTimeTextEdit.ForeColor = Color.White;

                timekeepingPenaltyTextEdit.BackColor = Color.Green;
                timekeepingPenaltyTextEdit.ForeColor = Color.White;

                timekeepingPenaltyTextEdit.Text = "1004";
                timekeepingTimeTextEdit.Text = "";
                timePenstextEdit.Text = "";
                timetextEdit.Text = "";
                penaltiestextEdit.Text = "";

                timePenstextEdit.Text = "";
                totaltextEdit.Text = "DNS";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with DNS button!\n" + ex.Message);
            }
            
        }
        // 
        // Connection to time keeper data and receive timekeeping data
        //
        delegate void SetInternetStateCallback(int idx);
        ClassConfig Config = new ClassConfig();
        public static UdpClient receivingUDPclient = new UdpClient();    
        public UdpClient sendingUDPclient = new UdpClient();
        public static System.Net.IPEndPoint RemoteIpEndPoint = new System.Net.IPEndPoint(System.Net.IPAddress.Any, 0);
        public static System.Threading.Thread ThreadReceive;
        public System.Threading.Thread ThreadInternet;
        public ClassTimingData TimingData = new ClassTimingData();
        public bool UseOldInput = true;

        [System.Runtime.InteropServices.DllImport("USER32")]
        public static extern int GetKeyState(long nVirtKey);
        public const short VK_SHIFT = 16;
        public string TrackingIncidents = "";
        string json;
        // Receive data from timekeeper
        public void ReceiveMessages()
        {
            try
            {
                int x;
                string s;         
                byte[] receiveBytes = receivingUDPclient.Receive(ref RemoteIpEndPoint);
                char[] strReturnData = System.Text.Encoding.UTF8.GetChars(receiveBytes);

                json = null;
                for (int i = 0; i < strReturnData.Length; i++)
                {
                    json += strReturnData[i];
                }

                string strResponseIP = RemoteIpEndPoint.Address.ToString();
                if (Strings.InStr(json, "RECOVER-START") > 0)
                {
                    TrackingIncidents += "|" + "RECOVER-START";
                }
                else if (Strings.InStr(json, "RECOVER-FINISH") > 0)
                {
                    TrackingIncidents += "|" + "RECOVER-FINISH";
                }
                else if (Strings.InStr(json, "RECOVER-PHASE") > 0)
                {
                    TrackingIncidents += "|" + "RECOVER-PHASE";
                }
                else if (Strings.InStr(json, "MAN-START") > 0)
                {
                    TrackingIncidents += "|" + "MAN-START";
                }
                else if (Strings.InStr(json, "MAN-FINISH") > 0)
                {
                    TrackingIncidents += "|" + "MAN-FINISH";
                }
                else if (Strings.InStr(json, "MAN-PHASE") > 0)
                {
                    TrackingIncidents += "|" + "MAN-PHASE";
                }
                else if (Strings.InStr(json, "V-") > 0)
                {
                    x = Strings.InStr(json, "V-");
                    s = Strings.Mid(json, x);
                    TrackingIncidents += "|" + Strings.Left(s, Strings.InStr(s, Convert.ToString('"')) - 1);
                }
                else
                {
                    TimingData.AssignJSON(json);
                }

                // Thread wieder aktivieren
                ThreadReceive = new System.Threading.Thread(ReceiveMessages);
                ThreadReceive.Start();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            // Console.WriteLine(e.Message)
            finally
            {
                //receivingUDPclient.Close();
            }
        }
        private void UDPTimer_Tick(object sender, EventArgs e)
        {
            string s;

            if (Config.FaultDigits == -1)
                s = Strings.Format(TimingData.Timing.Fehler, "##,##0.00");
            else
                s = Strings.Format(TimingData.Timing.Fehler, "##,##0." + Strings.StrDup(Config.FaultDigits, "0") + "");
            s = s + " / " + Strings.Format(TimingData.Timing.Zeit, "##,##0." + Strings.StrDup(Config.TimeDigits, "0") + "");
            if (TimingData.Timing.ZeitVorgabe > 0)
                s = s + " (+" + Strings.Format(TimingData.Timing.ZeitVorgabe, "##,##0." + Strings.StrDup(Config.TimeDigits, "0") + "") + ")";
            if (Label_FZ.Text != s)
                Label_FZ.Text = s;
        }
        public int ProgressPos;
        public int ProgressMax;
        private void Timer_ProgressBar_Tick(object sender, EventArgs e)
        {
            try
            {
                if (ProgressPos > ProgressMax)
                    ProgressPos = ProgressMax;
                if (ProgressBar.Maximum != ProgressMax)
                    ProgressBar.Maximum = ProgressMax;
                if (ProgressBar.Value != ProgressPos)
                    ProgressBar.Value = ProgressPos;
                if (ProgressBar.Value == ProgressBar.Maximum)
                    ProgressBar.Visible = false;
                else
                    ProgressBar.Visible = true;
                Application.DoEvents();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        // Compute time penalty, set values in live window, and create TV2.Dat file
        public void takingDataFromTimeKeeper()
        {
            try
            {
                PenaltyCompute penaltyCompute = new PenaltyCompute();

                // compute penalties
                int pen = 0;
                if (timetextEdit.Text == "" && timekeepingTimeTextEdit.Text != "")
                {
                    pen = penaltyCompute.computePenalties(Convert.ToDouble(timekeepingTimeTextEdit.Text));
                    timePenstextEdit.Text = Convert.ToString(pen);
                }

                if (timekeepingPenaltyTextEdit.Text != "" && penaltiestextEdit.Text == "")
                    totaltextEdit.Text = Convert.ToString(pen + Convert.ToDouble(timekeepingPenaltyTextEdit.Text));
                WriteDat writeDat = new WriteDat();
                if (chooseRound_comboBox.SelectedIndex == 0)
                {
                    writeDat.CreateDat2(obj, cnrtextEdit.Text, totaltextEdit.Text, timekeepingTimeTextEdit.Text, chooseRound_comboBox.Text);
                }
                else if (chooseRound_comboBox.SelectedIndex == 1)
                {
                    writeDat.CreateDat2(obj_round2, cnrtextEdit.Text, totaltextEdit.Text, timekeepingTimeTextEdit.Text, chooseRound_comboBox.Text);
                }
                else if (chooseRound_comboBox.SelectedIndex == 2)
                {
                    writeDat.CreateDat2(obj, cnrtextEdit.Text, totaltextEdit.Text, timekeepingTimeTextEdit.Text, chooseRound_comboBox.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with takingDataFromTimeKeeper method!\n" + ex.Message);
            }
           
        }       
        // 
        // Ribbon buttons for TV4.Dat, storder.npw, and fast reports
        //
        // Create TV4.Dat file
        private void startlistForTVbarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                WriteDat dt = new WriteDat();
                if (chooseRound_comboBox.SelectedIndex == 0)
                {
                    dt.CreateDat4R1(obj.DataTableR1, obj, chooseRound_comboBox.Text);
                    MessageBox.Show("TV4.DAT is created sucessfully!");
                }
                else if (chooseRound_comboBox.SelectedIndex == 1)
                {
                    if (obj_round2 != null)
                    {
                        dt.CreateDat4R2(round2.dataTable, obj_round2, chooseRound_comboBox.Text);
                        MessageBox.Show("TV4.DAT is created sucessfully!");
                    }                 
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with startListForTV button!\n" + ex.Message);
            }
            
        }
        // Create round1 storder.npw file
        private void round1barButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if(obj != null)
                {
                    round1.create_storder(obj, obj.DataTableR1);
                    MessageBox.Show("Round 1 storder.npw file is created sucessfully!");
                }               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with round1 startlist for show" + ex.Message);
            }
          
        }
        // Create round2 storder.npw file
        private void round2barButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (obj_round2 != null)
                {
                    round2.create_storder(obj_round2);
                    MessageBox.Show("Round 2 storder.npw file is created sucessfully!");
                }
                else
                {
                    //round2.create_storder(obj);
                    MessageBox.Show("Round 2 is not started yet!\n");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with round2 startlist for show" + ex.Message);
            }
            
        }     
        WriteFastReports fastReports = new WriteFastReports();
        private void startOrderForRound1barButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                round1_Database.GetInfoR1(obj.DataTableR1, obj);
                //Data source for fast report
                round1databaseBindingSource.DataSource = round1_Database.list_round1;
                fastReports.setReport(obj, storderReport);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with fast report for start order for round 1!\n" + ex.Message);
            }
           
        }
        private void startOrderForRound1barButtonItem_ItemPress(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (NationCup.ModifierKeys == Keys.Control)
                {
                    round1_Database.GetInfoR1(obj.DataTableR1, obj);
                    //Data source for fast report
                    round1databaseBindingSource.DataSource = round1_Database.list_round1;
                    fastReports.editReport(obj, storderReport);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with fast report for start order for round 1!\n" + ex.Message);
            }
        }
        // Generate fast report for startlist of round 1
        private void startListR1barButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                round1.getStartListR1(obj, obj.DataTableR1);
                //Data source for fast report
                teamCompetitionBindingSource1.DataSource = round1.startListR1;
                fastReports.setReport(obj, startListR1Report);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with fast report for startlist of round 1!\n" + ex.Message);
            }
              
        }
        // Open edit mode fast report for startlist of round 1
        private void startListR1barButtonItem_ItemPress(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (NationCup.ModifierKeys == Keys.Control)
            {
                round1.getStartListR1(obj, obj.DataTableR1);
                //Data source for fast report
                teamCompetitionBindingSource1.DataSource = round1.startListR1;
                fastReports.editReport(obj, startListR1Report);
            }
        }
        // Generate fast report for result of round 1
        Serialization obj_round1;
        private void resultOfR1barButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                obj_round1 = obj.Clone() as Serialization; 
                bool flag = false;
                for (int i = 0; i < obj_round1.Teams.Count; i++)
                {
                    for (int j = 0; j < obj_round1.Teams[i].MemberDetails.Count; j++)
                    {
                        if (obj_round1.Teams[i].MemberDetails[j].Round1_Total == null && obj_round1.Teams[i].MemberDetails[j].Round1_Time == null)
                        {
                            flag = true;
                        }
                    }
                }
                
                if (chooseRound_comboBox.SelectedIndex != 0 || flag == false)
                {
                    if (obj_round1.ResultR1.Count == 0)
                    {
                        round1.getresultListR1(obj_round1);
                    }
                   
                    //Data source for fast report
                    teamCompetitionBindingSource1.DataSource = obj_round1.ResultR1;
                    fastReports.setReport(obj_round1, resultForR1Report);
                }
                else
                {
                    MessageBox.Show("Round 1 is not finished yet!\n");
                }            
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with fast report for result of round 1!\n" + ex.Message);
            }          
        }
        // Open edit mode fast report for result of round 1
        private void resultOfR1barButtonItem_ItemPress(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (NationCup.ModifierKeys == Keys.Control)
            {
                bool flag = false;
                if (obj_round1 == null)
                {
                    obj_round1 = obj.Clone() as Serialization;
                }
                obj_round1 = rankingOrder.AscendingRanking(obj_round1);
                int count = 0;
                for (int i = 0; i < obj_round1.Teams.Count; i++)
                {
                    count = count + 1;
                    obj_round1.Teams[i].position = count;
                }

                for (int i = 0; i < obj_round1.Teams.Count; i++)
                {
                    for (int j = 0; j < obj_round1.Teams[i].MemberDetails.Count; j++)
                    {
                        if (obj_round1.Teams[i].MemberDetails[j].Round1_Total == null && obj_round1.Teams[i].MemberDetails[j].Round1_Time == null)
                        {
                            flag = true;
                        }
                    }
                }
                
                if (chooseRound_comboBox.SelectedIndex != 0 || flag == false)
                {
                    round1.getresultListR1(obj_round1);
                    //Data source for fast report
                    teamCompetitionBindingSource1.DataSource = round1.resultListR1;
                    fastReports.editReport(obj_round1, resultForR1Report);
                }
                else
                {
                    MessageBox.Show("Round 1 is not finished yet!\n");
                }
                //fastReports.setResultR1(obj_round1, flag);

                //if (chooseRound_comboBox.SelectedIndex != 0 || !flag)
                //{
                //    //Data source for fast report
                //    teamCompetitionBindingSource1.DataSource = obj_round1.Teams;
                //    fastReports.editReport(obj_round1, resultForR1Report);
                //}
                //else
                //{
                //    MessageBox.Show("Please check round 1 is finished completely or not!");
                //}
            }
        }        
        Serialization tmp_obj;
        
        private void startOrderForR2barButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (obj_round2 == null)
                {
                    obj_round2 = obj.Clone() as Serialization;
                }
                if (round1_Database.list_round2 == null)
                {
                    bool flag = false;
                    for (int i = 0; i < obj_round2.Teams.Count(); i++)
                    {
                        for (int j = 0; j < obj_round2.Teams[i].MemberDetails.Count(); j++)
                        {
                            if (obj_round2.Teams[i].MemberDetails[j].Round2_Penalty != null || obj_round2.Teams[i].MemberDetails[j].Round2_Time != null)
                            {
                                flag = true;
                                break;
                            }
                        }

                        if (flag)
                            break;
                    }
                    if (flag)
                    {
                        round2.create_datatable(obj_round2);
                        round1_Database.GetInfoR2(round2.dataTable, obj_round2);
                    }
                    else
                    {
                        obj_round2 = rankingOrder.AscendingRanking(obj_round2);
                        round2.create_datatable(obj_round2);
                        round1_Database.GetInfoR2(round2.dataTable, obj_round2);
                    }
                }
                //Data source for fast report
                round1databaseBindingSource.DataSource = round1_Database.list_round2;
                fastReports.setReport(obj_round2, storderReport);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with fast report for start order for round 2!\n" + ex.Message);
            }
           
        }
        private void startOrderForR2barButtonItem_ItemPress(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (NationCup.ModifierKeys == Keys.Control)
            {
                try
                {
                    if (obj_round2 == null)
                    {
                        obj_round2 = obj.Clone() as Serialization;
                    }
                    if (round1_Database.list_round2 == null)
                    {
                        bool flag = false;
                        for (int i = 0; i < obj_round2.Teams.Count(); i++)
                        {
                            for (int j = 0; j < obj_round2.Teams[i].MemberDetails.Count(); j++)
                            {
                                if (obj_round2.Teams[i].MemberDetails[j].Round2_Penalty != null || obj_round2.Teams[i].MemberDetails[j].Round2_Time != null)
                                {
                                    flag = true;
                                    break;
                                }

                            }

                            if (flag)
                                break;
                        }
                        if (flag)
                        {
                            round2.create_datatable(obj_round2);
                            round1_Database.GetInfoR2(round2.dataTable, obj_round2);
                        }
                        else
                        {
                            obj_round2 = rankingOrder.AscendingRanking(obj_round2);
                            round2.create_datatable(obj_round2);
                            round1_Database.GetInfoR2(round2.dataTable, obj_round2);
                        }
                    }
                    //Data source for fast report
                    round1databaseBindingSource.DataSource = round1_Database.list_round2;
                    fastReports.setReport(obj_round2, storderReport);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Problem with fast report for start order for round 2!\n" + ex.Message);
                }
            }
        }

        //Serialization obj_round2;

        // Generate fast report for startlist of round 2
        private void startListR2barButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (obj_round2 == null)
            {
                obj_round2 = obj.Clone() as Serialization;
            }

            //fastReports.getStartOrderAndStartListR2(obj_round2);       
            //Data source for fast report
            int count = 0;
            for (int i = 0; i < round2.startListR2.Count(); i++)
            {
                count = count + 1;
                round2.startListR2[i].position = count;
            }
            teamCompetitionBindingSource1.DataSource = round2.startListR2;
            // set parameters
            fastReports.setReport(obj_round2, startListR2Report);
        }
        // Open edit mode fast report for startlist of round 2
        private void startListR2barButtonItem_ItemPress(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (NationCup.ModifierKeys == Keys.Control)
                {
                    if (obj_round2 == null)
                    {
                        obj_round2 = obj.Clone() as Serialization;
                    }

                    //fastReports.getStartOrderAndStartListR2(obj_round2);
                    //Data source for fast report
                    teamCompetitionBindingSource1.DataSource = round2.startListR2;
                    // set parameters
                    fastReports.editReport(obj_round2, startListR2Report);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with opening edit mode of fast report for startlist of round 2!\n" + ex.Message);
            }
            
        }
        // Generate fast report for result of round 2
        private void resultOfR2barButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (obj_round2 == null)
                {
                    obj_round2 = obj.Clone() as Serialization;
                }
                bool flag = false;
                //obj_round2 = rankingOrder.AscendingRanking(obj_round2);
                for (int i = 0; i < round2.dataTable.Rows.Count; i++)
                {
                    if (string.IsNullOrEmpty(round2.dataTable.Rows[i].ItemArray[5].ToString()) || string.IsNullOrEmpty(round2.dataTable.Rows[i].ItemArray[6].ToString()))
                    {
                        flag = true;
                    }                   
                }                
                if (chooseRound_comboBox.SelectedIndex != 1 || flag == false)
                {
                    if(obj_round2.ResultR2.Count == 0)
                    {
                        round2.getresultListR2(obj_round2);
                    }
                    //Data source for fast report
                    teamCompetitionBindingSource1.DataSource = obj_round2.ResultR2;
                    // set parameters
                    fastReports.setReport(obj_round2, resultForR2Report);
                }
                else
                {
                    MessageBox.Show("Round 2 has not been finished yet!\n");
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with generating fast report for result of round 2!\n" + ex.Message);
            }
            
        }
        // Open edit mode fast report for result of round 2
        private void resultOfR2barButtonItem_ItemPress(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (NationCup.ModifierKeys == Keys.Control)
                {
                    if (obj_round2 == null)
                    {
                        obj_round2 = obj.Clone() as Serialization;
                    }

                    round2.getresultListR2(obj_round2);
                    //Data source for fast report
                    teamCompetitionBindingSource1.DataSource = round2.resultListR2;
                    fastReports.editReport(obj_round2, resultForR2Report);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with opening edit mode fast report for result of round 2" + ex.Message);
            }
            
        }
        //Serialization obj_final;
        // Generate fast report for final result
        private void finalReportBarButtonItem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (obj_round2 == null)
                {
                    obj_round2 = obj.Clone() as Serialization;
                }
                
                fastReports.setFinalRanking(obj_round2);
                
                //Data source for fast report
                teamCompetitionBindingSource1.DataSource = round2.resultListR2;
                fastReports.setReport(obj, finalResultReport);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with generating fast report for final result" + ex.Message);
            }          
        }
        //private void finalReport(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        //{
        //    //Paranthesis paranthesis = new Paranthesis();
        //    //tmp_obj = obj;

        //    //for (int i = 0; i < tmp_obj.Teams.Count; i++)
        //    //{
        //    //    paranthesis.update_paranthesisR1(tmp_obj.Teams[i].MemberDetails);
        //    //}

        //    //for (int i = 0; i < tmp_obj.Teams.Count; i++)
        //    //{
        //    //    int it = i;
        //    //    while (obj.Rank[i].teamName != tmp_obj.Teams[it].Name)
        //    //    {
        //    //        it++;
        //    //    }
        //    //    if (it != i)
        //    //    {
        //    //        TeamCompetition tmp_team = new TeamCompetition();
        //    //        tmp_team = tmp_obj.Teams[i];
        //    //        tmp_obj.Teams[i] = tmp_obj.Teams[it];
        //    //        tmp_obj.Teams[i].No = obj.Rank[i].rank;
        //    //        tmp_obj.Teams[it] = tmp_team;
        //    //    }
        //    //    else
        //    //    {
        //    //        tmp_obj.Teams[it].No = obj.Rank[i].rank;
        //    //    }
        //    //}

        //    obj_round2 = rankingOrder.AscendingRanking(obj_round2);
        //    //Data source for fast report
        //    teamCompetitionBindingSource1.DataSource = obj_round2.Teams;
        //    fastReports.setReport(obj_round2, finalResultReport);

        //}
    
        // Open edit mode fast report for final result
        private void finalReportBarButtonItem_ItemPress(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (NationCup.ModifierKeys == Keys.Control)
                {
                    if (obj_round2 == null)
                    {
                        obj_round2 = obj.Clone() as Serialization;
                    }
                    //obj_final = obj.Clone() as Serialization;
                    fastReports.setFinalRanking(obj_round2);
                    //Data source for fast report
                    teamCompetitionBindingSource1.DataSource = round2.resultListR2;
                    fastReports.editReport(obj_round2, finalResultReport);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with opening edit mode fast report for final result" + ex.Message);
            }
            
        }
        //private void finalReport_ItemPress(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        //{
        //    if (NationCup.ModifierKeys == Keys.Control)
        //    {
        //        obj_round2 = rankingOrder.AscendingRanking(obj_round2);
        //        //Data source for fast report
        //        teamCompetitionBindingSource1.DataSource = obj_round2.Teams;
        //        fastReports.editReport(obj_round2, finalResultReport);
        //    }
        //}
        // Generate fast report for startlist of current round
        private void startListOfCurrentRound_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (chooseRound_comboBox.SelectedIndex == 0)
                {
                   
                    if (round1.startListR1.Count == 0)
                    {
                        round1.getStartListR1(obj, obj.DataTableR1);
                    }
                    //Data source for fast report
                    teamCompetitionBindingSource1.DataSource = round1.startListR1;
                    // set parameters
                    fastReports.setReport(obj, startListR1Report);
                }
                else if (chooseRound_comboBox.SelectedIndex == 1)
                {
                    if (round2.startListR2.Count == 0)
                    {
                        round2.getStartListR2(obj_round2);
                    }

                    int count = 0;
                    for (int i = 0; i < round2.startListR2.Count(); i++)
                    {
                        count = count + 1;
                        round2.startListR2[i].position = count;
                    }
                    //Data source for fast report
                    teamCompetitionBindingSource1.DataSource = round2.startListR2;
                    // set parameters
                    fastReports.setReport(obj_round2, startListR2Report);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with generatiing fast report for startlist of current round!\n" + ex.Message);
            }
           
        }
        // Open edit mode fast report for startlist of current round 
        private void startListOfCurrentRound_ItemPress(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (NationCup.ModifierKeys == Keys.Control)
                {
                    if (chooseRound_comboBox.SelectedIndex == 0)
                    {
                        startListR1barButtonItem_ItemPress(sender, e);
                    }
                    else if (chooseRound_comboBox.SelectedIndex == 1)
                    {
                        startListR2barButtonItem_ItemPress(sender, e);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with opening edit mode fast report for startlist of current round!\n" + ex.Message);
            }
            
        }
        // Generate fast report for result of current finished round
        private void resultOfCurrentFinishedRound_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (chooseRound_comboBox.SelectedIndex == 0)
                {
                    if (obj_round1 == null)
                    {
                        obj_round1 = obj.Clone() as Serialization;
                    }
                    bool flag = false;
                    for (int i = 0; i < obj_round1.DataTableR1.Count; i++)
                    {
                        if (obj_round1.DataTableR1[i].Round1_penalty == null || obj_round1.DataTableR1[i].Round1_penalty == null)
                        {
                            flag = true;
                        }
                    }
                    if (flag == false)
                    {
                        round1.getresultListR1(obj_round1);
                        teamCompetitionBindingSource1.DataSource = round1.resultListR1;
                        fastReports.setReport(obj_round1, resultForR1Report);
                    }
                    else
                    {
                        MessageBox.Show("Current round is not finished yet!\n");
                    }

                }
                else if (chooseRound_comboBox.SelectedIndex == 1)
                {
                   
                    if (round2.dataTable != null)
                    {
                        bool flag = false;
                        for (int i = 0; i < round2.dataTable.Rows.Count; i++)
                        {
                            if (string.IsNullOrEmpty(round2.dataTable.Rows[i].ItemArray[5].ToString())|| string.IsNullOrEmpty(round2.dataTable.Rows[i].ItemArray[6].ToString()))
                            {
                                flag = true;
                            }
                        }
                        if (flag == false)
                        {
                            round2.getresultListR2(obj_round2);
                            teamCompetitionBindingSource1.DataSource = round2.resultListR2;
                            fastReports.setReport(obj_round2, resultForR2Report);
                        }
                        else
                        {
                            resultOfR1barButtonItem_ItemClick(sender, e);
                        }
                    }                 
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with generating fast report for result of current finished round!\n" + ex.Message);
            }
           
        }
        // Open edit mode fast report for result of current finished round 
        private void resultOfCurrentFinishedRound_ItemPress(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (NationCup.ModifierKeys == Keys.Control)
                {
                    if (chooseRound_comboBox.SelectedIndex == 0)
                    {
                        if (obj_round1 == null)
                        {
                            obj_round1 = obj.Clone() as Serialization;
                        }
                        bool flag = false;
                        for (int i = 0; i < obj_round1.DataTableR1.Count; i++)
                        {
                            if (obj_round1.DataTableR1[i].Round1_penalty == null || obj_round1.DataTableR1[i].Round1_penalty == null)
                            {
                                flag = true;
                            }
                        }
                        if (flag == false)
                        {
                            round1.getresultListR1(obj_round1);
                            teamCompetitionBindingSource1.DataSource = round1.resultListR1;
                            fastReports.editReport(obj_round1, resultForR1Report);
                        }
                        else
                        {
                            MessageBox.Show("Current round is not finished yet!\n");
                        }

                    }
                    else if (chooseRound_comboBox.SelectedIndex == 1)
                    {

                        if (round2.dataTable != null)
                        {
                            bool flag = false;
                            for (int i = 0; i < round2.dataTable.Rows.Count; i++)
                            {
                                if (string.IsNullOrEmpty(round2.dataTable.Rows[i].ItemArray[5].ToString()) || string.IsNullOrEmpty(round2.dataTable.Rows[i].ItemArray[6].ToString()))
                                {
                                    flag = true;
                                }
                            }
                            if (flag == false)
                            {
                                round2.getresultListR2(obj_round2);
                                teamCompetitionBindingSource1.DataSource = round2.resultListR2;
                                fastReports.editReport(obj_round2, resultForR2Report);
                            }
                            else
                            {
                                resultOfR1barButtonItem_ItemPress(sender, e);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with open edit mode fast report for result of current finished round!\n" + ex.Message);
            }
            
        }
        //
        // Live window components
        //        
        // Receive and present time, penalty from timekeeper when status is "ImZiel"
        public void receiveTimekeepingData()
        {
            try
            {
                if (json != null && json.Contains("\"Status\"" + ":" + " \"ImZiel\""))
                {
                    char[] spearator = { '/', ' ' };
                    Int32 count = 2;

                    // Using the Method 
                    String[] strlist = Label_FZ.Text.Split(spearator,
                           count, StringSplitOptions.None);
                    char[] charsToTrim = { '/' };

                    timekeepingPenaltyTextEdit.Text = strlist[0].Trim(charsToTrim);
                    timekeepingTimeTextEdit.Text = strlist[1].Trim(charsToTrim);

                    if (timetextEdit.Text == "" && (penaltiestextEdit.Text == "+" || penaltiestextEdit.Text == ""))
                    {
                        penaltiestextEdit.Text = string.Empty;
                        timetextEdit.BackColor = Color.White;
                        timetextEdit.ForeColor = Color.Black;

                        penaltiestextEdit.BackColor = Color.White;
                        penaltiestextEdit.ForeColor = Color.Black;

                        timekeepingTimeTextEdit.BackColor = Color.Green;
                        timekeepingTimeTextEdit.ForeColor = Color.White;

                        timekeepingPenaltyTextEdit.BackColor = Color.Green;
                        timekeepingPenaltyTextEdit.ForeColor = Color.White;
                        takingDataFromTimeKeeper();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with receiveTimekeepingData method!\n" + ex.Message);
            }
           
        }
        // Call receiveTimekeepingData function
        private void Label_FZ_TextChanged(object sender, EventArgs e)
        {
            receiveTimekeepingData();
        } 
      
        //private void timetextEdit_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    if (flag == true)
        //    {
        //        e.Handled = true;
        //        flag = false;
        //    }

        //    if (e.KeyChar == (char)Keys.Enter)
        //    {
        //        timekeepingTimeTextEdit.BackColor = Color.White;
        //        timekeepingTimeTextEdit.ForeColor = Color.Black;

        //        timekeepingPenaltyTextEdit.BackColor = Color.White;
        //        timekeepingPenaltyTextEdit.ForeColor = Color.Black;

        //        penaltiestextEdit.BackColor = Color.Green;
        //        penaltiestextEdit.ForeColor = Color.White;

        //        timetextEdit.BackColor = Color.Green;
        //        timetextEdit.ForeColor = Color.White;

        //        penaltycalculate();

        //        timetextEdit.Focus();
        //        timetextEdit.Select();
        //    }

        //    if (e.KeyChar == (char)Keys.Tab)
        //    {
        //        timetextEdit.Focus();
        //    }
        //}
        //private void penaltiestextEdit_KeyPress(object sender, KeyPressEventArgs e)
        //{
        //    if (flag == true)
        //    {
        //        e.Handled = true;
        //        flag = false;
        //    }

        //    if (e.KeyChar == (char)Keys.Enter)
        //    {
        //        timekeepingTimeTextEdit.BackColor = Color.White;
        //        timekeepingTimeTextEdit.ForeColor = Color.Black;

        //        timekeepingPenaltyTextEdit.BackColor = Color.White;
        //        timekeepingPenaltyTextEdit.ForeColor = Color.Black;

        //        penaltiestextEdit.BackColor = Color.Green;
        //        penaltiestextEdit.ForeColor = Color.White;

        //        timetextEdit.BackColor = Color.Green;
        //        timetextEdit.ForeColor = Color.White;

        //        penaltycalculate();
        //    }
        //}

        private void NationCup_FormClosed(object sender, FormClosedEventArgs e)
        {
            //ThreadReceive.Abort();
            Environment.Exit(0);
        }

       


        //private void NationCup_FormClosing(object sender, FormClosingEventArgs e)
        //{
        //    receivingUDPclient.Close();
        //}

        //private void penaltiestextEdit_EditValueChanged(object sender, EventArgs e)
        //{ 
        //    try
        //    {
        //        var tmpJSON = Newtonsoft.Json.JsonConvert.SerializeObject(obj, Formatting.Indented);
        //        if (File.Exists(Constants.sourceFilePath))
        //        {
        //            File.Delete(Constants.sourceFilePath);
        //            File.Create(Constants.sourceFilePath).Dispose();
        //        }
        //        File.WriteAllText(Constants.sourceFilePath, JToken.FromObject(tmpJSON).ToString());
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("Can not export Json file.\n" + ex.Message);
        //    }
        //}

        //private void penaltiestextEdit_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.Add || e.KeyCode == Keys.Y || e.KeyCode == Keys.J && e.KeyCode != Keys.Enter)
        //    {
        //        okButton_Click(sender, e);
        //    }
        //}
        //private void roundFinishButton_Click(object sender, EventArgs e)
        //{
        //    DropParticipants dropResultForRound1 = new DropParticipants();

        //    if (chooseRound_comboBox.SelectedIndex == 0)
        //    {
        //        dropResultForRound1.EliminateParticipant_Round1(obj);
        //        gridControl2.DataSource = null;
        //        gridControl2.MainView = round1_gridView;
        //        round1.create_datatable(obj);
        //        gridControl2.DataSource = round1.dataTable;

        //        gridControl3.Refresh();
        //        gridControl3.DataSource = null;
        //        gridControl3.DataSource = obj.Rank;
        //    }

        //    else if (chooseRound_comboBox.SelectedIndex == 1)
        //    {
        //        dropResultForRound1.EliminateParticipant_Round2(obj);
        //        gridControl2.DataSource = null;
        //        gridControl2.MainView = round2_gridView;
        //        round2.create_datatable(obj);
        //        gridControl2.DataSource = round2.dataTable;

        //        gridControl3.Refresh();
        //        gridControl3.DataSource = null;
        //        gridControl3.DataSource = obj.Rank;
        //    }
        //}

        // Call saveResultOfLiveWindow & setCnrIterator function, and update json
        //private void resulttextEdit_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode == Keys.Add || e.KeyCode == Keys.Y || e.KeyCode == Keys.J && e.KeyCode != Keys.Enter)
        //    {
        //        okButton_Click(sender, e);
        //    }
        //}
    }
}
