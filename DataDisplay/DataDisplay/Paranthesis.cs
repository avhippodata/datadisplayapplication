﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    public class Paranthesis
    {
        public void update_paranthesisR1(List<MemberDetails> memberDetails)
        {
            try
            {
                // Assign paranthesis if S status found 
                for (int i = 0; i < memberDetails.Count; i++)
                {
                    string tmp = memberDetails[i].Round1_Total;
                    if (memberDetails[i].Status_R1 == "S" && (tmp == "EL" || tmp == "DQ" || tmp == "RT" || tmp == "DNS"))
                    {
                        memberDetails[i].Round1_Total = "(" + memberDetails[i].Round1_Total + ")";
                        memberDetails[i].Round1_Time = "-";
                    }

                    else if (memberDetails[i].Status_R1 == "S" && (!tmp.Contains("(") || !tmp.Contains(")")) && (tmp != "EL" || tmp != "DQ" || tmp != "RT" || tmp != "DNS"))
                    {
                        memberDetails[i].Round1_Total = "(" + memberDetails[i].Round1_Total + ")";
                        memberDetails[i].Round1_Time = "(" + memberDetails[i].Round1_Time + ")";
                    }
                }

                // Remove paranthesis if W status found
                for (int i = 0; i < memberDetails.Count; i++)
                {
                    string tmp = memberDetails[i].Round1_Total;
                    string tmpTime = memberDetails[i].Round1_Time;
                    char[] charsToTrim = { '(', ')' };
                    if (memberDetails[i].Status_R1 == "W" && tmp != null)
                    {
                        memberDetails[i].Round1_Total = tmp.Trim(charsToTrim);
                        memberDetails[i].Round1_Time = tmpTime.Trim(charsToTrim);
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Problem with updating paranthesis for Round 1, Please check whether the status is assigned correctly!\n" + ex.Message);
            }
        }

        public void update_paranthesisR2(List<MemberDetails> memberDetails)
        {
            try
            {
                // Assign paranthesis if S status found 
                for (int i = 0; i < memberDetails.Count; i++)
                {
                    string tmp = memberDetails[i].Round2_Total;
                    if (memberDetails[i].Status_R2 == "S" && (tmp == "EL" || tmp == "DQ" || tmp == "RT" || tmp == "DNS"))
                    {
                        memberDetails[i].Round2_Total = "(" + memberDetails[i].Round2_Total + ")";
                        memberDetails[i].Round2_Time = "-";
                    }

                    else if (memberDetails[i].Status_R2 == "S" && (!tmp.Contains("(") || !tmp.Contains(")"))
                        && (tmp != "EL" || tmp != "DQ" || tmp != "RT" || tmp != "DNS"))
                    {
                        memberDetails[i].Round2_Total = "(" + memberDetails[i].Round2_Total + ")";
                        memberDetails[i].Round2_Time = "(" + memberDetails[i].Round2_Time + ")";
                    }
                }

                // Remove paranthesis if W status found
                for (int i = 0; i < memberDetails.Count; i++)
                {
                    string tmp = memberDetails[i].Round2_Total;
                    string tmpTime = memberDetails[i].Round2_Time;
                    char[] charsToTrim = { '(', ')' };
                    if (memberDetails[i].Status_R2 == "W" && tmp != null)
                    {
                        memberDetails[i].Round2_Total = tmp.Trim(charsToTrim);
                        memberDetails[i].Round2_Time = tmpTime.Trim(charsToTrim);
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Problem with updating paranthesis for Round 2, Please check whether the status is assigned correctly!\n" + ex.Message);
            }

           
        }
    }
}
