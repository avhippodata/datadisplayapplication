﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DataDisplay
{
    public class ParticipantStatusR1
    {
        public void status_update(List<MemberDetails> memberDetails, string cnr, string value, string current_time)
        {
            // Check if a member has null CNR, i.e., less than 4 member in a team
            bool flag = false;
            for (int i = 0; i < memberDetails.Count; i++)
            {
                if (memberDetails[i].CNR == null)
                {
                    flag = true;
                    break;
                }
            }

            // If there are less than 4 members in a team, assign them status as W
            if (flag)
            {
                for (int i = 0; i < memberDetails.Count; i++)
                {
                    if (memberDetails[i].CNR == cnr)
                    {
                        memberDetails[i].Status_R1 = "W";
                        return;
                    }
                }
            }

            // Check if the current member is not performaing or disqualified
            else if (value == "DNS" || value == "EL" || value == "DQ" || value == "RT")
            {
                for (int i = 0; i < memberDetails.Count; i++)
                {
                    if (memberDetails[i].CNR == cnr)
                    {
                        memberDetails[i].Status_R1 = "S";
                    }
                    else if (memberDetails[i].Round1_Total != null && (memberDetails[i].Round1_Total != "(DNS)" && memberDetails[i].Round1_Total != "(EL)" && memberDetails[i].Round1_Total != "(DQ)" && memberDetails[i].Round1_Total != "(RT)"))
                    {
                        memberDetails[i].Status_R1 = "W";
                    }
                }
            }

            else
            {
                // Check if all members have penalty null
                bool flag1 = false;
                for (int i = 0; i < memberDetails.Count; i++)
                {
                    if (memberDetails[i].Round1_Total != null)
                    {
                        flag1 = true;
                        break;
                    }
                }

                // If all members has null penalty, in that case assign current member status as W
                if (flag1 == false)
                {
                    for (int i = 0; i < memberDetails.Count; i++)
                    {
                        if (memberDetails[i].CNR == cnr)
                        {
                            memberDetails[i].Status_R1 = "W";
                            return;
                        }
                    }
                }

                // if three members has penalty null and the fourth person has penalty and is same as current member then assign current member status as W
                int null_count = 0;
                for (int i = 0; i < memberDetails.Count; i++)
                {
                    if (memberDetails[i].Round1_Total == null)
                        null_count++;
                }

                if (null_count == 3)
                {
                    for (int i = 0; i < memberDetails.Count; i++)
                    {
                        if (memberDetails[i].Round1_Total != null && memberDetails[i].CNR == cnr)
                        {
                            memberDetails[i].Status_R1 = "W";
                            return;
                        }
                    }
                }

                // Check if previous members has already disqualified or not performed
                bool flag2 = false;
                for (int i = 0; i < memberDetails.Count; i++)
                {
                    if (memberDetails[i].Round1_Total == "(DNS)" || memberDetails[i].Round1_Total == "(EL)" || memberDetails[i].Round1_Total == "(DQ)" || memberDetails[i].Round1_Total == "(RT)")
                    {
                        flag2 = true;
                        break;
                    }
                }

                // Current rider will be assigned W status
                if (flag2 == true)
                {
                    for (int i = 0; i < memberDetails.Count; i++)
                    {
                        if (memberDetails[i].CNR == cnr)
                        {
                            memberDetails[i].Status_R1 = "W";
                            return;
                        }
                    }
                }

                // Here we find the member with maximum penalty value   
                else
                {
                    int mx = Convert.ToInt32(value);
                    char[] charsToTrim = { '(', ')' };

                    for (int i = 0; i < memberDetails.Count; i++)
                    {
                        if (memberDetails[i].Round1_Total != null)
                        {
                            string tmp = memberDetails[i].Round1_Total;
                            memberDetails[i].Round1_Total = tmp.Trim(charsToTrim);
                        }

                        if (memberDetails[i].CNR != cnr && Convert.ToInt32(memberDetails[i].Round1_Total) > mx)
                            mx = Convert.ToInt32(memberDetails[i].Round1_Total);
                    }

                    // Check if a member has already maximum penalty
                    int count_mx = 0;
                    for (int i = 0; i < memberDetails.Count; i++)
                        if (Convert.ToInt32(memberDetails[i].Round1_Total) == mx)
                            count_mx += 1;

                    // If previously no member has maximum penalty then current member has maximum penalty
                    if (count_mx == 0 && mx == Convert.ToInt32(value))
                    {
                        for (int i = 0; i < memberDetails.Count; i++)
                        {
                            if (memberDetails[i].CNR == cnr)
                                memberDetails[i].Status_R1 = "S";
                            else if (memberDetails[i].Round1_Total != null)
                                memberDetails[i].Status_R1 = "W";
                        }
                    }

                    // If there are more than one member has maximum penalty
                    else if (count_mx >= 1)
                    {
                        // find the member who has taken more time
                        double mx_time = 0;
                        for (int i = 0; i < memberDetails.Count; i++)
                        {
                            if (memberDetails[i].Round1_Total != null && memberDetails[i].Round1_Time != null)
                            {
                                string _tmp = memberDetails[i].Round1_Time;
                                memberDetails[i].Round1_Time = _tmp.Trim(charsToTrim);
                                double tmp = Convert.ToDouble(memberDetails[i].Round1_Time);
                                if (Convert.ToInt32(memberDetails[i].Round1_Total) == mx)
                                {
                                    if (tmp > mx_time)
                                        mx_time = tmp;
                                }
                            }
                        }

                        // Check if the current member has maximum penalty and time among all other members
                        double tmp_current_time = Convert.ToDouble(current_time);
                        if (mx == Convert.ToInt32(value))
                        {
                            if (tmp_current_time > mx_time)
                                mx_time = tmp_current_time;
                        }

                        // Assign S to the member with maximum penalty and maximum time
                        for (int i = 0; i < memberDetails.Count; i++)
                        {
                            if (memberDetails[i].CNR == cnr)
                            {
                                if (Convert.ToInt32(value) == mx && Convert.ToDouble(current_time) == mx_time)
                                    memberDetails[i].Status_R1 = "S";
                                else
                                    memberDetails[i].Status_R1 = "W";
                            }

                            else if (memberDetails[i].Round1_Total != null || memberDetails[i].Round1_Time != null)
                            {
                                double tmp_time = Convert.ToDouble(memberDetails[i].Round1_Time);
                                int tmp_pen = Convert.ToInt32(memberDetails[i].Round1_Total);

                                if (tmp_pen == mx && tmp_time == mx_time)
                                    memberDetails[i].Status_R1 = "S";
                                else
                                    memberDetails[i].Status_R1 = "W";
                            }
                        }
                    }
                }        
            }
        }
    }
}