﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    public class PenaltyCompute
    {
        //ClassTimingData TimingData = new ClassTimingData();
        public int computePenalties(double time_taken)
        {
            int count = 0;
            try
            {
                double t_allow = NationCup.obj.Event.timeAllowedForRounds;

                if (time_taken <= t_allow)
                    return count;
                else
                {
                    while (time_taken > t_allow)
                    {
                        t_allow += 4;
                        count += 1;
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Problem with computing time penalty in PenaltyCompute.cs\n" + ex.Message);
            }

            return count;
        }
    }
}
