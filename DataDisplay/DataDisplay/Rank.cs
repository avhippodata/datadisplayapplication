﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    [Serializable]
    public class Ranking
    {
        public string teamName { get; set; }
        public int rank { get; set; }
        public int totalPenalties { get; set; }

        public void SetRank()
        {
            try
            {
                Constants.listOfRanking = new List<Ranking>();
                for (int i = 0; i < Constants.jsonInfo.Competition.Teams.Count(); i++)
                {
                    Ranking ranking = new Ranking();
                    ranking.teamName = Constants.jsonInfo.Competition.Teams[i].Name;
                    ranking.rank = 0;
                    ranking.totalPenalties = 0;
                    Constants.listOfRanking.Add(ranking);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with initializing rank data in Rank.cs\n" + ex.Message);
            }
        }
    }
}
