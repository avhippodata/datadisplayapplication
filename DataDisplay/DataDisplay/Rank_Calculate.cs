using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    class Rank_Calculate
    {
        public PenaltyData[] result;

        public void Rank_for_penalty(Serialization obj_round1)
        {
            try
            {
                // Collect data
                result = new PenaltyData[obj_round1.Teams.Count()];
                for (int i = 0; i < obj_round1.Teams.Count(); i++)
                {
                    PenaltyData tmp = new PenaltyData();
                    tmp.teamName = obj_round1.Teams[i].Name;
                    tmp.rank = 0;
                    tmp.score = obj_round1.Teams[i].Total_Round1_Penalty + obj_round1.Teams[i].Total_Round2_Penalty;
                    tmp.time_R1 = obj_round1.Teams[i].Total_Round1_Time;
                    tmp.time_R2 = obj_round1.Teams[i].Total_Round2_Time;
                    result[i] = tmp;
                }

                // Ascending order of teams based on penalty
                for (int i = 0; i < result.Count(); i++)
                {
                    int min_score = result[i].score;
                    int team_index = i;
                    for (int j = i; j < result.Count(); j++)
                    {
                        if (result[j].score < min_score)
                        {
                            min_score = result[j].score;
                            team_index = j;
                        }
                    }
                    PenaltyData tmp = new PenaltyData();
                    tmp = result[team_index];
                    result[team_index] = result[i];
                    result[i] = tmp;
                }

                // Ascending order of teams based on penalty with time
                for (int i = 0; i < result.Count(); i++)
                {
                    int current_score = result[i].score;
                    double min_time = result[i].time_R1;
                    int team_index = i;
                    int j = i + 1;

                    while (j < result.Count() && result[j].score == current_score)
                    {
                        if (result[j].time_R1 < min_time)
                        {
                            min_time = result[j].time_R1;
                            team_index = j;
                        }

                        j++;
                    }

                    PenaltyData tmp = new PenaltyData();
                    tmp = result[team_index];
                    result[team_index] = result[i];
                    result[i] = tmp;
                }

                // Assigning ranks to teams (teams with same penalty have same rank but the order is sorted by time)
                int position = 1;
                for (int i = 0; i < result.Count();)
                {
                    result[i].rank = position;

                    int it = i; // Temporary iterator without changing current index value

                    // If the team have same penalties
                    while (it + 1 < result.Count() && result[it].score == result[it + 1].score)
                    {
                        it = it + 1;
                        result[it].rank = position;
                    }

                    if (i != it)
                    {
                        i = it + 1;
                        position = i + 1;
                    }

                    else if (it == i)
                    {
                        i++;
                        position++;
                    }
                }

                // A list of teams with rank in the order of ranking
                for (int i = 0; i < result.Count(); i++)
                {
                    obj_round1.Rank[i].teamName = result[i].teamName;
                    obj_round1.Rank[i].rank = result[i].rank;
                    obj_round1.Rank[i].totalPenalties = result[i].score;
                }

                // A list of teams with ranks in the order of teams
                for (int i = 0; i < obj_round1.Teams.Count(); i++)
                {
                    for (int j = 0; j < result.Count(); j++)
                    {
                        if (obj_round1.Teams[i].Name == result[j].teamName)
                        {
                            obj_round1.Teams[i].Rank = result[j].rank;
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Problem with calculating rank having [penalty only]\n" + ex.Message);
            }

        }

        public void Rank_for_pen_time(Serialization obj_round2)
        {
            try
            {
                // Receive data from the participating teams
                result = new PenaltyData[obj_round2.Event.noOfteamsForNextRound];
                for (int i = 0; i < obj_round2.Event.noOfteamsForNextRound; i++)
                {
                    PenaltyData tmp = new PenaltyData();
                    tmp.teamName = obj_round2.Teams[i].Name;
                    tmp.rank = 0;
                    tmp.score = obj_round2.Teams[i].Total_Round1_Penalty + obj_round2.Teams[i].Total_Round2_Penalty;
                    tmp.time_R1 = obj_round2.Teams[i].Total_Round1_Time;
                    tmp.time_R2 = obj_round2.Teams[i].Total_Round2_Time;
                    result[i] = tmp;
                }

                // Ascending order of teams based on penalty
                for (int i = 0; i < result.Count(); i++)
                {
                    int min_score = result[i].score;
                    int team_index = i;
                    for (int j = i; j < result.Count(); j++)
                    {
                        if (result[j].score < min_score)
                        {
                            min_score = result[j].score;
                            team_index = j;
                        }
                    }
                    PenaltyData tmp = new PenaltyData();
                    tmp = result[team_index];
                    result[team_index] = result[i];
                    result[i] = tmp;
                }

                // Ascending order of teams based on penalty with time
                for (int i = 0; i < result.Count(); i++)
                {
                    int current_score = result[i].score;
                    double min_time = result[i].time_R2;
                    int team_index = i;
                    int j = i + 1;

                    while (j < result.Count() && result[j].score == current_score)
                    {
                        if (result[j].time_R2 < min_time)
                        {
                            min_time = result[j].time_R2;
                            team_index = j;
                        }

                        j++;
                    }

                    PenaltyData tmp = new PenaltyData();
                    tmp = result[team_index];
                    result[team_index] = result[i];
                    result[i] = tmp;
                }

                // Assigning ranks to teams based on time and penalty both
                int position = 1;
                for (int i = 0; i < result.Count();)
                {
                    result[i].rank = position;
                    int it = i; // Temporary iterator without changing current index value

                    // If the team have same penalties and time as well (practically not possible :D)
                    while (it + 1 < result.Count() && result[it].score == result[it + 1].score && result[it].time_R2 == result[it + 1].time_R2)
                    {
                        it = it + 1;
                        result[it].rank = position;
                    }

                    if (i != it)
                    {
                        i = it + 1;
                        position = i + 1;
                    }

                    else if (it == i)
                    {
                        i++;
                        position++;
                    }
                }

                // Assign ranks to the Obj.Rank, the rank order may differ
                for (int i = 0; i < result.Count(); i++)
                {
                    obj_round2.Rank[i].teamName = result[i].teamName;
                    obj_round2.Rank[i].rank = result[i].rank;
                    obj_round2.Rank[i].totalPenalties = result[i].score;
                }

                // Asiign rank to the teams which are not performing in R2
                for (int i = obj_round2.Event.noOfteamsForNextRound; i < obj_round2.Rank.Count(); i++)
                {
                    obj_round2.Rank[i].rank = i + 1;
                }

                // A list of teams with rank in the order of teams
                for (int i = 0; i < obj_round2.Rank.Count(); i++)
                {
                    for (int j = 0; j < obj_round2.Teams.Count(); j++)
                    {
                        if (obj_round2.Rank[i].teamName == obj_round2.Teams[j].Name)
                        {
                            obj_round2.Teams[j].Rank = obj_round2.Rank[i].rank;
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Problem with calculating rank having both time and penalty\n" + ex.Message);
            }
        }

        public void Rank_for_JumpOff(Serialization obj)
        {
            result = new PenaltyData[obj.Teams.Count()];
            for (int i = 0; i < obj.Teams.Count(); i++)
            {
                PenaltyData tmp = new PenaltyData();
                tmp.teamName = obj.Teams[i].Name;
                tmp.rank = 0;
                tmp.score = obj.Teams[i].Total_Round1_Penalty + obj.Teams[i].Total_Round2_Penalty;
                tmp.time_R2 = obj.Teams[i].Total_Round2_Time;
                result[i] = tmp;
            }

            // Ascending order of teams
            for (int i = 0; i < result.Count(); i++)
            {
                int min_score = result[i].score;
                int team_index = i;
                for (int j = i; j < result.Count(); j++)
                {
                    if (result[j].score < min_score)
                    {
                        min_score = result[j].score;
                        team_index = j;
                    }
                }
                PenaltyData tmp = new PenaltyData();
                tmp = result[team_index];
                result[team_index] = result[i];
                result[i] = tmp;
            }

            // Assigning ranks to teams i.e. teams with same penaties must have same rank to decide participating teams for jump off
            int position = 1;
            for (int i = 0; i < result.Count();)
            {
                result[i].rank = position;

                int it = i; // Temporary iterator without changing current index value

                // If the team have same penalties
                while (it + 1 < result.Count() && result[it].score == result[it + 1].score)
                {
                    it = it + 1;
                    result[it].rank = position;
                }

                if (i != it)
                {
                    i = it + 1;
                    position++;
                }

                else if (it == i)
                {
                    i++;
                    position++;
                }
            }

            // A list of teams with rank in the order of ranking
            for (int i = 0; i < result.Count(); i++)
            {
                obj.Rank[i].teamName = result[i].teamName;
                obj.Rank[i].rank = result[i].rank;
            }

            // A list of teams with ranks in the order of teams
            for (int i = 0; i < obj.Teams.Count(); i++)
            {
                for (int j = 0; j < result.Count(); j++)
                {
                    if (obj.Teams[i].Name == result[j].teamName)
                    {
                        obj.Teams[i].Rank = result[j].rank;
                    }
                }
            }
        }

        //public void Rank_for_pen_time(Serialization obj)
        //{
        //    Rank_for_penalty(obj);

        //    // Assigning ranks to teams
        //    int position = 1;
        //    for (int i = 0; i < result.Count();)
        //    {
        //        int it = i; // Temporary iterator without changing current index value
        //        float min_time = result[it].time;
        //        int team_index = it;

        //        // If the team have same penalties then retrieve the team with lowest time
        //        while (it + 1 < result.Count() && result[it].score == result[it + 1].score)
        //        {
        //            if (result[it + 1].time < min_time)
        //            {
        //                min_time = result[it + 1].time;
        //                team_index = it + 1;
        //            }
        //            it = it + 1;
        //        }

        //        // Move the team up-to current index 
        //        if (team_index != i)
        //        {
        //            PenaltyData tmp = new PenaltyData();
        //            tmp = result[team_index];
        //            result[team_index] = result[i];
        //            result[i] = tmp;
        //        }

        //        // Assign rank to current indexed team
        //        result[i].rank = position;
        //        i = i + 1;
        //        position++;
        //    }

        //    // A list of teams with rank in the order of ranking
        //    for (int i = 0; i < result.Count(); i++)
        //    {
        //        //MessageBox.Show(obj.Rank[i].teamName);
        //        obj.Rank[i].teamName = result[i].teamName;
        //        obj.Rank[i].rank = result[i].rank;
        //        obj.Rank[i].totalPenalties = result[i].score;
        //    }

        //    // A list of teams with rank in the order of teams
        //    for (int i = 0; i < obj.Teams.Count(); i++)
        //    {
        //        for (int j = 0; j < result.Count(); j++)
        //        {
        //            if (obj.Teams[i].Name == result[j].teamName)
        //            {
        //                obj.Teams[i].Rank = result[j].rank;
        //            }
        //        }
        //    }
        //}
    }
}
