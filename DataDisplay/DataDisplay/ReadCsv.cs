﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace DataDisplay
{
    public class ReadCsv
    {
        public void ReadingDataFromCsv(string sourceFilePath)
        {
            try
            {
                Constants.lines = null;
                Constants.lines = File.ReadAllLines(Constants.sourceFilePath);
                for (int i = 0; i < Constants.lines.Length; i++)
                {
                    List<string> tmp = new List<string>();
                    foreach (string word in Constants.lines[i].Split(','))
                    {
                        string _word = word.Replace("\"", "");
                        Console.WriteLine(_word);
                        tmp.Add(_word);
                    }
                    Constants.reportInfo.Add(tmp);
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Problem while reading all lines from csv(STA)!\n" + ex.Message);
            }

            try
            {
                // reading first line of STA file
                Constants.firstLine = Constants.lines[0].Split(',');
                Array.Reverse(Constants.firstLine, 0, Constants.firstLine.Length);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem while reading first line of csv(STA)\n" + ex.Message);
            }

            try
            {
                // total number of athlete-horse into lists
                Constants.allAthelete = new List<Athlete>();
                Constants.allHorse = new List<Horse>();
                Constants.numberOfLoop = Convert.ToInt16(Constants.firstLine.GetValue(1));
                for (int i = 26, j = 0; j < Constants.numberOfLoop; j++, i += 29)      // loop should go till 954
                {
                    //startPosition += 1;

                    Athlete _athlete = new Athlete();
                    _athlete.SetAthleteData(i);
                    Constants.allAthelete.Add(_athlete);

                    Horse _horse = new Horse();
                    _horse.SetHorseData(i);
                    Constants.allHorse.Add(_horse);

                    Constants.lastIndexOfCsv = i + 29; // It will store the value when last block of the athlete-horse data ends
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem while reading athlete and horse data from csv(STA)\n" + ex.Message);
            }

            // collect all individual athlete-horse couple
            Competitor competitor = new Competitor();
            competitor.setCompetitorInfo();

            try
            {
                // reading json data from STA files
                Constants.jsonString = null;
                for (int i = Constants.lastIndexOfCsv + 1; i < Constants.lines.Length; i++)
                {
                    if (!Constants.lines[i].Contains("<json>"))
                        continue;
                    else
                    {
                        for (int j = i + 1; j < Constants.lines.Length; j++)
                        {
                            if (Constants.lines[j].Contains("Competitions"))
                            {
                                Constants.jsonString += "\"Competition\":{";
                            }
                            else
                            {
                                Constants.jsonString += Constants.lines[j];
                            }
                        }
                        break;
                    }
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show("Problem while reading JSON part in STA file!\n" + ex.Message);
            }

            // deserialize collected json data
            Deserialization deserialization = new Deserialization();
            deserialization.GetJsonData();

            // parse deserialize data into teams
            TeamCompetition teamCompetition = new TeamCompetition();
            teamCompetition.SetTeamInfo();

            // Create DataTableR1
            DataTableR1 dataTableR1 = new DataTableR1();
            dataTableR1.create_DataTableR1();

            // parse deserialize data into teams
            Ranking ranking = new Ranking();
            ranking.SetRank();

            Results results = new Results();
            results.ResultR1();
            results.ResultR2();
            results.ResultFinal();
        }
    }
}
