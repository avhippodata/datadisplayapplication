﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace DataDisplay
{
    [Serializable]
    public class ReportData
    {
        public int competitionNumber { get; set; }
        public string competitionName { get; set; }
        public string headerText1 { get; set; }
        public string headerText2 { get; set; }
        public string headerText3 { get; set; }
        public string headerText4 { get; set; }
        public string headerText5 { get; set; }
        public string footerText1 { get; set; }
        public string footerText2 { get; set; }
        public string footerText3 { get; set; }
        public string footerText4 { get; set; }
        public string footerText5 { get; set; }
        public string judgesPosition { get; set; }
        public double prizeMoneyTotal { get; set; }

        public void GetCompetitionData()
        {
            try
            {
                //competition no and name
                competitionNumber = Convert.ToInt16(Constants.reportInfo[0][0]);
                competitionName = Constants.jsonInfo.Competition.Name;

                //headers
                int tmp = 0;
                for (int i = 0; i < Constants.reportInfo[4].Count - 1; i++)
                {
                    headerText1 += Constants.reportInfo[4][i] + ",";
                    tmp = i + 1;
                }
                headerText1 += Constants.reportInfo[4][tmp];
                tmp = 0;

                for (int i = 0; i < Constants.reportInfo[5].Count - 1; i++)
                {
                    headerText2 += Constants.reportInfo[5][i] + ",";
                    tmp = i + 1;
                }
                headerText2 += Constants.reportInfo[5][tmp];
                tmp = 0;

                for (int i = 0; i < Constants.reportInfo[6].Count - 1; i++)
                {
                    headerText3 += Constants.reportInfo[6][i] + ",";
                    tmp = i + 1;
                }
                headerText3 += Constants.reportInfo[6][tmp];
                tmp = 0;

                for (int i = 0; i < Constants.reportInfo[7].Count - 1; i++)
                {
                    headerText4 += Constants.reportInfo[7][i] + ",";
                    tmp = i + 1;
                }
                headerText4 += Constants.reportInfo[7][tmp];
                tmp = 0;

                for (int i = 0; i < Constants.reportInfo[8].Count - 1; i++)
                {
                    headerText5 += Constants.reportInfo[8][i] + ",";
                    tmp = i + 1;
                }
                headerText5 += Constants.reportInfo[8][tmp];
                tmp = 0;

                // footers====================================================          
                for (int i = 0; i < Constants.reportInfo[9].Count - 1; i++)
                {
                    footerText1 += Constants.reportInfo[9][i] + ",";
                    tmp = i + 1;
                }
                footerText1 += Constants.reportInfo[9][tmp];
                tmp = 0;

                for (int i = 0; i < Constants.reportInfo[10].Count - 1; i++)
                {
                    footerText2 += Constants.reportInfo[10][i] + ",";
                    tmp = i + 1;
                }
                footerText2 += Constants.reportInfo[10][tmp];
                tmp = 0;

                for (int i = 0; i < Constants.reportInfo[11].Count - 1; i++)
                {
                    footerText3 += Constants.reportInfo[11][i] + ",";
                    tmp = i + 1;
                }
                footerText3 += Constants.reportInfo[11][tmp];
                tmp = 0;

                for (int i = 0; i < Constants.reportInfo[12].Count - 1; i++)
                {
                    footerText4 += Constants.reportInfo[12][i] + ",";
                    tmp = i + 1;
                }
                footerText4 += Constants.reportInfo[12][tmp];
                tmp = 0;

                for (int i = 0; i < Constants.reportInfo[13].Count - 1; i++)
                {
                    footerText5 += Constants.reportInfo[13][i] + ",";
                    tmp = i + 1;
                }
                footerText5 += Constants.reportInfo[13][tmp];
                tmp = 0;

                judgesPosition = Constants.reportInfo[23][0];
                prizeMoneyTotal = Convert.ToDouble(Constants.reportInfo[24][0]);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Problem with getting competition data from reportInfo (STA) in ReportData.cs!\n" + ex.Message);
            }
        }
    }
        
}
