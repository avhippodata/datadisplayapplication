﻿using DevExpress.XtraGrid.Views.Base;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    public class Round1
    {
        public List<string[]> g1;
        public List<string[]> g2;
        public List<string[]> g3;
        public List<string[]> g4;

        public List<TeamCompetition> startListR1;
        public List<TeamCompetition> resultListR1;

        public DataTable dataTable;
        Rank_Calculate rank_Calculate = new Rank_Calculate();
       
        public void create_datatable(Serialization obj_round1)
        {
            try
            {
                dataTable = new DataTable();
                g1 = new List<string[]>();
                g2 = new List<string[]>();
                g3 = new List<string[]>();
                g4 = new List<string[]>();

                for (int i = 0; i < obj_round1.Teams.Count(); i++)
                {
                    bool flag = false;
                    for (int m = 0; m < obj_round1.Teams[i].MemberDetails.Count; m++)
                    {
                        if (obj_round1.Teams[i].MemberDetails[m].CNR == null)
                            flag = true;
                    }

                    if (!flag)
                    {
                        g1.Add(new string[] { Convert.ToString(obj_round1.Teams[i].No), obj_round1.Teams[i].IocCode, obj_round1.Teams[i].MemberDetails[0].CNR, obj_round1.Teams[i].MemberDetails[0].HorseName, obj_round1.Teams[i].MemberDetails[0].RiderName, obj_round1.Teams[i].MemberDetails[0].Round1_Total });

                        g2.Add(new string[] { Convert.ToString(obj_round1.Teams[i].No), obj_round1.Teams[i].IocCode, obj_round1.Teams[i].MemberDetails[1].CNR, obj_round1.Teams[i].MemberDetails[1].HorseName, obj_round1.Teams[i].MemberDetails[1].RiderName, obj_round1.Teams[i].MemberDetails[1].Round1_Total });

                        g3.Add(new string[] { Convert.ToString(obj_round1.Teams[i].No), obj_round1.Teams[i].IocCode, obj_round1.Teams[i].MemberDetails[2].CNR, obj_round1.Teams[i].MemberDetails[2].HorseName, obj_round1.Teams[i].MemberDetails[2].RiderName, obj_round1.Teams[i].MemberDetails[2].Round1_Total });

                        g4.Add(new string[] { Convert.ToString(obj_round1.Teams[i].No), obj_round1.Teams[i].IocCode, obj_round1.Teams[i].MemberDetails[3].CNR, obj_round1.Teams[i].MemberDetails[3].HorseName, obj_round1.Teams[i].MemberDetails[3].RiderName, obj_round1.Teams[i].MemberDetails[3].Round1_Total });
                    }

                    else
                    {
                        g2.Add(new string[] { Convert.ToString(obj_round1.Teams[i].No), obj_round1.Teams[i].IocCode, obj_round1.Teams[i].MemberDetails[1].CNR, obj_round1.Teams[i].MemberDetails[1].HorseName, obj_round1.Teams[i].MemberDetails[1].RiderName, obj_round1.Teams[i].MemberDetails[1].Round1_Total });

                        g3.Add(new string[] { Convert.ToString(obj_round1.Teams[i].No), obj_round1.Teams[i].IocCode, obj_round1.Teams[i].MemberDetails[2].CNR, obj_round1.Teams[i].MemberDetails[2].HorseName, obj_round1.Teams[i].MemberDetails[2].RiderName, obj_round1.Teams[i].MemberDetails[2].Round1_Total });

                        g4.Add(new string[] { Convert.ToString(obj_round1.Teams[i].No), obj_round1.Teams[i].IocCode, obj_round1.Teams[i].MemberDetails[3].CNR, obj_round1.Teams[i].MemberDetails[3].HorseName, obj_round1.Teams[i].MemberDetails[3].RiderName, obj_round1.Teams[i].MemberDetails[3].Round1_Total });
                    }
                }

                int it = 0;
                dataTable.Columns.Add("Position");
                //dataTable.Columns.Add("Team No");
                dataTable.Columns.Add("IOC");
                dataTable.Columns.Add("CNR");
                dataTable.Columns.Add("Horse Name");
                dataTable.Columns.Add("Rider Name");
                dataTable.Columns.Add("Round1_Penalty");
                for (int i = 0; i < g1.Count;)
                {
                    it = it + 1;
                    dataTable.Rows.Add(it, g1[i][1], g1[i][2], g1[i][3], g1[i][4], g1[i][5]);
                    i++;
                }

                for (int i = 0; i < g2.Count;)
                {
                    it = it + 1;
                    dataTable.Rows.Add(it, g2[i][1], g2[i][2], g2[i][3], g2[i][4], g2[i][5]);
                    i++;
                }

                for (int i = 0; i < g3.Count;)
                {
                    it = it + 1;
                    dataTable.Rows.Add(it, g3[i][1], g3[i][2], g3[i][3], g3[i][4], g3[i][5]);
                    i++;
                }

                for (int i = 0; i < g4.Count;)
                {
                    it = it + 1;
                    dataTable.Rows.Add(it, g4[i][1], g4[i][2], g4[i][3], g4[i][4], g4[i][5]);
                    i++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with creating datatable for round 1, please check obj or column fields of datatable!\n" + ex.Message);
            }
        }

        public void create_storder(Serialization obj_round1, List<DataTableR1> dataTableR1)
        {
            NpwStorderRankingR1 npw_storder = new NpwStorderRankingR1();
            StringBuilder csvcontent = new StringBuilder();
            for (int i = 0; i < obj_round1.Teams.Count(); i++)
            {
                int it = 0;
                while (it < obj_round1.Teams.Count && dataTableR1[i].IOC != obj_round1.Teams[it].IocCode)
                {
                    it++;
                }
                if (dataTableR1[i].IOC == obj_round1.Teams[it].IocCode)
                {
                    string csv = "";
                    int order = 0;
                    csv = npw_storder.team_info(csv, obj_round1, it, order);
                    csvcontent.AppendLine(csv);

                    for (int j = 0; j < obj_round1.Teams[it].MemberDetails.Count(); j++)
                    {
                        csv = null;
                        order += 1;
                        csv = npw_storder.member_info(csv, obj_round1, it, j, order);
                        csvcontent.AppendLine(csv);
                    }
                }
            }
            if (NationCup.obj_internal.GeneralSetting.showFolderPath != null)
            {
                string csvpath = Path.Combine(NationCup.obj_internal.GeneralSetting.showFolderPath, "storder.npw");
                File.Create(csvpath).Dispose();
                File.AppendAllText(csvpath, csvcontent.ToString());
            }
            else
            {
                MessageBox.Show("Please provide show folder path in general settings!\n");
            }

        }

        public void create_starter(Serialization obj_round1, int no, string cnr)
        {
            try
            {
                NpwStarterErgebnisR1 npw_starter = new NpwStarterErgebnisR1();
                StringBuilder csvcontent = new StringBuilder();

                string csv = "";
                int order = 0;

                for (int i = 0; i < obj_round1.Teams.Count(); i++)
                {
                    if (no == obj_round1.Teams[i].No)
                    {
                        csv = npw_starter.team_info(csv, obj_round1, i, order);
                        csvcontent.AppendLine(csv);

                        for (int k = 0; k < obj_round1.Teams[i].MemberDetails.Count(); k++)
                        {
                            csv = null;
                            order += 1;

                            if (cnr == obj_round1.Teams[i].MemberDetails[k].CNR)
                                csv = npw_starter.current_member_info(csv, obj_round1, i, k, order);

                            else
                                csv = npw_starter.member_info(csv, obj_round1, i, k, order);

                            csvcontent.AppendLine(csv);
                        }

                        if (NationCup.obj_internal.GeneralSetting.showFolderPath != null)
                        {
                            string csvpath = Path.Combine(NationCup.obj_internal.GeneralSetting.showFolderPath, "starter.npw");
                            File.Create(csvpath).Dispose();
                            File.AppendAllText(csvpath, csvcontent.ToString());
                            Console.WriteLine(csvcontent.ToString());
                        }
                        else
                        {
                            MessageBox.Show("Please provide show folder path in general settings!\n");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with creating starter.npw in round 1!\n" + ex.Message);
            }
            
        }
        
        public void create_ergebnis(Serialization obj_round1, int no, string cnr)
        {
            try
            {
                NpwStarterErgebnisR1 npw_ergebnis = new NpwStarterErgebnisR1();
                StringBuilder csvcontent = new StringBuilder();

                string csv = "";
                int order = 0;
                for (int i = 0; i < obj_round1.Teams.Count(); i++)
                {
                    if (no == obj_round1.Teams[i].No)
                    {
                        //ParticipantStatus participantStatus = new ParticipantStatus();

                        csv = npw_ergebnis.team_info(csv, obj_round1, i, order);
                        csvcontent.AppendLine(csv);

                        for (int k = 0; k < obj_round1.Teams[i].MemberDetails.Count(); k++)
                        {
                            csv = null;
                            order += 1;

                            if (cnr == obj_round1.Teams[i].MemberDetails[k].CNR)
                            {
                                //participantStatus.status_update(obj.Teams[i].MemberDetails, obj.Teams[i].MemberDetails[k].Round1_Penalty);
                                csv = npw_ergebnis.current_member_info(csv, obj_round1, i, k, order);
                            }

                            else
                                csv = npw_ergebnis.member_info(csv, obj_round1, i, k, order);

                            csvcontent.AppendLine(csv);
                        }

                        if (NationCup.obj_internal.GeneralSetting.showFolderPath != null)
                        {
                            string csvpath = Path.Combine(NationCup.obj_internal.GeneralSetting.showFolderPath, "ergebnis.npw");
                            File.Create(csvpath).Dispose();
                            File.AppendAllText(csvpath, csvcontent.ToString());
                            Console.WriteLine(csvcontent.ToString());
                        }
                        else
                        {
                            MessageBox.Show("Please provide show folder path in general settings!\n");
                        }     
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with creating ergebnis.npw in round 1!\n" + ex.Message);
            }
           
        }

        public void create_ranking(Serialization obj_round1)
        {
            try
            {
                rank_Calculate.Rank_for_penalty(obj_round1);
                NpwStorderRankingR1 npw_ranking = new NpwStorderRankingR1();
                StringBuilder csvcontent = new StringBuilder();

                for (int i = 0; i < rank_Calculate.result.Count(); i++)
                {
                    string csv = "";
                    int order = 0;
                    for (int j = 0; j < obj_round1.Teams.Count; j++)
                    {
                        if (rank_Calculate.result[i].teamName == obj_round1.Teams[j].Name)
                        {
                            //obj.Rank[j].rank = result[i].rank; TODO: new data structure has to be created to find correct ranking, the rank has changed because of index mismatch
                            csv = npw_ranking.team_info(csv, obj_round1, j, order);
                            csvcontent.AppendLine(csv);

                            for (int k = 0; k < obj_round1.Teams[j].MemberDetails.Count(); k++)
                            {
                                csv = null;
                                order += 1;
                                csv = npw_ranking.member_info(csv, obj_round1, j, k, order);
                                csvcontent.AppendLine(csv);
                            }
                        }
                    }
                }

                if (NationCup.obj_internal.GeneralSetting.showFolderPath != null)
                {
                    string csvpath = Path.Combine(NationCup.obj_internal.GeneralSetting.showFolderPath, "ranking.npw");
                    File.Create(csvpath).Dispose();
                    File.AppendAllText(csvpath, csvcontent.ToString());
                }
                else
                {
                    MessageBox.Show("Please provide show folder path in general settings!\n");
                }   
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with creating ranking.npw in round1!\n" + ex.Message);
            }           
        }

        public void getStartListR1(Serialization obj_round1, List<DataTableR1> dataTableR1)
        {
            startListR1 = new List<TeamCompetition>();

            for (int i = 0; i < obj_round1.Teams.Count; i++)
            {
                int it = 0;
                while (it < obj_round1.Teams.Count && dataTableR1[i].IOC != obj_round1.Teams[it].IocCode)
                {
                    it++;
                }
                if (dataTableR1[i].IOC == obj_round1.Teams[it].IocCode)
                {
                    startListR1.Add(obj_round1.Teams[it]);
                }
            }
            int count = 0;
            for (int i = 0; i < startListR1.Count(); i++)
            {
                count = count + 1;
                startListR1[i].position = count;
            }
        }

        public void getresultListR1(Serialization obj_round1)
        {
            resultListR1 = new List<TeamCompetition>();
            for (int i = 0; i < obj_round1.Rank.Count; i++)
            {
                int it = 0;
                while (it < obj_round1.Teams.Count && obj_round1.Rank[i].teamName != obj_round1.Teams[it].Name)
                {
                    it++;
                }
                if (obj_round1.Rank[i].teamName == obj_round1.Teams[it].Name)
                {
                    resultListR1.Add(obj_round1.Teams[it]);
                    obj_round1.ResultR1.Add(obj_round1.Teams[it]);
                }
            }
            int count = 0;
            for (int i = 0; i < resultListR1.Count(); i++)
            {
                count = count + 1;
                resultListR1[i].position = count;
            }
        }
    }

    public class Round1_database
    {
        public int pos { get; set; }
        public string CNR { get; set; }
        public DateTime time { get; set; }
        public string horse { get; set; }
        public string riderName { get; set; }
        public string nation { get; set; }
        public string AbstammungOhneBesitzer { get; set; }
        //public string PferdBesitzer { get; set; }
        public List<Round1_database> list_round1;
        public List<Round1_database> list_round2;


        public string getHorseData(Serialization obj_round1, int cnr)
        {
            string _s = "";
            for (int i = 0; i < obj_round1.Competitors.Count; i++)
            {
                if (obj_round1.Competitors[i].horse.cNo == cnr)
                {
                    // Farbe
                    if (!string.IsNullOrEmpty(obj_round1.Competitors[i].horse.color))
                    {
                        if (!string.IsNullOrEmpty(_s))
                            _s = _s + " / ";
                        _s = _s + obj_round1.Competitors[i].horse.color;
                    }

                    // Alter
                    if (DateAndTime.Now.Year > Convert.ToInt32(obj_round1.Competitors[i].horse.yearOfBirth) & Convert.ToInt32(obj_round1.Competitors[i].horse.yearOfBirth) > 0)
                    {
                        if (!string.IsNullOrEmpty(_s))
                            _s = _s + " / ";
                        _s = _s + (DateAndTime.Now.Year - Convert.ToInt32(obj_round1.Competitors[i].horse.yearOfBirth)).ToString() + "y";
                    }

                    // Geschlecht
                    if (!string.IsNullOrEmpty(obj_round1.Competitors[i].horse.sex))
                    {
                        if (!string.IsNullOrEmpty(_s))
                            _s = _s + " / ";
                        _s = _s + obj_round1.Competitors[i].horse.sex;
                    }

                    // Vater
                    if (!string.IsNullOrEmpty(obj_round1.Competitors[i].horse.fatherName))
                    {
                        if (!string.IsNullOrEmpty(_s))
                            _s = _s + " / ";
                        _s = _s + obj_round1.Competitors[i].horse.fatherName;
                    }

                    // MutterVater
                    if (!string.IsNullOrEmpty(obj_round1.Competitors[i].horse.motherFatherName))
                    {
                        if (!string.IsNullOrEmpty(_s))
                            _s = _s + " / ";
                        _s = _s + obj_round1.Competitors[i].horse.motherFatherName;
                    }

                    // nation
                    if (!string.IsNullOrEmpty(obj_round1.Competitors[i].horse.nation))
                    {
                        if (!string.IsNullOrEmpty(_s))
                            _s = _s + " / ";
                        _s = _s + obj_round1.Competitors[i].horse.breed;
                    }

                    // FEI ID
                    if (!string.IsNullOrEmpty(obj_round1.Competitors[i].horse.feiId))
                    {
                        if (!string.IsNullOrEmpty(_s))
                            _s = _s + " / ";
                        _s = _s + obj_round1.Competitors[i].horse.feiId;
                    }

                    // Besitzername
                    if (!string.IsNullOrEmpty(obj_round1.Competitors[i].horse.onwer))
                    {
                        if (!string.IsNullOrEmpty(_s))
                            _s = _s + " / ";
                        _s = _s + obj_round1.Competitors[i].horse.onwer;
                    }
                    break;
                }             
            }
            return _s;

        }

        public void GetInfoR1(List<DataTableR1> dataTableR1, Serialization obj_round1)
        {
            int count = 1;
            list_round1 = new List<Round1_database>();
            for (int i = 0; i < dataTableR1.Count; i++)
            {             
                Round1_database member = new Round1_database();
                member.pos = count++;
                member.CNR = dataTableR1[i].CNR;
                member.time = DateTime.Now;
                member.horse = dataTableR1[i].HorseName;
                member.riderName = dataTableR1[i].RiderName;
                member.nation = dataTableR1[i].IOC;
                member.AbstammungOhneBesitzer = getHorseData(obj_round1, Convert.ToInt32(member.CNR));
                //member.PferdBesitzer = getHorseData(obj, Convert.ToInt32(member.CNR));
                list_round1.Add(member);
            }
        }


        public void GetInfoR2(DataTable d1, Serialization obj_round2)
        {
            int count = 1;
            list_round2 = new List<Round1_database>();
            for (int i = 0; i < d1.Rows.Count; i++)
            {
                Round1_database member = new Round1_database();
                member.pos = count++;
                member.CNR = d1.Rows[i].ItemArray[2].ToString();
                member.time = DateTime.Now;
                member.horse = d1.Rows[i].ItemArray[3].ToString();
                member.riderName = d1.Rows[i].ItemArray[4].ToString();
                member.nation = d1.Rows[i].ItemArray[1].ToString();
                member.AbstammungOhneBesitzer = getHorseData(obj_round2, Convert.ToInt32(member.CNR));
                //member.PferdBesitzer = getHorseData(obj, Convert.ToInt32(member.CNR));
                list_round2.Add(member);
            }
        }
    }
}
