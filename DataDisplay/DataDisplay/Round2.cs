﻿using DevExpress.XtraGrid.Views.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{

    public class Round2
    {
        public List<string[]> g1;
        public List<string[]> g2;
        public List<string[]> g3;
        public List<string[]> g4;
        public List<TeamCompetition> startListR2;
        public List<TeamCompetition> resultListR2;
        public DataTable dataTable;
        Rank_Calculate rank_Calculate = new Rank_Calculate();

        public void create_datatable(Serialization obj_round2)
        {
            try
            {
                dataTable = new DataTable();
                g1 = new List<string[]>();
                g2 = new List<string[]>();
                g3 = new List<string[]>();
                g4 = new List<string[]>();

                for (int i = obj_round2.Event.noOfteamsForNextRound - 1; i >= 0; i--)
                {
                    bool flag = false;
                    for (int m = 0; m < obj_round2.Teams[i].MemberDetails.Count; m++)
                    {
                        if (obj_round2.Teams[i].MemberDetails[m].CNR == null)
                            flag = true;
                    }

                    if (!flag)
                    {
                        g1.Add(new string[] { Convert.ToString(obj_round2.Teams[i].No), obj_round2.Teams[i].IocCode, obj_round2.Teams[i].MemberDetails[0].CNR, obj_round2.Teams[i].MemberDetails[0].HorseName, obj_round2.Teams[i].MemberDetails[0].RiderName, obj_round2.Teams[i].MemberDetails[0].Round2_Total, obj_round2.Teams[i].MemberDetails[0].Round2_Time });

                        g2.Add(new string[] { Convert.ToString(obj_round2.Teams[i].No), obj_round2.Teams[i].IocCode, obj_round2.Teams[i].MemberDetails[1].CNR, obj_round2.Teams[i].MemberDetails[1].HorseName, obj_round2.Teams[i].MemberDetails[1].RiderName, obj_round2.Teams[i].MemberDetails[1].Round2_Total, obj_round2.Teams[i].MemberDetails[1].Round2_Time });

                        g3.Add(new string[] { Convert.ToString(obj_round2.Teams[i].No), obj_round2.Teams[i].IocCode, obj_round2.Teams[i].MemberDetails[2].CNR, obj_round2.Teams[i].MemberDetails[2].HorseName, obj_round2.Teams[i].MemberDetails[2].RiderName, obj_round2.Teams[i].MemberDetails[2].Round2_Total, obj_round2.Teams[i].MemberDetails[2].Round2_Time });

                        g4.Add(new string[] { Convert.ToString(obj_round2.Teams[i].No), obj_round2.Teams[i].IocCode, obj_round2.Teams[i].MemberDetails[3].CNR, obj_round2.Teams[i].MemberDetails[3].HorseName, obj_round2.Teams[i].MemberDetails[3].RiderName, obj_round2.Teams[i].MemberDetails[3].Round2_Total, obj_round2.Teams[i].MemberDetails[3].Round2_Time });
                    }
                    else
                    {
                        g2.Add(new string[] { Convert.ToString(obj_round2.Teams[i].No), obj_round2.Teams[i].IocCode, obj_round2.Teams[i].MemberDetails[1].CNR, obj_round2.Teams[i].MemberDetails[1].HorseName, obj_round2.Teams[i].MemberDetails[1].RiderName, obj_round2.Teams[i].MemberDetails[1].Round2_Total, obj_round2.Teams[i].MemberDetails[1].Round2_Time });

                        g3.Add(new string[] { Convert.ToString(obj_round2.Teams[i].No), obj_round2.Teams[i].IocCode, obj_round2.Teams[i].MemberDetails[2].CNR, obj_round2.Teams[i].MemberDetails[2].HorseName, obj_round2.Teams[i].MemberDetails[2].RiderName, obj_round2.Teams[i].MemberDetails[2].Round2_Total, obj_round2.Teams[i].MemberDetails[2].Round2_Time });

                        g4.Add(new string[] { Convert.ToString(obj_round2.Teams[i].No), obj_round2.Teams[i].IocCode, obj_round2.Teams[i].MemberDetails[3].CNR, obj_round2.Teams[i].MemberDetails[3].HorseName, obj_round2.Teams[i].MemberDetails[3].RiderName, obj_round2.Teams[i].MemberDetails[3].Round2_Total, obj_round2.Teams[i].MemberDetails[3].Round2_Time });
                    }
                }

                int it = 0;
                dataTable.Columns.Add("Position");
                //dataTable.Columns.Add("Team No");
                dataTable.Columns.Add("IOC");
                dataTable.Columns.Add("CNR");
                dataTable.Columns.Add("Horse Name");
                dataTable.Columns.Add("Rider Name");
                dataTable.Columns.Add("Round2_Penalty");
                dataTable.Columns.Add("Round2_Time");

                for (int i = 0; i < g1.Count;)
                {
                    it = it + 1;
                    dataTable.Rows.Add(it, g1[i][1], g1[i][2], g1[i][3], g1[i][4], g1[i][5], g1[i][6]);
                    i++;
                }

                for (int i = 0; i < g2.Count;)
                {
                    it = it + 1;
                    dataTable.Rows.Add(it, g2[i][1], g2[i][2], g2[i][3], g2[i][4], g2[i][5], g2[i][6]);
                    i++;
                }

                for (int i = 0; i < g3.Count;)
                {
                    it = it + 1;
                    dataTable.Rows.Add(it, g3[i][1], g3[i][2], g3[i][3], g3[i][4], g3[i][5], g3[i][6]);
                    i++;
                }

                for (int i = 0; i < g4.Count;)
                {
                    it = it + 1;
                    dataTable.Rows.Add(it, g4[i][1], g4[i][2], g4[i][3], g4[i][4], g4[i][5], g4[i][6]);
                    i++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with creating datatable in round2, please check obj or column fields of datatable!\n" + ex.Message);
            }
          
        }

        public void create_storder(Serialization obj_round2)
        {
            try
            {
                NpwStorderRankingR2 npw_storder = new NpwStorderRankingR2();

                StringBuilder csvcontent = new StringBuilder();

                for (int i = obj_round2.Event.noOfteamsForNextRound - 1; i >= 0; i--)
                {
                    string csv = "";
                    int order = 0;
                    csv = npw_storder.team_info(csv, obj_round2, i, order);
                    csvcontent.AppendLine(csv);

                    for (int j = 0; j < obj_round2.Teams[i].MemberDetails.Count(); j++)
                    {
                        csv = null;
                        order += 1;
                        csv = npw_storder.member_info(csv, obj_round2, i, j, order);
                        csvcontent.AppendLine(csv);
                    }
                }

                if (NationCup.obj_internal.GeneralSetting.showFolderPath != null)
                {
                    string csvpath = Path.Combine(NationCup.obj_internal.GeneralSetting.showFolderPath, "storder.npw");
                    File.Create(csvpath).Dispose();
                    File.AppendAllText(csvpath, csvcontent.ToString());
                }
                else
                {
                    MessageBox.Show("Please provide show folder path in general settings!\n");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with creating storder.npw in round2!\n" + ex.Message);
            }
            
        }

        public void create_starter(Serialization obj_round2, int no, string cnr)
        {
            try
            {
                NpwStarterErgebnisR2 npw_starter = new NpwStarterErgebnisR2();
                StringBuilder csvcontent = new StringBuilder();

                string csv = "";
                int order = 0;

                for (int i = 0; i < obj_round2.Teams.Count(); i++)
                {
                    if (no == obj_round2.Teams[i].No)
                    {
                        csv = npw_starter.team_info(csv, obj_round2, i, order);
                        csvcontent.AppendLine(csv);

                        for (int k = 0; k < obj_round2.Teams[i].MemberDetails.Count(); k++)
                        {
                            csv = null;
                            order += 1;

                            if (cnr == obj_round2.Teams[i].MemberDetails[k].CNR)
                                csv = npw_starter.current_member_info(csv, obj_round2, i, k, order);

                            else
                                csv = npw_starter.member_info(csv, obj_round2, i, k, order);

                            csvcontent.AppendLine(csv);
                        }

                        if (NationCup.obj_internal.GeneralSetting.showFolderPath != null)
                        {
                            string csvpath = Path.Combine(NationCup.obj_internal.GeneralSetting.showFolderPath, "starter.npw");
                            File.Create(csvpath).Dispose();
                            File.AppendAllText(csvpath, csvcontent.ToString());
                            Console.WriteLine(csvcontent.ToString());
                        }
                        else
                        {
                            MessageBox.Show("Please provide show folder path in general settings!\n");
                        }    
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with creating starter.npw in round2!\n" + ex.Message);
            }
            
        }

        public void create_ergebnis(Serialization obj_round2, int no, string cnr)
        {
            try
            {
                NpwStarterErgebnisR2 npw_ergebnis = new NpwStarterErgebnisR2();
                StringBuilder csvcontent = new StringBuilder();

                string csv = "";
                int order = 0;

                for (int i = 0; i < obj_round2.Teams.Count(); i++)
                {
                    if (no == obj_round2.Teams[i].No)
                    {
                        csv = npw_ergebnis.team_info(csv, obj_round2, i, order);
                        csvcontent.AppendLine(csv);

                        for (int k = 0; k < obj_round2.Teams[i].MemberDetails.Count(); k++)
                        {
                            csv = null;
                            order += 1;

                            if (cnr == obj_round2.Teams[i].MemberDetails[k].CNR)
                            {
                                csv = npw_ergebnis.current_member_info(csv, obj_round2, i, k, order);
                            }

                            else
                                csv = npw_ergebnis.member_info(csv, obj_round2, i, k, order);

                            csvcontent.AppendLine(csv);
                        }

                        if (NationCup.obj_internal.GeneralSetting.showFolderPath != null)
                        {
                            string csvpath = Path.Combine(NationCup.obj_internal.GeneralSetting.showFolderPath, "ergebnis.npw");
                            File.Create(csvpath).Dispose();
                            File.AppendAllText(csvpath, csvcontent.ToString());
                            Console.WriteLine(csvcontent.ToString());
                        }
                        else
                        {
                            MessageBox.Show("Please provide show folder path in general settings!\n");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with creating ergebnis.npw in round2!\n" + ex.Message);
            }
           
        }

        public void create_ranking(Serialization obj_round2)
        {
            try
            {
                rank_Calculate.Rank_for_pen_time(obj_round2);

                NpwStorderRankingR2 npw_ranking = new NpwStorderRankingR2();
                StringBuilder csvcontent = new StringBuilder();

                for (int i = 0; i < rank_Calculate.result.Count(); i++)
                {
                    string csv = "";
                    int order = 0;
                    for (int j = 0; j < obj_round2.Teams.Count; j++)
                    {
                        if (rank_Calculate.result[i].teamName == obj_round2.Teams[j].Name)
                        {
                            csv = npw_ranking.team_info(csv, obj_round2, j, order);
                            csvcontent.AppendLine(csv);

                            for (int k = 0; k < obj_round2.Teams[j].MemberDetails.Count(); k++)
                            {
                                csv = null;
                                order += 1;
                                csv = npw_ranking.member_info(csv, obj_round2, j, k, order);
                                csvcontent.AppendLine(csv);
                            }
                        }
                    }
                }

                if (NationCup.obj_internal.GeneralSetting.showFolderPath != null)
                {
                    string csvpath = Path.Combine(NationCup.obj_internal.GeneralSetting.showFolderPath, "ranking.npw");
                    File.Create(csvpath).Dispose();
                    File.AppendAllText(csvpath, csvcontent.ToString());
                }
                else
                {
                    MessageBox.Show("Please provide show folder path in general settings!\n");
                }  
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with creating ranking.npw in round2!\n" + ex.Message);
            }
        }

        public void getStartListR2(Serialization obj_round2)
        {
            startListR2 = new List<TeamCompetition>();
            for (int i = 0; i < obj_round2.Event.noOfteamsForNextRound; i++)
            {
                int it = 0;
                while (it < obj_round2.Teams.Count && dataTable.Rows[i].ItemArray[1].ToString() != obj_round2.Teams[it].IocCode)
                {
                    it++;
                }
                if (dataTable.Rows[i].ItemArray[1].ToString() == obj_round2.Teams[it].IocCode)
                {
                    startListR2.Add(obj_round2.Teams[it]);
                }
            }
            int count = 0;
            for (int i = 0; i < startListR2.Count(); i++)
            {
                count = count + 1;
                startListR2[i].position = count;
            }
        }

        public void getresultListR2(Serialization obj_round2)
        {
            resultListR2 = new List<TeamCompetition>();
            for (int i = 0; i < obj_round2.Rank.Count; i++)
            {
                int it = 0;
                while (it < obj_round2.Teams.Count && obj_round2.Rank[i].teamName != obj_round2.Teams[it].Name)
                {
                    it++;
                }
                if (obj_round2.Rank[i].teamName == obj_round2.Teams[it].Name)
                {
                    resultListR2.Add(obj_round2.Teams[it]);
                    obj_round2.ResultR2.Add(obj_round2.Teams[it]);
                }
            }
            int count = 0;
            for (int i = 0; i < resultListR2.Count(); i++)
            {
                count = count + 1;
                resultListR2[i].position = count;
            }
        }
    }
}
