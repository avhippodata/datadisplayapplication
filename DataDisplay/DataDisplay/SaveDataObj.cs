﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataDisplay
{
    public class SaveDataObj
    {
        public void saveResultObj(List<MemberDetails> m_round, List<MemberDetails> m_obj)
        {
            for (int i = 0; i < m_round.Count; i++)
            {
                for (int j = 0; j < m_obj.Count; j++)
                {
                    if (m_round[i].CNR == m_obj[j].CNR)
                    {
                        m_obj[j].Round1_Time = m_round[i].Round1_Time;
                        m_obj[j].Round1_Penalty = m_round[i].Round1_Penalty;
                        m_obj[j].Round1_Time_Penalty = m_round[i].Round1_Time_Penalty;
                        m_obj[j].Round1_Total = m_round[i].Round1_Total;
                        m_obj[j].Status_R1 = m_round[i].Status_R1;

                        m_obj[j].Round2_Time = m_round[i].Round2_Time;
                        m_obj[j].Round2_Penalty = m_round[i].Round2_Penalty;
                        m_obj[j].Round2_Time_Penalty = m_round[i].Round2_Time_Penalty;
                        m_obj[j].Round2_Total = m_round[i].Round2_Total;
                        m_obj[j].Status_R2 = m_round[i].Status_R2;

                        m_obj[j].JumpOff_Time = m_round[i].JumpOff_Time;
                        m_obj[j].JumpOff_Penalty = m_round[i].JumpOff_Penalty;
                        m_obj[j].JumpOff_Time_Penalty = m_round[i].JumpOff_Time_Penalty;
                        m_obj[j].JumpOff_Total = m_round[i].JumpOff_Total;
                        //m_obj[j].Status_R1 = m_round[i].Status_R1;
                        break;
                    }
                }
            }
        }

        public void saveTime(Serialization obj, int cbox, string cnr, string time)
        {
            for (int i = 0; i < obj.Teams.Count(); i++)
            {
                for (int j = 0; j < obj.Teams[i].MemberDetails.Count(); j++)
                {
                    if (obj.Teams[i].MemberDetails[j].CNR == cnr)
                    {
                        if (cbox == 0)
                        {
                            obj.Teams[i].MemberDetails[j].Round1_Time = time;
                            break;
                        }
                        else if (cbox == 1)
                        {
                            obj.Teams[i].MemberDetails[j].Round2_Time = time;
                            break;
                        }

                        else if(cbox == 2)
                        {
                            break;
                        }
                        else if (cbox == 3)
                        {
                            obj.Teams[i].MemberDetails[j].JumpOff_Time = time;
                            break;
                        }
                    }
                }
            }
        }

        public void savePenalty(Serialization obj, int cbox, string cnr, string penalty)
        {
            for (int i = 0; i < obj.Teams.Count(); i++)
            {
                for (int j = 0; j < obj.Teams[i].MemberDetails.Count(); j++)
                {
                    if (obj.Teams[i].MemberDetails[j].CNR == cnr)
                    {
                        if (cbox == 0)
                        {
                            obj.Teams[i].MemberDetails[j].Round1_Penalty = penalty;
                            break;
                        }
                        else if (cbox == 1)
                        {
                            obj.Teams[i].MemberDetails[j].Round2_Penalty = penalty;
                            break;
                        }

                        else if (cbox == 2)
                        {
                            break;
                        }
                        else if (cbox == 3)
                        {
                            obj.Teams[i].MemberDetails[j].JumpOff_Penalty = penalty;
                            break;
                        }
                        break;
                    }
                }
            }
        }

        public void saveTimePenalty(Serialization obj, int cbox, string cnr, string timePenalty)
        {
            for (int i = 0; i < obj.Teams.Count(); i++)
            {
                for (int j = 0; j < obj.Teams[i].MemberDetails.Count(); j++)
                {
                    if (obj.Teams[i].MemberDetails[j].CNR == cnr)
                    {
                        if (cbox == 0)
                        {
                            obj.Teams[i].MemberDetails[j].Round1_Time_Penalty = timePenalty;
                            break;
                        }
                        else if (cbox == 1)
                        {
                            obj.Teams[i].MemberDetails[j].Round2_Time_Penalty = timePenalty;
                            break;
                        }

                        else if (cbox == 2)
                        {
                            break;
                        }
                        else if (cbox == 3)
                        {
                            obj.Teams[i].MemberDetails[j].JumpOff_Time_Penalty = timePenalty;
                            break;
                        }
                        break;
                    }
                }
            }
        }

        public void saveTotalPenalty(Serialization obj, int cbox, string cnr, string totalPenalty)
        {
            for (int i = 0; i < obj.Teams.Count(); i++)
            {
                for (int j = 0; j < obj.Teams[i].MemberDetails.Count(); j++)
                {
                    if (obj.Teams[i].MemberDetails[j].CNR == cnr)
                    {
                        if (cbox == 0)
                        {
                            obj.Teams[i].MemberDetails[j].Round1_Total = totalPenalty;
                            break;
                        }
                        else if (cbox == 1)
                        {
                            obj.Teams[i].MemberDetails[j].Round2_Total = totalPenalty;
                            break;
                        }

                        else if (cbox == 2)
                        {
                            break;
                        }
                        else if (cbox == 3)
                        {
                            obj.Teams[i].MemberDetails[j].JumpOff_Total = totalPenalty;
                            break;
                        }
                        break;
                    }
                }
            }
        }
    }
}
