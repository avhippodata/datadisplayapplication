﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    [Serializable]
    public class Serialization: ICloneable
    {
        public Show Event = new Show();
        public ReportData Competition = new ReportData();
        public List<Competitor> Competitors = new List<Competitor>();
        public List<TeamCompetition> Teams = new List<TeamCompetition>();
        public List<Ranking> Rank = new List<Ranking>();
        public List<DataTableR1> DataTableR1 = new List<DataTableR1>();
        public List<TeamCompetition> ResultR1 = new List<TeamCompetition>();
        public List<TeamCompetition> ResultR2 = new List<TeamCompetition>();
        public List<TeamCompetition> ResultFinal = new List<TeamCompetition>();

        public void JsonCreation()
        {
            try
            {
                Teams = Constants.listOfTeamCompetition;
                Competitors = Constants.listOfIndivCompetitor;
                Event.SetShowData();
                Competition.GetCompetitionData();
                Rank = Constants.listOfRanking;
                DataTableR1 = Constants.listOfDataTableR1;
                ResultR1 = Constants.listOfResultR1;
                ResultR2 = Constants.listOfResultR2;
                ResultFinal = Constants.listOfFinalResult;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Problem with creating JSON in Serialization.cs!\n" + ex.Message);
            }
            
        }

        public object Clone()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                if (this.GetType().IsSerializable)
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(stream, this);              
                    stream.Position = 0;
                    return formatter.Deserialize(stream);
                }
                return null;
            }
        }
    }
}
