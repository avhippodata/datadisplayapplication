﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    [Serializable]
    public class Show
    {
        public int number { get; set; }
        public string name { get; set; }
        public DateTime date { get; set; }
        public double timeAllowedForRounds { get; set; }
        public double timeAllowedForJumpoff { get; set; }
        public int totalRounds { get; set; }
        public bool teamsForNextRound { get; set; } = true; // true -> yes and false -> no
        public int noOfteamsForNextRound { get; set; }
        public int noOfResultCounts { get; set; }
        public bool jumpOff { get; set; } = true; // true -> yes and false -> no
        public bool ridersForJumpOff { get; set; } = true; // true -> single rider and false -> whole team

        public void SetShowData()
        {
            try
            {
                number = Convert.ToInt32(Constants.jsonInfo.Event.Number);
                name = Constants.jsonInfo.Event.Name;
                date = Convert.ToDateTime(Constants.jsonInfo.Event.Start, CultureInfo.GetCultureInfo("de-DE").DateTimeFormat);
                timeAllowedForRounds = 0;
                timeAllowedForJumpoff = 0;
                totalRounds = 0;
                noOfteamsForNextRound = 0;
                noOfResultCounts = 3;
                //teamsForNextRound = true;
                //jumpOff = true;
                //ridersForJumpOff = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with initializing show data and competition information in Show.cs!\n" + ex.Message);
            }
           
        }

    }
}
