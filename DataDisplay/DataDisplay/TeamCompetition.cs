﻿using DevExpress.XtraEditors.Controls;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    public class PenaltyData
    {
        public string teamName { get; set; }
        public int score { get; set; }
        public double time_R1 { get; set; }
        public double time_R2 { get; set; }
        public int rank { get; set; }
    }

    [Serializable]
    public class MemberDetails
    {
        public string CNR { get; set; }
        public string HorseName { get; set; }
        public string RiderName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IocCode { get; set; }
        public string AbstammungOhneBesitzer { get; set; }
        public string Round1_Penalty { get; set; }
        public string Round1_Time { get; set; }
        public string Round1_Time_Penalty { get; set; }
        public string Round1_Total { get; set; }
        public string Round2_Penalty { get; set; }
        public string Round2_Time { get; set; }
        public string Round2_Time_Penalty { get; set; }
        public string Round2_Total { get; set; }
        public string JumpOff_Penalty { get; set; }
        public string JumpOff_Time { get; set; }
        public string JumpOff_Time_Penalty { get; set; }
        public string JumpOff_Total { get; set; }
        public string Status_R1 { get; set; }
        public string Status_R2 { get; set; }

        public string getHorseData(List<Horse> list, int cnr)
        {
            string _s = "";
            try
            {               
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].cNo == cnr)
                    {
                        // Farbe
                        if (!string.IsNullOrEmpty(list[i].color))
                        {
                            if (!string.IsNullOrEmpty(_s))
                                _s = _s + " / ";
                            _s = _s + list[i].color;
                        }

                        // Alter
                        if (DateAndTime.Now.Year > Convert.ToInt32(list[i].yearOfBirth) & Convert.ToInt32(list[i].yearOfBirth) > 0)
                        {
                            if (!string.IsNullOrEmpty(_s))
                                _s = _s + " / ";
                            _s = _s + (DateAndTime.Now.Year - Convert.ToInt32(list[i].yearOfBirth)).ToString() + "y";
                        }

                        // Geschlecht
                        if (!string.IsNullOrEmpty(list[i].sex))
                        {
                            if (!string.IsNullOrEmpty(_s))
                                _s = _s + " / ";
                            _s = _s + list[i].sex;
                        }

                        // Vater
                        if (!string.IsNullOrEmpty(list[i].fatherName))
                        {
                            if (!string.IsNullOrEmpty(_s))
                                _s = _s + " / ";
                            _s = _s + list[i].fatherName;
                        }

                        // MutterVater
                        if (!string.IsNullOrEmpty(list[i].motherFatherName))
                        {
                            if (!string.IsNullOrEmpty(_s))
                                _s = _s + " / ";
                            _s = _s + list[i].motherFatherName;
                        }

                        // nation
                        if (!string.IsNullOrEmpty(list[i].nation))
                        {
                            if (!string.IsNullOrEmpty(_s))
                                _s = _s + " / ";
                            _s = _s + list[i].breed;
                        }

                        // FEI ID
                        if (!string.IsNullOrEmpty(list[i].feiId))
                        {
                            if (!string.IsNullOrEmpty(_s))
                                _s = _s + " / ";
                            _s = _s + list[i].feiId;
                        }

                        // Besitzername
                        if (!string.IsNullOrEmpty(list[i].onwer))
                        {
                            if (!string.IsNullOrEmpty(_s))
                                _s = _s + " / ";
                            _s = _s + list[i].onwer;
                        }
                        break;
                    }
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex.Message);
            }
            return _s;
        }
    }

    [Serializable]
    public class TeamCompetition
    {
        public int No { get; set; }
        public string Name { get; set; }
        public string IocCode { get; set; }
        public string Trainer { get; set; }
        public int Rank { get; set; }
        public int position { get; set; }

        public List<MemberDetails> MemberDetails { get;  set ;  }

        public int Total_Round1_Penalty {
            get
            {
                int panelty1 = 0;

                if (MemberDetails != null && MemberDetails.Count > 1)
                {
                    panelty1 = MemberDetails.Where(x => (x.Round1_Total != null) && (x.Status_R1 == "W") && (! x.Round1_Total.Contains("(")) &&(! x.Round1_Total.Contains(")"))).Sum(x => Convert.ToInt32(x.Round1_Total));
                    //panelty1 = MemberDetails.Where(x => (x.Round1_Penalty != null) && (x.Status == "W")).Sum(x => Convert.ToInt32(x.Round1_Penalty));
                }

                return panelty1;
            }
        }

        public double Total_Round1_Time
        {
            get
            {
                //Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
                double timeX = 0;

                if (MemberDetails != null && MemberDetails.Count > 1)
                {
                    timeX = MemberDetails.Where(x => (x.Round1_Time != null) && (x.Status_R1 == "W") && (!x.Round1_Time.Contains("(")) &&
                    (!x.Round1_Time.Contains(")"))).Sum(x => double.Parse(x.Round1_Time));
                    //timeX = MemberDetails.Where(x => (x.Round2_Time != null) &&  (!x.Round2_Time.Contains("(")) && (!x.Round2_Time.Contains(")")) && (!x.Round2_Time.Contains("-"))).Sum(x => float.Parse(x.Round2_Time, System.Globalization.CultureInfo.InvariantCulture.NumberFormat));
                }

                return timeX;
            }
        }
        
        public int Total_Round2_Penalty
        {
            get
            {
                int panelty2 = 0;

                if (MemberDetails != null && MemberDetails.Count > 1)
                {           
                    panelty2 = MemberDetails.Where(x => (x.Round2_Total != null) && (x.Status_R2 == "W") && 
                    (!x.Round2_Total.Contains("(")) && (!x.Round2_Total.Contains(")"))).Sum(x => Convert.ToInt32(x.Round2_Total));
                    //panelty2 = MemberDetails.Where(x => (x.Round2_Total != null) && (!x.Round2_Total.Contains("(")) && (!x.Round2_Total.Contains(")"))).Sum(x => Convert.ToInt32(x.Round2_Total));
                }

                return panelty2;
            }
        }

        public double Total_Round2_Time
        {
            get
            {
                //Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
                double timeX = 0;

                if (MemberDetails != null && MemberDetails.Count > 1)
                {
                    timeX = MemberDetails.Where(x => (x.Round2_Time != null) && (x.Status_R2 == "W") && (!x.Round2_Time.Contains("(")) && 
                    (!x.Round2_Time.Contains(")"))).Sum(x => double.Parse(x.Round2_Time));
                    //timeX = MemberDetails.Where(x => (x.Round2_Time != null) &&  (!x.Round2_Time.Contains("(")) && (!x.Round2_Time.Contains(")")) && (!x.Round2_Time.Contains("-"))).Sum(x => float.Parse(x.Round2_Time, System.Globalization.CultureInfo.InvariantCulture.NumberFormat));
                }

                return timeX;
            }
        }

        public int Total_JumpOff_Penalty
        {
            get
            {
                int panelty = 0;

                if (MemberDetails != null && MemberDetails.Count > 1)
                {
                    panelty = MemberDetails.Where(x => (x.JumpOff_Penalty != null) && (!x.JumpOff_Penalty.Contains("(")) && (!x.JumpOff_Penalty.Contains(")"))).Sum(x => Convert.ToInt32(x.JumpOff_Penalty));
                }

                return panelty;
            }
        }

        public float Total_JumpOff_Time
        {
            get
            {
                float timeX = 0;
                //Thread.CurrentThread.CurrentCulture = new CultureInfo("de-DE");
                if (MemberDetails != null && MemberDetails.Count > 1)
                {
                    timeX = MemberDetails.Where(x => (x.JumpOff_Time != null) && (!x.JumpOff_Time.Contains("(")) && (!x.JumpOff_Time.Contains(")")) && (!x.JumpOff_Time.Contains("-"))).Sum(x => float.Parse(x.JumpOff_Time, System.Globalization.CultureInfo.InvariantCulture.NumberFormat));
                }

                return timeX;
            }
        }

        public int Total_all_Penalty
        {
            get
            {
                int panelty = 0;

                if (MemberDetails != null && MemberDetails.Count > 1)
                {
                    panelty = Total_Round1_Penalty + Total_Round2_Penalty;
                }

                return panelty;
            }
        }

        public void SetTeamInfo()
        {
            try
            {
                int count = 1;
                Constants.listOfTeamCompetition = new List<TeamCompetition>();
                for (int i = 0; i < Constants.jsonInfo.Competition.Teams.Count(); i++)
                {
                    TeamCompetition teamCompetition = new TeamCompetition();
                    teamCompetition.No = Constants.jsonInfo.Competition.Teams[i].Nr;
                    teamCompetition.Name = Constants.jsonInfo.Competition.Teams[i].Name;
                    teamCompetition.IocCode = Constants.jsonInfo.Competition.Teams[i].Nation;
                    teamCompetition.Trainer = Constants.jsonInfo.Competition.Teams[i].Trainer;
                    teamCompetition.Rank = 0;
                    teamCompetition.position = count++;
                    teamCompetition.GetTeamInfo(i);
                    Constants.listOfTeamCompetition.Add(teamCompetition);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with writing data of teams into teamCompetition (SetTeamInfo method)!\n" + ex.Message);
            }
        }

        public void GetTeamInfo(int x)
        {
            try
            {
                Round1_database round1_Database = new Round1_database();
                MemberDetails = new List<MemberDetails>();
                for (int i = 0; i < Constants.jsonInfo.Competition.Teams[x].Members.Count(); i++)
                {
                    MemberDetails member = new MemberDetails();

                    int _cnr = Constants.jsonInfo.Competition.Teams[x].Members[i].CNR;
                    for (int h = 0; h < Constants.allHorse.Count(); h++)
                    {
                        if (Constants.allHorse[h].cNo == _cnr)
                        {
                            member.CNR = Convert.ToString(Constants.allHorse[h].cNo);
                            member.HorseName = Constants.allHorse[h].name;
                            member.RiderName = Constants.allAthelete[h].name;
                            member.FirstName = Constants.allAthelete[h].firstName;
                            member.LastName = Constants.allAthelete[h].lastName;
                            member.IocCode = Constants.allAthelete[h].iocCode;
                            member.AbstammungOhneBesitzer = member.getHorseData(Constants.allHorse, Convert.ToInt32(member.CNR));
                            member.Status_R1 = null;
                            member.Status_R2 = null;
                            break;
                        }
                    }

                    MemberDetails.Add(member);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Problem with writing competitor data (Athlete and Horse) in GetTeamInfo method in TeamComeptition.cs!\n" + ex.Message);
            }
        }    
    }
}

