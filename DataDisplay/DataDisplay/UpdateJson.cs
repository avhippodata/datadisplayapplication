﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    public class UpdateJson
    {
        public void modifyJson(Serialization new_obj)
        {
            try
            {
                // Json updates
                var tmpJSON = Newtonsoft.Json.JsonConvert.SerializeObject(new_obj, Formatting.Indented);
                if (File.Exists(Constants.sourceFilePath))
                {
                    File.Delete(Constants.sourceFilePath);
                    File.Create(Constants.sourceFilePath).Dispose();
                }
                File.WriteAllText(Constants.sourceFilePath, JToken.FromObject(tmpJSON).ToString());
            }
            catch(Exception ex)
            {
                MessageBox.Show("Unable to modify JSON file (HTC) in UpdateJson.cs!\n" + ex.Message);
            }
        }

        public void modifyJsonForGeneralSettings(SerializationForInternal new_obj)
        {
            try
            {
                // Json updates
                var tmpJSON = Newtonsoft.Json.JsonConvert.SerializeObject(new_obj, Formatting.Indented);
                if (File.Exists(Constants.internalJsonPath))
                {
                    File.Delete(Constants.internalJsonPath);
                    File.Create(Constants.internalJsonPath).Dispose();
                }
                File.WriteAllText(Constants.internalJsonPath, JToken.FromObject(tmpJSON).ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to modify internal JSON file (%temp%) for general settings!\n" + ex.Message);
            }
}
    }
}
