﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    public class WriteDat
    {
        List<string> temp = new List<string>();
        public void getDataForDat(Serialization new_obj, string cnr)
        {
            try
            {
                for (int i = 0; i < new_obj.Teams.Count(); i++)
                {
                    for (int j = 0; j < new_obj.Teams[i].MemberDetails.Count(); j++)
                    {
                        if (new_obj.Teams[i].MemberDetails[j].CNR == cnr)
                        {
                            for (int k = 0; k < new_obj.Competitors.Count; k++)
                            {
                                if (Convert.ToString(new_obj.Competitors[k].horse.cNo) == cnr)
                                {
                                    temp.Add(DateTime.Now.ToString("h:mm")); // 0
                                    temp.Add(Convert.ToString(new_obj.Competitors[k].startPosition)); //1
                                    temp.Add(new_obj.Teams[i].MemberDetails[j].CNR); // 2
                                    temp.Add(new_obj.Teams[i].MemberDetails[j].HorseName); //3
                                    temp.Add(new_obj.Teams[i].MemberDetails[j].LastName + "," + new_obj.Teams[i].MemberDetails[j].FirstName); // 4
                                    temp.Add(new_obj.Competitors[k].athlete.clubName); // 5
                                    temp.Add(new_obj.Teams[i].MemberDetails[j].IocCode); // 6
                                    temp.Add(new_obj.Competitors[k].athlete.feiId); // 7
                                    temp.Add(Convert.ToString(new_obj.Competitors.Count())); //8
                                    temp.Add(new_obj.Competitors[k].horse.yearOfBirth); // 9
                                    temp.Add(new_obj.Competitors[k].horse.fatherName); // 10
                                    temp.Add(new_obj.Competitors[k].horse.motherFatherName); //11
                                    temp.Add(new_obj.Competitors[k].horse.onwer); // 12
                                    temp.Add(new_obj.Competitors[k].horse.breed); //13
                                    temp.Add(new_obj.Competitors[k].horse.sex); // 14
                                    temp.Add(new_obj.Competitors[k].horse.nation); // 15
                                    temp.Add(Convert.ToString(new_obj.Teams[i].Rank)); // 16
                                    temp.Add(new_obj.Competitors[k].horse.breedingArea); // 17
                                    temp.Add(new_obj.Competitors[k].horse.nationalId); // 18
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Problem with obtaining competitors data (Athlete and Horse) in WriteDat.cs!\n" + ex.Message);
            }
        }

        public void CreateDat1(Serialization new_obj, string cnr, string round)
        {
            try
            {
                char Q = '"';
                string N = "\r\n";
                string csv;

                getDataForDat(new_obj, cnr);

                string nationRider = " " + temp[6] + "  ";
                int currentyr = DateTime.Now.Year;
                int horseyr = currentyr - Convert.ToInt16(temp[9].ToString());
                string horsedetails = Convert.ToString(horseyr) + " years old / " + temp[10].ToString() + " / " + temp[11].ToString() + " / " + temp[17].ToString();

                csv = Q + "" + Q + "," + Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + new_obj.Competition.competitionNumber + Q + N +
                    Q + new_obj.Competition.competitionName + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + (Convert.ToInt32(temp[8]) - Convert.ToInt32(temp[1])) + Q + N +
                    Q + "" + Q + N +
                    Q + temp[0].ToString() + Q + N +
                    Q + temp[1].ToString() + ". Starter In " + round + Q + N +
                    Q + temp[2].ToString() + Q + N +
                    Q + temp[3].ToString() + Q + N +
                    Q + temp[4].ToString() + Q + N +
                    Q + temp[5].ToString() + Q + N +
                    Q + "      " + Q + N +
                    Q + nationRider + Q + N +
                    Q + temp[7].ToString() + Q + N +
                    Q + "" + Q + N +
                    Q + temp[8].ToString() + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + temp[9].ToString() + Q + N +
                    Q + temp[10].ToString() + Q + N +
                    Q + temp[11].ToString() + Q + N +
                    Q + temp[12].ToString() + Q + N +
                    Q + temp[13].ToString() + Q + N +
                    Q + temp[17].ToString() + Q + N +
                    Q + "" + Q + N +
                    Q + temp[14].ToString() + Q + N +
                    Q + temp[18].ToString() + Q + N +
                    Q + horsedetails + Q;

                if (NationCup.obj_internal.GeneralSetting.tvFolderPath != null)
                {
                    string csvpath = Path.Combine(NationCup.obj_internal.GeneralSetting.tvFolderPath, "TV1.DAT");
                    File.Create(csvpath).Dispose();
                    File.AppendAllText(csvpath, csv.ToString());
                }
                else
                {
                    MessageBox.Show("Please provide TV folder path in general settings!\n");
                }
                
            }
            catch(Exception ex)
            {
                MessageBox.Show("Problem with creating TV1.DAT in WriteDat.cs!\n" + ex.Message);
            }
        }

        public void CreateDat2(Serialization new_obj, string cnr, string penalties, string times, string round)
        {
            try
            {
                char Q = '"';
                string N = "\r\n";
                string csv;

                getDataForDat(new_obj, cnr);

                string nationRider = " " + temp[6] + "  ";
                int currentyr = DateTime.Now.Year;
                int horseyr = currentyr - Convert.ToInt16(temp[9].ToString());
                string horsedetails = Convert.ToString(horseyr) + " years old / " + temp[10].ToString() + " / " + temp[11].ToString() + " / " + temp[17].ToString();
                string resultformated = penalties + " penalties   " + times + " sec";
                string rankformated = "Momentan Rang " + temp[16];

                csv = Q + "" + Q + "," + Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + new_obj.Competition.competitionNumber + Q + N +
                    Q + new_obj.Competition.competitionName + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + (Convert.ToInt32(temp[8]) - Convert.ToInt32(temp[1])) + Q + N +
                    Q + "" + Q + N +
                    Q + temp[0].ToString() + Q + N +
                    Q + temp[1].ToString() + ". Starter In " + round + Q + N +
                    Q + temp[2].ToString() + Q + N +
                    Q + temp[3].ToString() + Q + N +
                    Q + temp[4].ToString() + Q + N +
                    Q + temp[5].ToString() + Q + N +
                    Q + "      " + Q + N +
                    Q + nationRider + Q + N +
                    Q + temp[7].ToString() + Q + N +
                    Q + "" + Q + N +
                    Q + temp[8].ToString() + Q + N +
                    Q + resultformated + Q + N +
                    Q + rankformated + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + (Convert.ToInt32(round.Remove(0, 6)) - 1) + Q + N +
                    Q + penalties + Q + N +
                    Q + times + Q + N +
                    Q + temp[16] + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "0.000" + Q + N +
                    Q + "0.000" + Q + N +
                    Q + "0.000" + Q + N +
                    Q + "0.000" + Q + N +
                    Q + "0.000" + Q + N +
                    Q + "0.000" + Q + N +
                    Q + "0.000" + Q + N +
                    Q + "0.000" + Q + N +
                    Q + "0.000" + Q + N +
                    Q + "0.000" + Q + N +
                    Q + "0.000" + Q + N +
                    Q + "0.000" + Q + N +
                    Q + "0.000" + Q + N +
                    Q + "0.000" + Q + N +
                    Q + "0.000" + Q + N +
                    Q + "0.000" + Q + N +
                    Q + "0.000%" + Q + N +
                    Q + "" + Q + N +
                    Q + temp[9].ToString() + Q + N +
                    Q + temp[10].ToString() + Q + N +
                    Q + temp[11].ToString() + Q + N +
                    Q + temp[12].ToString() + Q + N +
                    Q + temp[13].ToString() + Q + N +
                    Q + temp[17].ToString() + Q + N +
                    Q + "" + Q + N +
                    temp[14].ToString() + Q + N +
                    Q + temp[18].ToString() + Q + N +
                    Q + horsedetails + Q;

                if (NationCup.obj_internal.GeneralSetting.tvFolderPath != null)
                {
                    string csvpath = Path.Combine(NationCup.obj_internal.GeneralSetting.tvFolderPath, "TV2.DAT");
                    File.Create(csvpath).Dispose();
                    File.AppendAllText(csvpath, csv.ToString());
                }
                else
                {
                    MessageBox.Show("Please provide TV folder path in general settings!\n");
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with creating TV2.DAT in WriteDat.cs!\n" + ex.Message);
            }
        }

        public void CreateDat3(Serialization new_obj, string round)
        {
            try
            {
                char Q = '"';
                string N = "\r\n";
                string csv;

                string competitionDetails = Q + new_obj.Competition.competitionNumber + Q + "," + Q + new_obj.Competition.competitionName + Q + "," + Q + "?" + Q + "," + Q + "?" + Q;
                int roundNo = Convert.ToInt16(round.Remove(0, 6)) - 1;
                string resultsRoundDetails = Q + "?" + Q + "," + new_obj.Competitors.Count() + "," + roundNo;

                csv = Q + "" + Q + N + competitionDetails + N + resultsRoundDetails + N + Q + "" + Q + N + Q + "" + Q + N + Q + "" + Q + N + Q + "" + Q + N + Q + "" + Q +
                    N + Q + "" + Q + N + Q + "?" + Q + N + Q + "" + Q + N + Q + "" + Q + N + Q + "" + Q + N;

                if (round.Remove(0, 6) == Convert.ToString(1))
                {
                    for (int i = 0; i < new_obj.Competitors.Count(); i++)
                    {
                        for (int j = 0; j < new_obj.Teams.Count; j++)
                        {
                            for (int k = 0; k < new_obj.Teams[j].MemberDetails.Count; k++)
                            {
                                if (Convert.ToString(new_obj.Competitors[i].horse.cNo) == new_obj.Teams[j].MemberDetails[k].CNR)
                                {
                                    csv = new_obj.Competition.competitionNumber + "," + Q + new_obj.Competitors[i].horse.name + Q + "," + Q + new_obj.Competitors[i].athlete.lastName + "," +
                                        new_obj.Competitors[i].athlete.firstName + Q + "," + Q + new_obj.Competitors[i].athlete.clubName + Q + "," + Q + new_obj.Teams[j].MemberDetails[k].Round1_Total +
                                        " penalties      " + new_obj.Teams[j].MemberDetails[k].Round1_Time + " sec" + Q + "," + Q + new_obj.Teams[j].MemberDetails[k].IocCode + Q + "," + Q + new_obj.Teams[j].Rank + "." + Q + "," + Q + Q + N +
                                        new_obj.Teams[j].MemberDetails[k].Round1_Total + "," + new_obj.Teams[j].MemberDetails[k].Round1_Time + "," + roundNo + N + "?";
                                }
                            }
                        }

                    }
                }

                if (round.Remove(0, 6) == Convert.ToString(2))
                {
                    for (int i = 0; i < new_obj.Competitors.Count(); i++)
                    {
                        for (int j = 0; j < new_obj.Teams.Count; j++)
                        {
                            for (int k = 0; k < new_obj.Teams[j].MemberDetails.Count; k++)
                            {
                                if (Convert.ToString(new_obj.Competitors[i].horse.cNo) == new_obj.Teams[j].MemberDetails[k].CNR)
                                {
                                    csv = new_obj.Competition.competitionNumber + "," + Q + new_obj.Competitors[i].horse.name + Q + "," + Q + new_obj.Competitors[i].athlete.lastName + "," +
                                        new_obj.Competitors[i].athlete.firstName + Q + "," + Q + new_obj.Competitors[i].athlete.clubName + Q + "," + Q + new_obj.Teams[j].MemberDetails[k].Round2_Total +
                                        " penalties      " + new_obj.Teams[j].MemberDetails[k].Round2_Time + " sec" + Q + "," + Q + new_obj.Teams[j].MemberDetails[k].IocCode + Q + "," + Q + new_obj.Teams[j].Rank + "." + Q + "," + Q + Q + N +
                                        new_obj.Teams[j].MemberDetails[k].Round2_Total + "," + new_obj.Teams[j].MemberDetails[k].Round2_Time + "," + roundNo + N + "?";
                                }
                            }
                        }

                    }
                }

                if (NationCup.obj_internal.GeneralSetting.tvFolderPath != null)
                {
                    string csvpath = Path.Combine(NationCup.obj_internal.GeneralSetting.tvFolderPath, "TV3.DAT");
                    File.Create(csvpath).Dispose();
                    File.AppendAllText(csvpath, csv.ToString());
                }           
                else
                {
                    MessageBox.Show("Please provide TV folder path in general settings!\n");
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with creating TV3.DAT in WriteDat.cs!\n" + ex.Message);
            }
        }

        public void CreateDat4R1(List<DataTableR1> dataTableR1, Serialization new_obj, string round)
        {
            try
            {
                char Q = '"';
                string N = "\r\n";
                string csv;

                string competitionDetails = Q + new_obj.Competition.competitionNumber + Q + "," + Q + new_obj.Competition.competitionName + Q + "," +
                    Q + "W" + Q + "," + Q + dataTableR1.Count + Q;


                csv = Q + "" + Q + "," + Q + "" + Q + N +
                    competitionDetails + N +
                    Q + "" + Q + ",0,0" + N +
                    Q + "" + Q + "," + Q + DateTime.Now.ToString("HH:mmtt") + Q + "," + (Convert.ToInt32(round.Remove(0, 6)) - 1) + N + // file saving time + round number
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "1" + Q + N +
                    Q + "W" + Q + N +
                    Q + "W" + Q + N +
                    Q + "N" + Q + N +
                    Q + "100" + Q + N +
                    Q + new_obj.Competition.judgesPosition + Q + N +
                    Q + new_obj.Competition.prizeMoneyTotal + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + new_obj.Competition.headerText1 + Q + N +
                    Q + new_obj.Competition.headerText2 + Q + N +
                    Q + new_obj.Competition.headerText3 + Q + N +
                    Q + new_obj.Competition.headerText4 + Q + N +
                    Q + new_obj.Competition.headerText5 + Q + N +
                    Q + new_obj.Competition.footerText1 + Q + N +
                    Q + new_obj.Competition.footerText2 + Q + N +
                    Q + new_obj.Competition.footerText3 + Q + N +
                    Q + new_obj.Competition.footerText4 + Q + N +
                    Q + new_obj.Competition.footerText5 + Q;

                if (NationCup.obj_internal.GeneralSetting.tvFolderPath != null)
                {
                    string csvpath = Path.Combine(NationCup.obj_internal.GeneralSetting.tvFolderPath, "TV4.DAT");
                    File.Create(csvpath).Dispose();
                    File.AppendAllText(csvpath, csv.ToString());
                    for (int i = 0; i < dataTableR1.Count; i++)
                    {

                        for (int j = 0; j < new_obj.Competitors.Count(); j++)
                        {
                            if (Convert.ToInt32(dataTableR1[i].CNR) == new_obj.Competitors[j].horse.cNo)
                            {
                                csv = N + Convert.ToString(i + 1) + "," +
                                Q + dataTableR1[i].HorseName + Q + "," +
                                Q + new_obj.Competitors[j].athlete.lastName + "," + new_obj.Competitors[j].athlete.firstName + Q + "," +
                                Q + new_obj.Competitors[j].athlete.clubName + Q + "," +
                                Q + new_obj.Competitors[j].athlete.feiId + Q + "," +
                                new_obj.Competitors[j].athlete.followingBreak + "," +
                                Q + new_obj.Competitors[j].athlete.iocCode + Q;

                                File.AppendAllText(csvpath, csv.ToString());
                            }
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Please provide TV folder path in general settings!\n");
                }
                

                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with creating TV4.DAT in WriteDat.cs!\n" + ex.Message);
            }
        }

        public void CreateDat4R2(DataTable dataTable, Serialization new_obj, string round)
        {
            try
            {
                char Q = '"';
                string N = "\r\n";
                string csv;

                string competitionDetails = Q + new_obj.Competition.competitionNumber + Q + "," + Q + new_obj.Competition.competitionName + Q + "," +
                    Q + "W" + Q + "," + Q + dataTable.Rows.Count + Q;


                csv = Q + "" + Q + "," + Q + "" + Q + N +
                    competitionDetails + N +
                    Q + "" + Q + ",0,0" + N +
                    Q + "" + Q + "," + Q + DateTime.Now.ToString("HH:mmtt") + Q + "," + (Convert.ToInt32(round.Remove(0, 6)) - 1) + N + // file saving time + round number
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + "1" + Q + N +
                    Q + "W" + Q + N +
                    Q + "W" + Q + N +
                    Q + "N" + Q + N +
                    Q + "100" + Q + N +
                    Q + new_obj.Competition.judgesPosition + Q + N +
                    Q + new_obj.Competition.prizeMoneyTotal + Q + N +
                    Q + "" + Q + N +
                    Q + "" + Q + N +
                    Q + new_obj.Competition.headerText1 + Q + N +
                    Q + new_obj.Competition.headerText2 + Q + N +
                    Q + new_obj.Competition.headerText3 + Q + N +
                    Q + new_obj.Competition.headerText4 + Q + N +
                    Q + new_obj.Competition.headerText5 + Q + N +
                    Q + new_obj.Competition.footerText1 + Q + N +
                    Q + new_obj.Competition.footerText2 + Q + N +
                    Q + new_obj.Competition.footerText3 + Q + N +
                    Q + new_obj.Competition.footerText4 + Q + N +
                    Q + new_obj.Competition.footerText5 + Q;

                if (NationCup.obj_internal.GeneralSetting.tvFolderPath != null)
                {
                    string csvpath = Path.Combine(NationCup.obj_internal.GeneralSetting.tvFolderPath, "TV4.DAT");
                    File.Create(csvpath).Dispose();
                    File.AppendAllText(csvpath, csv.ToString());
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {

                        for (int j = 0; j < new_obj.Competitors.Count(); j++)
                        {
                            if (Convert.ToInt32(dataTable.Rows[i].ItemArray[2]) == new_obj.Competitors[j].horse.cNo)
                            {
                                csv = N + Convert.ToString(i + 1) + "," +
                                Q + dataTable.Rows[i].ItemArray[3].ToString() + Q + "," +
                                Q + new_obj.Competitors[j].athlete.lastName + "," + new_obj.Competitors[j].athlete.firstName + Q + "," +
                                Q + new_obj.Competitors[j].athlete.clubName + Q + "," +
                                Q + new_obj.Competitors[j].athlete.feiId + Q + "," +
                                new_obj.Competitors[j].athlete.followingBreak + "," +
                                Q + new_obj.Competitors[j].athlete.iocCode + Q;

                                File.AppendAllText(csvpath, csv.ToString());
                            }
                        }

                    }
                }
                else
                {
                    MessageBox.Show("Please provide TV folder path in general settings!\n");
                }



            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with creating TV4.DAT in WriteDat.cs!\n" + ex.Message);
            }
        }
    }
}

