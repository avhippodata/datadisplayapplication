﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    public class WriteFastReports
    {
        public List<TeamCompetition> finalResultList;

        public void setRanking(Serialization obj, Serialization tmp_obj)
        {
            try
            {
                Paranthesis paranthesis = new Paranthesis();
                tmp_obj = obj;

                for (int i = 0; i < tmp_obj.Teams.Count; i++)
                {
                    paranthesis.update_paranthesisR1(tmp_obj.Teams[i].MemberDetails);
                }

                for (int i = 0; i < tmp_obj.Teams.Count; i++)
                {
                    int it = i;
                    while (obj.Rank[i].teamName != tmp_obj.Teams[it].Name)
                    {
                        it++;
                    }
                    if (it != i)
                    {
                        TeamCompetition tmp_team = new TeamCompetition();
                        tmp_team = tmp_obj.Teams[i];
                        tmp_obj.Teams[i] = tmp_obj.Teams[it];
                        tmp_obj.Teams[i].No = obj.Rank[i].rank;
                        tmp_obj.Teams[it] = tmp_team;
                    }
                    else
                    {
                        tmp_obj.Teams[it].No = obj.Rank[i].rank;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with setRanking method for fast report!\n" + ex.Message);
            }
           
        } 

        public void setResultR1(Serialization obj, bool flag)
        {
            try
            {
                flag = false;
                for (int i = 0; i < obj.Teams.Count; i++)
                {
                    for (int j = 0; j < obj.Teams[i].MemberDetails.Count; j++)
                    {
                        if (obj.Teams[i].MemberDetails[j].Round1_Total == null && obj.Teams[i].MemberDetails[j].Round1_Time == null)
                        {
                            flag = true;
                        }
                    }
                }
                if(flag == true)
                {
                    MessageBox.Show("Round 1 has not been finished yet!\n");
                }              
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with setResultR1 method for fast report!\n" + ex.Message);
            }
            
        }

        public void setResultR2(Serialization obj, bool flag)
        {
            flag = false;
            for (int i = 0; i < obj.Teams.Count; i++)
            {
                for (int j = 0; j < obj.Teams[i].MemberDetails.Count; j++)
                {
                    if (obj.Teams[i].MemberDetails[j].Round2_Total == null && obj.Teams[i].MemberDetails[j].Round2_Time == null)
                    {
                        flag = true;
                    }
                }
            }
            if (flag == true)
            {
                MessageBox.Show("Round 2 has not been finished yet!\n");
            }
        }

        public void setReport(Serialization obj, FastReport.Report report)
        {
            try
            {
                // set parameters
                report.SetParameterValue("competitionName", obj.Competition.competitionName);
                report.SetParameterValue("headerText1", obj.Competition.headerText1);
                report.SetParameterValue("headerText2", obj.Competition.headerText2);
                report.SetParameterValue("headerText3", obj.Competition.headerText3);
                report.SetParameterValue("headerText4", obj.Competition.headerText4);
                report.SetParameterValue("headerText5", obj.Competition.headerText5);
                report.SetParameterValue("footerText1", obj.Competition.footerText1);
                report.SetParameterValue("footerText2", obj.Competition.footerText2);
                report.SetParameterValue("footerText3", obj.Competition.footerText3);
                report.SetParameterValue("footerText4", obj.Competition.footerText4);
                report.SetParameterValue("footerText5", obj.Competition.footerText5);
                report.SetParameterValue("prizeMoneyTotal", obj.Competition.prizeMoneyTotal);
                report.SetParameterValue("competitionNumber", obj.Competition.competitionNumber);
                //show report
                report.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem while setting fast report parameter values (setReport method)!\n" + ex.Message);
            }
                    
        }

        public void editReport(Serialization obj, FastReport.Report report)
        {
            try
            {
                // set parameters
                report.SetParameterValue("competitionName", obj.Competition.competitionName);
                report.SetParameterValue("headerText1", obj.Competition.headerText1);
                report.SetParameterValue("headerText2", obj.Competition.headerText2);
                report.SetParameterValue("headerText3", obj.Competition.headerText3);
                report.SetParameterValue("headerText4", obj.Competition.headerText4);
                report.SetParameterValue("headerText5", obj.Competition.headerText5);
                report.SetParameterValue("footerText1", obj.Competition.footerText1);
                report.SetParameterValue("footerText2", obj.Competition.footerText2);
                report.SetParameterValue("footerText3", obj.Competition.footerText3);
                report.SetParameterValue("footerText4", obj.Competition.footerText4);
                report.SetParameterValue("footerText5", obj.Competition.footerText5);
                report.SetParameterValue("prizeMoneyTotal", obj.Competition.prizeMoneyTotal);
                report.SetParameterValue("competitionNumber", obj.Competition.competitionNumber);
                //show report
                report.Design();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem while setting fast report parameter values (editReport method)!\n" + ex.Message);
            }
           
        }

        ChangeRankingOrder rankingOrder = new ChangeRankingOrder();
        Round1_database round1_Database = new Round1_database();
        Round2 round2 = new Round2();

        public void getStartOrderAndStartListR2(Serialization obj_round2)
        {
            if (obj_round2 == null)
            {
                obj_round2 = NationCup.obj.Clone() as Serialization;
            }

            bool flag = false;
            for (int i = 0; i < obj_round2.Teams.Count(); i++)
            {
                for (int j = 0; j < obj_round2.Teams[i].MemberDetails.Count(); j++)
                {
                    if (obj_round2.Teams[i].MemberDetails[j].Round2_Penalty != null || obj_round2.Teams[i].MemberDetails[j].Round2_Time != null)
                    {
                        flag = true;
                        break;
                    }

                }

                if (flag)
                    break;
            }
            if (flag)
            {
                round2.create_datatable(obj_round2);
                round1_Database.GetInfoR2(round2.dataTable, obj_round2);
                round2.getStartListR2(obj_round2);
            }
            else
            {
                obj_round2 = rankingOrder.AscendingRanking(obj_round2);
                round2.create_datatable(obj_round2);
                round1_Database.GetInfoR2(round2.dataTable, obj_round2);
                round2.getStartListR2(obj_round2);
            }

            
        }

        public void setFinalRanking(Serialization obj_final)
        {
            finalResultList = new List<TeamCompetition>();
            for (int i = 0; i < obj_final.Rank.Count; i++)
            {
                int it = 0;
                while (it < obj_final.Teams.Count && obj_final.Rank[i].teamName != obj_final.Teams[it].Name)
                {
                    it++;
                }
                if (obj_final.Rank[i].teamName == obj_final.Teams[it].Name)
                {
                    finalResultList.Add(obj_final.Teams[it]);
                    //obj_final.ResultFinal.Add(obj_final.Teams[it]);
                }
            }
            int count = 0;
            for (int i = 0; i < finalResultList.Count(); i++)
            {
                count = count + 1;
                finalResultList[i].position = count;
            }
        }
    }
}
