using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    public class NpwStarterErgebnisJO
    {
        public string team_info(string csv, Serialization new_obj, int no, int order) // no stands for index of each team
        {
            try
            {
                string Q = "\"";
                string C = ",";
                string D = Q + C + Q;

                csv = Q + D +
                    new_obj.Teams[no].No + D +
                    order + D +
                    new_obj.Teams[no].IocCode + D +
                    new_obj.Teams[no].Rank + "." + D +
                    new_obj.Teams[no].Total_all_Penalty + D +
                    new_obj.Teams[no].Total_Round2_Time + D +
                    "Im Umlauf" + D +
                    new_obj.Teams[no].Total_Round1_Penalty + D +
                    0.00 + D +
                    new_obj.Teams[no].Name + D +
                    new_obj.Teams[no].Total_Round2_Penalty + D +
                    new_obj.Teams[no].Total_Round2_Time + D +
                    new_obj.Teams[no].Trainer + D +
                    new_obj.Teams[no].Total_JumpOff_Penalty + D +
                    new_obj.Teams[no].Total_JumpOff_Time + D +
                    0 + D + 0.00 + D + 0.00 + D + 0.00 + Q;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with writing NPW of team in JumpOff [for starter and ergebnis]!\n" + ex.Message);
            }
            return csv;
        }

        public string member_info(string csv, Serialization new_obj, int no, int k, int order)  // k stand for index of each member in a team
        {
            try
            {
                string Q = "\"";
                string C = ",";
                string D = Q + C + Q;
                char[] charsToTrim = { '(', ')' };
                string tmp = new_obj.Teams[no].MemberDetails[k].Round1_Total;
                if (new_obj.Teams[no].MemberDetails[k].Round1_Total != null)
                {
                    tmp = tmp.Trim(charsToTrim);
                }

                csv = Q + D +
                    new_obj.Teams[no].No + D +
                    order + D +
                    new_obj.Teams[no].IocCode + D +
                    new_obj.Teams[no].MemberDetails[k].CNR + D +
                    new_obj.Teams[no].MemberDetails[k].HorseName + D +
                    new_obj.Teams[no].MemberDetails[k].LastName + C + new_obj.Teams[no].MemberDetails[k].FirstName + D +
                    new_obj.Teams[no].MemberDetails[k].IocCode + D +
                    tmp + D +
                    0.00 + D +
                    new_obj.Teams[no].MemberDetails[k].Status_R1 + D +
                    new_obj.Teams[no].MemberDetails[k].Round2_Total + D +
                    new_obj.Teams[no].MemberDetails[k].Round2_Time + D +
                    " " + D +
                    new_obj.Teams[no].MemberDetails[k].JumpOff_Penalty + D +
                    new_obj.Teams[no].MemberDetails[k].JumpOff_Time + D +
                    " " + D + 0.00 + D + 0.00 + D + " " + Q;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with writing NPW of members of teams in JumpOff [for starter and ergebnis]!\n" + ex.Message);
            }
            return csv;
        }

        public string current_member_info(string csv, Serialization new_obj, int no, int k, int order)  // k stand for index of each member in a team
        {
            try
            {
                string Q = "\"";
                string C = ",";
                string D = Q + C + Q;
                char[] charsToTrim = { '(', ')' };
                string tmp = new_obj.Teams[no].MemberDetails[k].Round1_Total;
                if (new_obj.Teams[no].MemberDetails[k].Round1_Total != null)
                {
                    tmp = tmp.Trim(charsToTrim);
                }

                csv = Q + "A" + D +
                    new_obj.Teams[no].No + D +
                    order + D +
                    new_obj.Teams[no].IocCode + D +
                    new_obj.Teams[no].MemberDetails[k].CNR + D +
                    new_obj.Teams[no].MemberDetails[k].HorseName + D +
                    new_obj.Teams[no].MemberDetails[k].LastName + C + new_obj.Teams[no].MemberDetails[k].FirstName + D +
                    new_obj.Teams[no].MemberDetails[k].IocCode + D +
                    tmp + D +
                    0.00 + D +
                    new_obj.Teams[no].MemberDetails[k].Status_R1 + D +
                    new_obj.Teams[no].MemberDetails[k].Round2_Total + D +
                    new_obj.Teams[no].MemberDetails[k].Round2_Time + D +
                    " " + D +
                    new_obj.Teams[no].MemberDetails[k].JumpOff_Penalty + D +
                    new_obj.Teams[no].MemberDetails[k].JumpOff_Time + D +
                    " " + D + 0.00 + D + 0.00 + D + " " + Q;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with writing NPW of current member of a team in JumpOff [for starter and ergebnis]!\n" + ex.Message);
            }
            return csv;
        }

    }

    public class NpwStorderRankingJO
    {
        public string team_info(string csv, Serialization new_obj, int no, int order) // no stands for index of each team
        {
            try
            {
                string Q = "\"";
                string C = ",";
                string D = Q + C + Q;

                csv = Q + new_obj.Teams[no].No + D +
                    order + D +
                    new_obj.Teams[no].IocCode + D +
                    new_obj.Teams[no].Rank + "." + D +
                    new_obj.Teams[no].Total_all_Penalty + D +
                    new_obj.Teams[no].Total_Round2_Time + D +
                    "Im Umlauf" + D +
                    new_obj.Teams[no].Total_Round1_Penalty + D +
                    0.00 + D +
                    new_obj.Teams[no].Name + D +
                    new_obj.Teams[no].Total_Round2_Penalty + D +
                    new_obj.Teams[no].Total_Round2_Time + D +
                    new_obj.Teams[no].Trainer + D +
                    new_obj.Teams[no].Total_JumpOff_Penalty + D +
                    new_obj.Teams[no].Total_JumpOff_Time + D +
                    0 + D + 0.00 + D + 0.00 + D + 0.00 + Q;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with writing NPW of team in JumpOff [for storder and ranking]!\n" + ex.Message);
            }

            return csv;
        }

        public string member_info(string csv, Serialization new_obj, int no, int k, int order)  // k stand for index of each member in a team
        {
            try
            {
                string Q = "\"";
                string C = ",";
                string D = Q + C + Q;
                char[] charsToTrim = { '(', ')' };
                string tmp = new_obj.Teams[no].MemberDetails[k].Round1_Total;
                if (new_obj.Teams[no].MemberDetails[k].Round1_Total != null)
                {
                    tmp = tmp.Trim(charsToTrim);
                }

                csv = Q + new_obj.Teams[no].No + D +
                    order + D +
                    new_obj.Teams[no].IocCode + D +
                    new_obj.Teams[no].MemberDetails[k].CNR + D +
                    new_obj.Teams[no].MemberDetails[k].HorseName + D +
                    new_obj.Teams[no].MemberDetails[k].LastName + C + new_obj.Teams[no].MemberDetails[k].FirstName + D +
                    new_obj.Teams[no].MemberDetails[k].IocCode + D +
                    tmp + D +
                    0.00 + D +
                    new_obj.Teams[no].MemberDetails[k].Status_R1 + D +
                    new_obj.Teams[no].MemberDetails[k].Round2_Total + D +
                    new_obj.Teams[no].MemberDetails[k].Round2_Time + D +
                    " " + D +
                    new_obj.Teams[no].MemberDetails[k].JumpOff_Penalty + D +
                    new_obj.Teams[no].MemberDetails[k].JumpOff_Time + D +
                    " " + D + 0.00 + D + 0.00 + D + " " + Q;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with writing NPW of members of teams in JumpOff [for storder and ranking]!\n" + ex.Message);
            }
            return csv;
        }
    }
}
