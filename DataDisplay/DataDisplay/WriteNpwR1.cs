﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DataDisplay
{
    public class NpwStarterErgebnisR1
    {
        public string team_info(string csv, Serialization obj_round1, int no, int order) // no stands for index of each team
        {
            try
            {
                string Q = "\"";
                string C = ",";
                string D = Q + C + Q;

                csv = Q + D + 
                    obj_round1.Teams[no].No + D + 
                    order + D + 
                    obj_round1.Teams[no].IocCode + D + 
                    obj_round1.Teams[no].Rank + "." + D + 
                    obj_round1.Teams[no].Total_all_Penalty + D + 
                    obj_round1.Teams[no].Total_Round2_Time + D + 
                    "Im Umlauf" + D + 
                    obj_round1.Teams[no].Total_Round1_Penalty + D + 
                    0.00 + D + 
                    obj_round1.Teams[no].Name + D + 
                    obj_round1.Teams[no].Total_Round2_Penalty + D +
                    obj_round1.Teams[no].Total_Round2_Time + D + 
                    obj_round1.Teams[no].Trainer + D + 
                    obj_round1.Teams[no].Total_JumpOff_Penalty + D + 
                    obj_round1.Teams[no].Total_JumpOff_Time + D + 
                    0 + D + 0.00 + D + 0.00 + D + 0.00 + Q;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with writing NPW of team in Round1 [for starter and ergebnis]!\n" + ex.Message);
            }
            return csv;
        }

        public string member_info(string csv, Serialization obj_round1, int no, int k, int order)  // k stand for index of each member in a team
        {
            try
            {
                string Q = "\"";
                string C = ",";
                string D = Q + C + Q;
                char[] charsToTrim = { '(', ')' };
                string tmp = obj_round1.Teams[no].MemberDetails[k].Round1_Total;
                if (obj_round1.Teams[no].MemberDetails[k].Round1_Total != null)
                {
                    tmp = tmp.Trim(charsToTrim);
                }

                csv = Q + D + 
                    obj_round1.Teams[no].No + D + 
                    order + D + 
                    obj_round1.Teams[no].IocCode + D + 
                    obj_round1.Teams[no].MemberDetails[k].CNR + D + 
                    obj_round1.Teams[no].MemberDetails[k].HorseName + D +
                    obj_round1.Teams[no].MemberDetails[k].LastName + C + obj_round1.Teams[no].MemberDetails[k].FirstName + D + 
                    obj_round1.Teams[no].MemberDetails[k].IocCode + D + 
                    tmp + D + 
                    0.00 + D + 
                    obj_round1.Teams[no].MemberDetails[k].Status_R1 + D + 
                    obj_round1.Teams[no].MemberDetails[k].Round2_Total + D + 
                    obj_round1.Teams[no].MemberDetails[k].Round2_Time + D + 
                    " " + D + 
                    obj_round1.Teams[no].MemberDetails[k].JumpOff_Penalty + D + 
                    obj_round1.Teams[no].MemberDetails[k].JumpOff_Time + D + 
                    " " + D + 0.00 + D + 0.00 + D + " " + Q;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with writing NPW of members of teams in Round1 [for starter and ergebnis]!\n" + ex.Message);
            }
            return csv;
        }

        public string current_member_info(string csv, Serialization obj_round1, int no, int k, int order)  // k stand for index of each member in a team
        {
            try
            {
                string Q = "\"";
                string C = ",";
                string D = Q + C + Q;
                char[] charsToTrim = { '(', ')' };
                string tmp = obj_round1.Teams[no].MemberDetails[k].Round1_Total;
                if (obj_round1.Teams[no].MemberDetails[k].Round1_Total != null)
                {
                    tmp = tmp.Trim(charsToTrim);
                }

                csv = Q + "A" + D + 
                    obj_round1.Teams[no].No + D + 
                    order + D + 
                    obj_round1.Teams[no].IocCode + D + 
                    obj_round1.Teams[no].MemberDetails[k].CNR + D + 
                    obj_round1.Teams[no].MemberDetails[k].HorseName + D +
                    obj_round1.Teams[no].MemberDetails[k].LastName + C + obj_round1.Teams[no].MemberDetails[k].FirstName + D + 
                    obj_round1.Teams[no].MemberDetails[k].IocCode + D + 
                    tmp + D + 
                    0.00 + D + 
                    obj_round1.Teams[no].MemberDetails[k].Status_R1 + D + 
                    obj_round1.Teams[no].MemberDetails[k].Round2_Total + D + 
                    obj_round1.Teams[no].MemberDetails[k].Round2_Time + D + 
                    " " + D + 
                    obj_round1.Teams[no].MemberDetails[k].JumpOff_Penalty + D + 
                    obj_round1.Teams[no].MemberDetails[k].JumpOff_Time + D + 
                    " " + D + 0.00 + D + 0.00 + D + " " + Q;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem with writing NPW of current member of a team in Round1 [for starter and ergebnis]!\n" + ex.Message);
            }
            return csv;
        }
    }

    public class NpwStorderRankingR1
    {
        public string team_info(string csv, Serialization obj_round1, int no, int order) // no stands for index of each team
        {
            try
            {
                string Q = "\"";
                string C = ",";
                string D = Q + C + Q;

                csv = Q + obj_round1.Teams[no].No + D +
                    order + D +
                    obj_round1.Teams[no].IocCode + D +
                    obj_round1.Teams[no].Rank + "." + D +
                    obj_round1.Teams[no].Total_all_Penalty + D +
                    obj_round1.Teams[no].Total_Round2_Time + D +
                    "Im Umlauf" + D +
                    obj_round1.Teams[no].Total_Round1_Penalty + D +
                    0.00 + D +
                    obj_round1.Teams[no].Name + D +
                    obj_round1.Teams[no].Total_Round2_Penalty + D +
                    obj_round1.Teams[no].Total_Round2_Time + D +
                    obj_round1.Teams[no].Trainer + D +
                    obj_round1.Teams[no].Total_JumpOff_Penalty + D +
                    obj_round1.Teams[no].Total_JumpOff_Time + D +
                    0 + D + 0.00 + D + 0.00 + D + 0.00 + Q;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Problem with writing NPW of team in round1 [for storder and ranking]!\n" + ex.Message);
            }

            return csv;
        }

        public string member_info(string csv, Serialization obj_round1, int no, int k, int order)  // k stand for index of each member in a team
        {
            try
            {
                string Q = "\"";
                string C = ",";
                string D = Q + C + Q;
                char[] charsToTrim = { '(', ')' };
                string tmp = obj_round1.Teams[no].MemberDetails[k].Round1_Total;
                if (obj_round1.Teams[no].MemberDetails[k].Round1_Total != null)
                {
                    tmp = tmp.Trim(charsToTrim);
                }

                csv = Q + obj_round1.Teams[no].No + D +
                    order + D +
                    obj_round1.Teams[no].IocCode + D +
                    obj_round1.Teams[no].MemberDetails[k].CNR + D +
                    obj_round1.Teams[no].MemberDetails[k].HorseName + D +
                    obj_round1.Teams[no].MemberDetails[k].LastName + C + obj_round1.Teams[no].MemberDetails[k].FirstName + D +
                    obj_round1.Teams[no].MemberDetails[k].IocCode + D +
                    tmp + D +
                    0.00 + D +
                    obj_round1.Teams[no].MemberDetails[k].Status_R1 + D +
                    obj_round1.Teams[no].MemberDetails[k].Round2_Total + D +
                    obj_round1.Teams[no].MemberDetails[k].Round2_Time + D +
                    " " + D +
                    obj_round1.Teams[no].MemberDetails[k].JumpOff_Penalty + D +
                    obj_round1.Teams[no].MemberDetails[k].JumpOff_Time + D +
                    " " + D + 0.00 + D + 0.00 + D + " " + Q;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Problem with writing NPW of members of teams in Round1 [for storder and ranking]!\n" + ex.Message);
            }
            return csv;
        }
    }
}
